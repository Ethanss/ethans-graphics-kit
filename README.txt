Ethans Graphics Kit, a collection of graphics effect for Unity3D(version 5.3.1 specifically)
Ethan Alexander Shulman 2017 http://xaloez.com/


Ethans Graphics Kit includes:

-Lighting System(requires shader level 5), real-time deferred path-tracer with forward-rendered fallback.
-Ethans Lighting, forward-rendered lighting.
-Sky Renderer(requires shader level 3.0), renders sky(clouds, atmospheric scattering, godrays, sun and stars/custom background) to a cubemap. 
-Procedural Generation(CPU based), easily generate triangle meshes from distance functions and procedural textures.
-Image Effects, automatic shader generator(powered by neural networks) and helper classes for using image effect shaders.



Scripting documentation http://xaloez.com/projects/projects/ethans%20graphics%20kit/docs/index.html