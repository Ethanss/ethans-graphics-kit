﻿//Ethan Alexander Shulman 2017
using UnityEngine;
using System.Collections;
using UnityEngine.Rendering;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace EthansGraphicsKit.Sky
{
    /// <summary>
    /// Runtime sky cubemap renderer.
    /// </summary>
    public class SkyRenderer : MonoBehaviour
    {

        /// <summary>
        /// Disable cloud rendering and just display simple and fast sky.
        /// </summary>
        [Tooltip("Disable cloud rendering and just display simple and fast sky.")]
        public bool disableCloudRendering = false;

        /// <summary>
        /// Add ground below horizon and don't render sky below that point to save processing cycles.
        /// </summary>
        [Tooltip("Add ground below horizon and don't render sky below that point to save processing cycles.")]
        public bool renderBelowHorizon = false;

        [Space(15)]

        /// <summary>
        /// Color of the sky in the skybox.
        /// </summary>
        [Tooltip("Color of the sky in the skybox.")]
        public Color skyColor = new Color32(0x45,0x67,0x9F,0xFF);

        /// <summary>
        /// Color of the sky when the sun is at the horizon.
        /// </summary>
        [Tooltip("Color of the sky when the sun is at the horizon.")]
        public Color sunsetColor = new Color(0.9f, 0.4f, 0.5f);

        /// <summary>
        /// Color of the sky when the sun is below the equator.
        /// </summary>
        [Tooltip("Color of the sky when sun is below the equator.")]
        public Color nightSkyColor = new Color(0, 0, 0.01f);

        /// <summary>
        /// Color of the ground in the skybox.
        /// </summary>
        [Tooltip("Color of the ground in the skybox.")]
        public Color groundColor = new Color(0.2f,0.2f,0.2f);

        /// <summary>
        /// Size of the sun.
        /// </summary>
        [Tooltip("Size of the sun.")]
        [Range(0.0f,1.0f)]
        public float sunSize = 0.1f;

        /// <summary>
        /// Intensity of the suns light.
        /// </summary>
        [Tooltip("Intensity of the suns light.")]
        [Range(0.0f,10.0f)]
        public float sunIntensity = 1.0f;
        
        /// <summary>
        /// Color of the sun in the skybox.
        /// </summary>
        [Tooltip("Color of the sun in the skybox.")]
        public Color sunColor = new Color(0.9031f, 0.8687f, 0.7055f);

        /// <summary>
        /// Sun direction object.
        /// </summary>
        [Tooltip("Sun direction object.")]
        public GameObject sunDirection = null;

        /// <summary>
        /// Emissive cubemap.
        /// </summary>
        [Tooltip("Emissive cubemap.")]
        public Cubemap skyEmissiveMap = null;

        /// <summary>
        /// Emissive map strength.
        /// </summary>
        [Tooltip("Emissive map strength.")]
        public float emissiveMapIntensity = 1.0f;

        /// <summary>
        /// Emissive cubemap rotation.
        /// </summary>
        [Tooltip("Emissive cubemap rotation.")]
        public Vector2 emissiveMapRotation = Vector2.zero;

        [Space(15)]

        /// <summary>
        /// Percentage of cloudiness ranging from 0-1, 1 being completely cloudy.
        /// </summary>
        [Tooltip("Percentage of cloudiness ranging from 0-1, 1 being completely cloudy.")]
        [Range(0.0f,1.0f)]
        public float cloudiness = 0.5f;

        /// <summary>
        /// Color of the clouds in the skybox.
        /// </summary>
        [Tooltip("Color of the clouds in the skybox.")]
        public Color cloudColor = new Color(0.95f, 0.95f, 0.95f);

        /// <summary>
        /// Cloud density.
        /// </summary>
        [Tooltip("Cloud density.")]
        [Range(0.1f,1.0f)]
        public float cloudDensity = 0.94f;

        /// <summary>
        /// Roughness of the clouds shape.
        /// </summary>
        [Tooltip("Roughness of the clouds shape.")]
        [Range(0.0f,1.0f)]
        public float cloudRoughness = 0.25f;
        
        /// <summary>
        /// Fluffiness of the clouds.
        /// </summary>
        [Tooltip("Fluffiness of the clouds.")]
        [Range(0.0f, 1.0f)]
        public float cloudFluffiness = 0.2f;
                     
        /// <summary>
        /// Clouds movement on the X axis.
        /// </summary>
        [Tooltip("Clouds movement on the X axis.")]
        public float cloudMovementX = 0.0f;

        /// <summary>
        /// Clouds movement on the Z axis.
        /// </summary>
        [Tooltip("Clouds movement on the Z axis.")]
        public float cloudMovementZ = 0.0f;
        
        /// <summary>
        /// Ambient mist.
        /// </summary>
        [Tooltip("Ambient mist.")]
        [Range(0.0f,1.0f)]
        public float ambientCloudiness = 0.0f;

        /// <summary>
        /// Clouds shape scale.
        /// </summary>
        [Tooltip("Clouds shape scale.")]
        public Vector3 cloudScale = new Vector3(1.0f, 1.0f, 1.0f);

        /// <summary>
        /// Clouds deformation amount.
        /// </summary>
        [Tooltip("Clouds deformation amount.")]
        [Range(0.0f,1.0f)]
        public float cloudDeform = 1.0f;

        /// <summary>
        /// Clouds height in the sky.
        /// </summary>
        [Tooltip("Clouds height in the sky.")]
        [Range(-3.0f,3.0f)]
        public float yPosition = 0.0f;


        [Space(15)]


        /// <summary>
        /// Texture resolution of each cubemap face, must be power of two.
        /// </summary>
        [Tooltip("Texture resolution of each cubemap face, must be power of two.")]
        public int cubemapFaceResolution = 512;
        
        /// <summary>
        /// Delay(in frames) between rendering paths, decreases GPU load by sacrificing render time.
        /// </summary>
        [Tooltip("Delay(in frames) between rendering paths, decreases GPU load by sacrificing render time.")]
        [Range(1,30)]
        public int renderDelay = 1;

        /// <summary>
        /// Apply gamma correct to cubemap.
        /// </summary>
        [Tooltip("Apply gamma correct to cubemap.")]
        public bool applyGammaCorrect = true;

        /// <summary>
        /// Automatically move clouds, requires autoRender is enabled.
        /// </summary>
        [Tooltip("Automatically move clouds, requires autoRender is enabled.")]
        public bool autoCloudMovement = true;

        /// <summary>
        /// Cloud movement vector.
        /// </summary>
        [Tooltip("Cloud movement vector.")]
        public Vector2 autoCloudMovementDir = Vector2.one*0.04f;


        [HideInInspector]
        public Shader skyShader;
        [HideInInspector]
        public Shader cubemapShader;

        private Mesh quadMesh = null;

        private Material skyRenderingMaterial, cubemapMaterial;
        private RenderTexture skyCubemap;

        private CommandBuffer displayCubemap;

        private readonly Vector2[] cubemapFaceRotations = new Vector2[] {
            new Vector2(Mathf.PI/2.0f,Mathf.PI/2.0f),//front
            new Vector2(Mathf.PI/-2.0f,Mathf.PI/2.0f),//back
            new Vector2(0.0f,Mathf.PI/2.0f),//left
            new Vector2(Mathf.PI,Mathf.PI/2.0f),//right
            new Vector2(Mathf.PI/2.0f,0.0f),//up
            new Vector2(-Mathf.PI/2.0f,Mathf.PI),//down
        };
        private readonly CubemapFace[] cubemapFaces = new CubemapFace[] {
            CubemapFace.PositiveZ,
            CubemapFace.NegativeZ,
            CubemapFace.PositiveX,
            CubemapFace.NegativeX,
            CubemapFace.PositiveY,
            CubemapFace.NegativeY
        };
        
        void OnEnable()
        {
            //if enabled, check if cloud renderer is supported
            if (!disableCloudRendering)
            {
                if (!(SystemInfo.supportsRenderToCubemap & SystemInfo.supportsRenderTextures)) {
                    //cloud rendering not supported
                    disableCloudRendering = true;
                    Debug.LogError("Ethans Sky Renderer - System does not support cloud rendering, disabling cloud rendering.");
                }
            }

            //initialize 
            skyRenderingMaterial = new Material(skyShader);
            cubemapMaterial = new Material(cubemapShader);

            if (applyGammaCorrect)
            {
                skyRenderingMaterial.EnableKeyword("ESR_GAMMA_CORRECT");
            }
            else
            {
                skyRenderingMaterial.DisableKeyword("ESR_GAMMA_CORRECT");
            }

            if (quadMesh == null) {
                GameObject quadObj = GameObject.CreatePrimitive(PrimitiveType.Quad);
                quadMesh = quadObj.GetComponent<MeshFilter>().sharedMesh;
                GameObject.Destroy(quadObj);
            }
            

            cubemapFaceResolution = Mathf.ClosestPowerOfTwo(cubemapFaceResolution);
            skyCubemap = new RenderTexture(cubemapFaceResolution, cubemapFaceResolution, 0, RenderTextureFormat.ARGB32);
            skyCubemap.isCubemap = true;

            displayCubemap = new CommandBuffer();
            for (int i = 0; i < 6; i++)
            {
                Vector2 faceRot = cubemapFaceRotations[i];
                displayCubemap.SetGlobalVector("_FaceParams", new Vector4(faceRot.x, faceRot.y, 0.0f, 0.0f));

                displayCubemap.SetRenderTarget(skyCubemap, 0, cubemapFaces[i]);
                displayCubemap.DrawMesh(quadMesh, Matrix4x4.identity, skyRenderingMaterial, 0, disableCloudRendering?4:0);
            }

            StartCoroutine(AutoRender());
        }

        void OnDisable()
        {
            StopAllCoroutines();

            GameObject.Destroy(skyRenderingMaterial);
            GameObject.Destroy(cubemapMaterial);
            GameObject.Destroy(skyCubemap);

            displayCubemap.Dispose();
        }

        /// <summary>
        /// Reload.
        /// </summary>
        public void Reload()
        {
            OnDisable();
            OnEnable();
        }

        /// <summary>
        /// Update sky settings.
        /// </summary>
        public void UpdateSettings()
        {
            if (skyEmissiveMap != null)
            {
                skyRenderingMaterial.SetTexture("_SkyEmissiveMap", skyEmissiveMap);
                skyRenderingMaterial.EnableKeyword("SKY_EMISSIVE_MAP");
            }
            else
            {
                skyRenderingMaterial.DisableKeyword("SKY_EMISSIVE_MAP");
            }
            if (renderBelowHorizon)
            {
                skyRenderingMaterial.DisableKeyword("DONT_RENDER_BELOW_Y");
            }
            else
            {
                skyRenderingMaterial.EnableKeyword("DONT_RENDER_BELOW_Y");
            }

            Vector3 sunDir = sunDirection==null?Vector3.one:sunDirection.transform.forward;
            sunDir.Normalize();

            skyRenderingMaterial.SetVector("_CloudSizeAndSunSize", new Vector4(cloudScale.x, cloudScale.y, cloudScale.z, Mathf.Pow(sunSize,2.2f)));
            skyRenderingMaterial.SetVector("_GroundColorAndCloudRoughness", new Vector4(groundColor.r, groundColor.b, groundColor.b, cloudRoughness*0.1f));
            skyRenderingMaterial.SetVector("_SunColorAndCloudFluffiness", new Vector4(sunColor.r, sunColor.g, sunColor.b, cloudFluffiness));
            skyRenderingMaterial.SetVector("_CloudColorAndCloudDensity", new Vector4(cloudColor.r, cloudColor.g, cloudColor.b, cloudDensity));
            skyRenderingMaterial.SetVector("_CloudDisplacementAndMovementX", new Vector4(cloudDeform*0.2f, 0.0f, 0.0f, cloudMovementX));
            skyRenderingMaterial.SetVector("_SunDirectionAndMovementY", new Vector4(-sunDir.x,-sunDir.y,-sunDir.z, cloudMovementZ));
            skyRenderingMaterial.SetVector("_CloudinessAndEmissiveMapRot", new Vector4(cloudiness, ambientCloudiness, emissiveMapRotation.x, emissiveMapRotation.y));
            skyRenderingMaterial.SetVector("_SkyColorAndSunIntensity", new Vector4(skyColor.r, skyColor.g, skyColor.b, sunIntensity));
            skyRenderingMaterial.SetVector("_EmissiveMapIntensityAndYPosition", new Vector2(emissiveMapIntensity, yPosition));
            skyRenderingMaterial.SetVector("_SunsetColor", sunsetColor);
            skyRenderingMaterial.SetVector("_NightSkyColor", nightSkyColor);

            cubemapMaterial.SetTexture("_Tex", skyCubemap);
            if (Camera.main != null) Camera.main.clearFlags = CameraClearFlags.Skybox;
            RenderSettings.skybox = cubemapMaterial;
        }

        private IEnumerator AutoRender()
        {
            float lastCloudTime = Time.time;

            UpdateSettings();

            while (true)
            {

                    if (autoCloudMovement)
                    {
                        float nowCloudTime = Time.time,
                              delta = (nowCloudTime-lastCloudTime)*0.1f;
                        cloudMovementX += autoCloudMovementDir.x * delta;
                        cloudMovementZ += autoCloudMovementDir.y * delta;
                        lastCloudTime = nowCloudTime;

                        Vector3 sunDir = sunDirection == null ? Vector3.one : sunDirection.transform.forward;
                        sunDir.Normalize();
                        skyRenderingMaterial.SetVector("_CloudDisplacementAndMovementX", new Vector4(cloudDeform * 0.2f, 0.0f, 0.0f, cloudMovementX));
                        skyRenderingMaterial.SetVector("_SunDirectionAndMovementY", new Vector4(-sunDir.x, -sunDir.y, -sunDir.z, cloudMovementZ));
                    }
                    Render();
                
                yield return new WaitForSeconds(renderDelay/60.0f);
            }
        }


        /// <summary>
        /// Render sky.
        /// </summary>
        /// <returns></returns>
        public void Render()
        {
            Graphics.ExecuteCommandBuffer(displayCubemap);
        }



#if UNITY_EDITOR
        [MenuItem("GameObject/Ethans Graphics Kit/Sky Renderer", false, 10)]
        static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            GameObject go = new GameObject("Sky Renderer");
            go.AddComponent<SkyRenderer>();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
#endif
    }
}