﻿//Ethan Alexander Shulman 2016
Shader "Hidden/EthansSkyRenderer"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100


		//first pass is the sky path tracing
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			ZTest Off
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile _____ SKY_EMISSIVE_MAP
			#pragma multi_compile _____ DONT_RENDER_BELOW_Y
			#pragma multi_compile ____ ESR_GAMMA_CORRECT


			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			//frame/cubemap face settings
			float _PathFrame;
			float4 _FaceParams;

			//cloud settings
			float4 _CloudSizeAndSunSize,_GroundColorAndCloudRoughness,_SunColorAndCloudFluffiness,
			_CloudColorAndCloudDensity,_CloudDisplacementAndMovementX, _SunDirectionAndMovementY, _SkyColorAndSunIntensity;
			float4 _CloudinessAndEmissiveMapRot;
			float3 _EmissiveMapIntensityAndYPosition, _NightSkyColor, _SunsetColor;

			samplerCUBE _SkyEmissiveMap;

			//rendering settings
			#define minDelta 0.5
			#define maxDelta 1.0
			#define skipDelta 0.5
			#define viewRangeXZ 60.0
			#define viewRangeY 11.0
			#define sunImportance 0.97
//(clamp((_SunDirectionAndMovementY.y+0.1)*5.0,0.0,0.97))


			//utility functions
			inline float2 rot2d(float2 p, float a) {
				return mul(float2x2(sin(a),cos(a),-cos(a),sin(a)), p);
			}

			float ffract(float p) {
				return (abs(p)%1.0)*2.-1.;
			}
			float3 ffract(float3 p) {
				return (abs(p)%1.0)*2.-1.;
			}

			//returns 1 if a is less than b else it returns 0
			float floatLessThan(in float a, in float b) {
				return clamp((b-a)*1e10, 0., 1.);
			}
			//random float 0-1 from seed a
			float hash(in float a) {
				return ((abs(a*24384.2973)%1.0)*512.34593+a*128.739623);
			}
			//random float 0-1 from seed p
			float hash3(in float3 p) {
				return ((abs(p.x)%1.0)*128.234+(abs(p.y)%1.0)*124.234+((abs(p.z)%1.0)*128.234)+
						((abs(p.x)*128.234)%1.0)*18.234+(abs(p.y*128.234)%1.0)*18.234+((abs(p.z*128.234)%1.0)*18.234))%1.0;
			}

			//random ray in a hemisphere relative to d, uses p as a seed
			float3 randomHemiRay(in float3 d, in float3 p) {
				float3 rand = normalize(ffract(ffract(p)*512.124+ffract(p*16.234)*64.3249+ffract(p*128.234)*12.4345));
				return rand*sign(dot(d,rand));
			}

			//random ray using p as a seed
			float3 randomRay(in float3 p) {
				return normalize(ffract(ffract(p)*512.124+ffract(p*16.234)*64.3249+ffract(p*128.234)*12.4345));
			}

			//calculates density of cloud from distance d and point p
			float dstToDensityMix(float d, float3 p) {
				return min(1., (d-skipDelta)*10.*(1.-_SunColorAndCloudFluffiness.w));
			}

			float smax(float x, float y) {
				return log(exp(x)+exp(y));
			}


			//sky scene functions


			//sample background 
			float3 background(float3 rd) {
				#if defined(DONT_RENDER_BELOW_Y)
				float3 gcp = _GroundColorAndCloudRoughness.xyz;
				#if defined(ESR_GAMMA_CORRECT)
				gcp = pow(_GroundColorAndCloudRoughness.xyz, 2.2);;
				#endif	
				if (rd.y < 0.0) return gcp;
				#endif

				float3 sky = _SunColorAndCloudFluffiness.xyz*_SkyColorAndSunIntensity.w*max(0.0, 1.0-max(0., length(rd - _SunDirectionAndMovementY.xyz)-_CloudSizeAndSunSize.w)*6.0)*6.0;
				#if defined(DONT_RENDER_BELOW_Y)
				/*float sun = max(1.0 - (1.0 + 10.0 * max(0., _SunDirectionAndMovementY.y) + 1.0 * rd.y) * max(0., length(rd - _SunDirectionAndMovementY.xyz)-_CloudSizeAndSunSize.w),0.0)
							    + 0.3 * pow(1.0-rd.y,12.0) * (1.6-max(0.,_SunDirectionAndMovementY.y));
				sky = lerp(_SkyColorAndSunIntensity.xyz, _SunColorAndCloudFluffiness.xyz*_SkyColorAndSunIntensity.w, sun)
						  * ((0.5 + 1.0 * pow(max(0.,_SunDirectionAndMovementY.y),0.4)) * (1.5-abs(_SunDirectionAndMovementY.y))
						  + pow(sun, 5.2) * max(0.,_SunDirectionAndMovementY.y) * (5.0 + 15.0 * max(0.,_SunDirectionAndMovementY.y))*_SkyColorAndSunIntensity.w);*/
				float h = 1.-max(0.,rd.y);
				sky += max(lerp(_SkyColorAndSunIntensity.xyz*(.6+_SunDirectionAndMovementY.y*.4),//sky
            _SunsetColor, (1.-abs(_SunDirectionAndMovementY.y))*h-pow(length(rd-_SunDirectionAndMovementY.xyz)*.4,2.)), //horizon scattering
            _NightSkyColor); //night
				#endif

				#if defined(SKY_EMISSIVE_MAP)
				float3 semrd = rd;
				semrd.yz = rot2d(semrd.yz, _CloudinessAndEmissiveMapRot.w);
				semrd.xz = rot2d(semrd.xz, _CloudinessAndEmissiveMapRot.z);
				sky += texCUBE(_SkyEmissiveMap, semrd).xyz*_EmissiveMapIntensityAndYPosition.x;
				#endif

				sky = clamp(sky,0.,1.);
				#if defined(ESR_GAMMA_CORRECT)
				sky = pow(sky,2.2);
				#endif			
				return sky;
			}

			//distance function defining the clouds shape
			float df(float3 p) {
				//cloud base shape
				float3 cp = (p+float3(_CloudDisplacementAndMovementX.w, 0.0, _SunDirectionAndMovementY.w));

				float ldst = abs(p.y-(58.0-_EmissiveMapIntensityAndYPosition.y)+length(p.xz)/5.0)+(4.0-_CloudinessAndEmissiveMapRot.x*6.0f);
				cp /= _CloudSizeAndSunSize.xyz;

				for (int i = 1; i < 4; i++) {
				    float pfi = pow(float(i),2.)*_CloudDisplacementAndMovementX.x,
						dpfi = 1.0/max(1e-7,pfi),
                		dmx = lerp(1.,ldst+cos((cp.x+cp.y+cp.z)*dpfi)*pfi,_GroundColorAndCloudRoughness.w);
					ldst += cos(cp.x*dpfi+cos(26.2348+dmx*(cp.y*.39)*dpfi)*4.)*
							 cos(cp.y*dpfi+cos(29.8937+dmx*(cp.z*.37)*dpfi)*4.)*
							 cos(cp.z*dpfi+cos(14.972+dmx*(cp.x*.41)*dpfi)*4.)*pfi;
				}

				return max(ldst, skipDelta);
			}


			

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = float4(v.vertex.xy*2.0,0.0,1.0);
				o.uv = v.vertex.xy*2.0;
				#if UNITY_UV_STARTS_AT_TOP
				//o.uv.y = 1.0-o.uv.y;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//set up ray with cube face rotation
				float3 rayDir = 0.0;

				rayDir = normalize(float3(i.uv,1.0));
				rayDir.yz = rot2d(rayDir.yz, _FaceParams.y);
				rayDir.xz = rot2d(rayDir.xz, _FaceParams.x);

				//ray position at origin
				float3 rp = float3(0.0,50.0,0.0),
					 rd = rayDir;
				
				//if bottom half of cubemap skip processing and just return background
				#if defined(DONT_RENDER_BELOW_Y)
				if (rayDir.y < 0.0) {
					fixed4 ryc = fixed4(background(rayDir), 1.0);
				#if defined(ESR_GAMMA_CORRECT)
				ryc.xyz = pow(ryc.xyz, 1.0/2.2);
				#endif
					return ryc;
				}
				#endif


				float3 sunBackgroundDir = _SunDirectionAndMovementY.xyz;
				sunBackgroundDir.y = max(0.05, sunBackgroundDir.y);
				sunBackgroundDir = normalize(sunBackgroundDir);

				float frame = _PathFrame;
				#define rndifrm(s) (((abs(frame*.044877+s)%1.0)*256.494+frame*.02934)%1.0)

				float4 c = float4(1,1,1,0);  
    
				//render
				for (int i = 0; i < 64; i++) {
					float d = df(rp),
						  dt = d*(minDelta+hash3(rp+rndifrm(rp)*1024.)*(maxDelta-minDelta)),
						  kt = dstToDensityMix(d,rp);
					if (lerp(_CloudColorAndCloudDensity.w, _CloudinessAndEmissiveMapRot.y*clamp(1.0-max(0.0, (d-skipDelta)*(1.-_SunColorAndCloudFluffiness.w)/10.0-0.5), 0.0, 1.0), kt)*max(1.,dt*.1) > hash3(rp+rndifrm(rp)*256.)) {//if density > probability random then ray hits cloud
						c.xyz *= lerp(_CloudColorAndCloudDensity.xyz,float3(1,1,1),kt);
						c.w = 1.0;
						rd = normalize(lerp(randomRay(rp+rndifrm(rp*1024.)*1024.), sunBackgroundDir, clamp(floor(hash3(rp*.9+rndifrm(rp*1.5)*512.)+sunImportance+kt),0.,1.)));
					}
        
					rp += rd*dt;
					if (abs(rp.y-50.0) > viewRangeY || length(rp.xz) > viewRangeXZ) break;
				}
  
				c.xyz *= background(rd);//if light ray makes it too edge of world illuminate it  
				c.xyz = lerp(background(rayDir), c.xyz, min(1.0,c.w));				

				#if defined(ESR_GAMMA_CORRECT)
				c.xyz = pow(c.xyz, 1.0/2.2);
				#endif

				c.w = 0.1;
				return c;
				/*return float4( lerp(background(rayDir), c.xyz, c.w)/_FaceParams.z,//blend result with background
								1.0);
			*/			
			}
			ENDCG
		}


		//second pass copy cube map face
		Pass
		{
			Blend Off
			ZTest Off
			ZWrite Off
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			sampler2D _FaceToCopy;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = float4(v.vertex.xy*2.0,0.0,1.0);
				o.uv = v.vertex.xy+0.5;
				#if UNITY_UV_STARTS_AT_TOP
				o.uv.y = 1.0-o.uv.y;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return tex2D(_FaceToCopy, i.uv);
			}
			ENDCG
		}

		//third pass lerp between cubemap frames, optionally applying gamma correct
		Pass
		{
			Blend Off
			ZTest Off
			ZWrite Off
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile ____ ESR_GAMMA_CORRECT

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			sampler2D_float _NextFrame;
			sampler2D _LastFrame;
			float _FrameLerpAmount;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = float4(v.vertex.xy*2.0,0.0,1.0);
				o.uv = v.vertex.xy+0.5;
				#if UNITY_UV_STARTS_AT_TOP
				o.uv.y = 1.0-o.uv.y;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 c = lerp(tex2D(_LastFrame, i.uv), tex2D(_NextFrame, i.uv), _FrameLerpAmount);
				
				#if defined(ESR_GAMMA_CORRECT)
				c.xyz = pow(c.xyz, 1.0/2.2);
				#endif

				c.w = 1.0;

				return c; 
			}
			ENDCG
		}

		//fourth pass copy cubemap frames, optionally applying gamma correct
		Pass
		{
			Blend Off
			ZTest Off
			ZWrite Off
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile ____ ESR_GAMMA_CORRECT

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			sampler2D_float _FaceToCopy;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = float4(v.vertex.xy*2.0,0.0,1.0);
				o.uv = v.vertex.xy+0.5;
				#if UNITY_UV_STARTS_AT_TOP
				o.uv.y = 1.0-o.uv.y;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 c = tex2D(_FaceToCopy, i.uv);
				
				#if defined(ESR_GAMMA_CORRECT)
				c.xyz = pow(c.xyz, 1.0/2.2);
				#endif

				return c; 
			}
			ENDCG
		}


		//fifth pass is quick sky render with no clouds
		Pass
		{
			Blend Off
			ZTest Off
			ZWrite Off
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile ____ SKY_EMISSIVE_MAP
			#pragma multi_compile ____ DONT_RENDER_BELOW_Y
			#pragma multi_compile ____ ESR_GAMMA_CORRECT

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};


			//frame/cubemap face settings
			float _PathFrame;
			float4 _FaceParams;

			//cloud settings
			float4 _CloudSizeAndSunSize,_GroundColorAndCloudRoughness,_SunColorAndCloudFluffiness,
			_CloudColorAndCloudDensity,_CloudDisplacementAndMovementX, _SunDirectionAndMovementY, _SkyColorAndSunIntensity;
			float4 _CloudinessAndEmissiveMapRot;
			float3 _EmissiveMapIntensityAndYPosition, _NightSkyColor, _SunsetColor;

			samplerCUBE _SkyEmissiveMap;


			inline float2 rot2d(float2 p, float a) {
				return mul(float2x2(sin(a),cos(a),-cos(a),sin(a)), p);
			}

			//sample background 
			float3 background(float3 rd) {
				#if defined(DONT_RENDER_BELOW_Y)
				float3 gcp = _GroundColorAndCloudRoughness.xyz;
				#if defined(ESR_GAMMA_CORRECT)
				gcp = pow(_GroundColorAndCloudRoughness.xyz, 2.2);;
				#endif	
				if (rd.y < 0.0) return gcp;
				#endif

				float3 sky = _SunColorAndCloudFluffiness.xyz*_SkyColorAndSunIntensity.w*max(0.0, 1.0-max(0., length(rd - _SunDirectionAndMovementY.xyz)-_CloudSizeAndSunSize.w)*6.0)*6.0;
				#if defined(DONT_RENDER_BELOW_Y)
				/*float sun = max(1.0 - (1.0 + 10.0 * max(0., _SunDirectionAndMovementY.y) + 1.0 * rd.y) * max(0., length(rd - _SunDirectionAndMovementY.xyz)-_CloudSizeAndSunSize.w),0.0)
							    + 0.3 * pow(1.0-rd.y,12.0) * (1.6-max(0.,_SunDirectionAndMovementY.y));
				sky = lerp(_SkyColorAndSunIntensity.xyz, _SunColorAndCloudFluffiness.xyz*_SkyColorAndSunIntensity.w, sun)
						  * ((0.5 + 1.0 * pow(max(0.,_SunDirectionAndMovementY.y),0.4)) * (1.5-abs(_SunDirectionAndMovementY.y))
						  + pow(sun, 5.2) * max(0.,_SunDirectionAndMovementY.y) * (5.0 + 15.0 * max(0.,_SunDirectionAndMovementY.y))*_SkyColorAndSunIntensity.w);*/
				float h = 1.-max(0.,rd.y);
				sky += max(lerp(_SkyColorAndSunIntensity.xyz*(.6+_SunDirectionAndMovementY.y*.4),//sky
            _SunsetColor, (1.-abs(_SunDirectionAndMovementY.y))*h-pow(length(rd-_SunDirectionAndMovementY.xyz)*.4,2.)), //horizon scattering
            _NightSkyColor); //night
				#endif

				#if defined(SKY_EMISSIVE_MAP)
				float3 semrd = rd;
				semrd.yz = rot2d(semrd.yz, _CloudinessAndEmissiveMapRot.w);
				semrd.xz = rot2d(semrd.xz, _CloudinessAndEmissiveMapRot.z);
				sky += texCUBE(_SkyEmissiveMap, semrd).xyz*_EmissiveMapIntensityAndYPosition.x;
				#endif

				sky = clamp(sky,0.,1.);
				#if defined(ESR_GAMMA_CORRECT)
				sky = pow(sky,2.2);
				#endif			
				return sky;
			}
			


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = float4(v.vertex.xy*2.0,0.0,1.0);
				o.uv = v.vertex.xy*2.0;
				#if UNITY_UV_STARTS_AT_TOP
				//o.uv.y = 1.0-o.uv.y;
				#endif
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//set up ray with cube face rotation
				float3 rayDir = 0.0;

				rayDir = normalize(float3(i.uv,1.0));
				rayDir.yz = rot2d(rayDir.yz, _FaceParams.y);
				rayDir.xz = rot2d(rayDir.xz, _FaceParams.x);

				float4 c = float4(background(rayDir), 1.0);
				#if defined(ESR_GAMMA_CORRECT)
				c.xyz = pow(c.xyz, 1.0/2.2);
				#endif

				return c;
			}
			ENDCG
		}
	}
}
