Shader "Hidden/EGK_Dither"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		//regular dither, param1 = per color channel precision
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			
			#include "UnityCG.cginc"


			sampler2D _MainTex;
			float3 _Param1, _Param2, _Param3;
			float _Strength;

			float dither(float2 u) {
				#define l(i) length((u*.7+(i*float2(1,8))%1.+cos(u.yx*.2333+i*8.))%1.-.5)
				return min(l(.1),l(.6))*2.6666-1.;
			}

			fixed4 frag (v2f_img i) : SV_Target
			{
				return fixed4(floor(tex2D(_MainTex, i.uv).xyz*(_Param1+.99)+dither(i.uv*_ScreenParams.xy))/_Param1, 1);
			}
			ENDCG
		}


		//1-bit dither, param1 = median, param2 = below median color, param3 = above median color
		Pass
			{
				CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag

				#include "UnityCG.cginc"


				sampler2D _MainTex;
				float3 _Param1, _Param2, _Param3;
				float _Strength;

				float dither(float2 u) {
					#define l(i) length((u*.7+(i*float2(1,8))%1.+cos(u.yx*.2333+i*8.))%1.-.5)
					return min(l(.1),l(.6))*2.6666-1.;
				}

				fixed4 frag(v2f_img i) : SV_Target
				{
					fixed3 c = tex2D(_MainTex, i.uv).xyz;
					fixed2 p = normalize(fixed2(length(c-_Param2),length(c-_Param3)))*2.;
					return fixed4(lerp(_Param2, _Param3, clamp(p.x-p.y+dither(i.uv*_ScreenParams.xy)*.5, 0., 1.)), 1);
				}
				ENDCG
			}
	}

	Fallback "Hidden/BlitCopy"
}
