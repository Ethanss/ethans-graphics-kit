//Ethan Alexander Shulman 2017

Shader "EGK/Image Effects/Portal"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Tint ("Tint", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Transparent"
				"Queue"="Transparent" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 opos : TEXCOORD1;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST, _Tint;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.opos = v.vertex.xyz;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col;
				col.xyz = 0.0;
				col.w = 1.0;

				float3 rd = mul(_World2Object, float4(_WorldSpaceCameraPos,1)).xyz;
				rd = normalize(i.opos-rd);
				
				float dx = (i.opos.x-0.5*sign(rd.x))/-rd.x,
					  dy = (i.opos.y-0.5*sign(rd.y))/-rd.y;
    
				float3 spos;
				if (abs(dx) < abs(dy)) {
					spos = float3((i.opos+rd*abs(dx)).zy/float2(10.,2.), (1.+max(0.,abs(dx)-1.)));
				} else {
					spos = float3((i.opos+rd*abs(dy)).xz/float2(2.,10.), (1.+max(0.,abs(dy)-1.)));
				}

				col.xyz = tex2D(_MainTex, spos.xy)/spos.z;
				return col*_Tint;
			}
			ENDCG
		}
	}
}
