//Ethan Alexander Shulman 2017

using UnityEngine;
using UnityEngine.Rendering;

namespace EthansGraphicsKit.ImageEffects
{
    public class TemporalFullscreenImageEffect : MonoBehaviour
    {
        public Shader effect;
        public int pass = 0;
        [Range(0.0f, 1.0f)]
        public float strength = 1.0f;
        [Range(0.0f, 1.0f)]
        public float temporality = 0.1f;
        public Vector3 param1, param2, param3;

        private Material effectMat;
        private RenderTexture last;


        void OnEnable()
        {
            last = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);

            effectMat = new Material(effect);
            effectMat.SetVector("_MainTexSize", new Vector2(Screen.width, Screen.height));
            effectMat.SetVector("_Param1", param1);
            effectMat.SetVector("_Param2", param2);
            effectMat.SetVector("_Param3", param3);
            effectMat.SetFloat("_Strength", strength);
            effectMat.SetFloat("_Temporality", temporality);
        }

        void OnDisable()
        {
            GameObject.DestroyImmediate(effectMat);
            GameObject.DestroyImmediate(last);
        }


        /// <summary>
        /// 
        /// </summary>
        public void UpdateStrength()
        {
            effectMat.SetFloat("_Strength", strength);
        }

        public void UpdateTemporality()
        {
            effectMat.SetFloat("_Temporality", temporality);
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateParams()
        {
            effectMat.SetVector("_Param1", param1);
            effectMat.SetVector("_Param2", param2);
            effectMat.SetVector("_Param3", param3);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        public void SetParam1(Vector3 p1)
        {
            param1 = p1;
            effectMat.SetVector("_Param1", param1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        public void SetParam2(Vector3 p1)
        {
            param2 = p1;
            effectMat.SetVector("_Param2", param2);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1"></param>
        public void SetParam3(Vector3 p1)
        {
            param3 = p1;
            effectMat.SetVector("_Param3", param3);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="w"></param>
        /// <param name="h"></param>
        public void SetSrcTextureSize(int w, int h)
        {
            effectMat.SetVector("_MainTexSize", new Vector2(w, h));
        }




        void OnRenderImage(RenderTexture src, RenderTexture dst)
        {
            effectMat.SetTexture("_LastTex", last);
            effectMat.SetTexture("_MainTex", src);
            Graphics.Blit(src, dst, effectMat, pass);
            Graphics.Blit(dst, last);
        }
    }
}