//Ethan Alexander Shulman 2017


float3 _ELFogColor, _ELFogParams,
		_VolumeOrigin, _VolumeParams;
float _ELGIOnly;
sampler2D _ELReflection, _ELGI;


inline fixed3 sampleGlobalIlluminationVolume(float3 pos, float3 bias) {
	float3 vpos = clamp((pos - _VolumeOrigin) / _VolumeParams.z + bias, 0., _VolumeParams.x - 2.);

	float2 lxz = vpos.xz%1.0;
	half2 fxz = floor(vpos.xz);

	half4 c1, c2, c3;
	half2 s1 = fxz,
		s2 = fxz,
		s3 = fxz;

	s2.y += 1.0;
	s3.x += 1.0;
	if (lxz.x + lxz.y > 1.0) {
		s1 += 1.0;
		lxz = 1.0 - lxz.yx;
	}

	c1 = tex2Dlod(_ELGI, float4((float2(s1.x+s1.y*_VolumeParams.x, vpos.y)) / _VolumeParams.yx, 0, 0));
	c2 = tex2Dlod(_ELGI, float4((float2(s2.x + s2.y*_VolumeParams.x, vpos.y)) / _VolumeParams.yx, 0, 0));
	c3 = tex2Dlod(_ELGI, float4((float2(s3.x + s3.y*_VolumeParams.x, vpos.y)) / _VolumeParams.yx, 0, 0));

	return c1*(1.0 - (lxz.x + lxz.y)) + c2*lxz.y + c3*lxz.x;
}

inline float3 computeLighting(v2f i, fixed3 albedo) {
	//worldspace reflection and depth used for fog
	float3 camPos = i.wpos - _WorldSpaceCameraPos;
	half3 reflDir = reflect(normalize(camPos), i.normal);
	float fogDepth = min(1., length(camPos) / _ELFogParams.x);

	float3 diff, spec;
#if SHADER_API_D3D9 || SHADER_API_D3D11_9X
	diff = float3(0.1, 0.1, 0.1);
	spec = float3(0.1, 0.1, 0.1);
#else
	if (_ELGIOnly > 0.5) {
		diff = 0;
		spec = 0;
	}
	else {
		diff = pow(sampleGlobalIlluminationVolume(i.wpos, i.normal), .6);//sample gi volume
		spec = tex2Dlod(_ELReflection, float4((atan2(reflDir.x, reflDir.z) / 6.283185 + .5), reflDir.y*.5 + .5, 0., _Roughness*6.));//sample reflection probe
		spec = lerp(spec, diff, max(_ELGIOnly, (_Roughness - .7)*3.3));
	}
#endif

	//global directional light
	if (_WorldSpaceLightPos0.w < 1.) {
		half3 la = _LightColor0*LIGHT_ATTENUATION(i);
		diff += la*max(0.,dot(i.normal, _WorldSpaceLightPos0.xyz));
		spec += la*pow(max(0.,dot(reflDir, _WorldSpaceLightPos0.xyz)), 64.-_Roughness*62.)*(2.-_Roughness*1.5);
	}
	
	//final sum and fog mix
	return lerp(albedo*(_DiffuseColor*_Emission + _DiffuseColor*diff*(.5 - .5*_Metallic) + _SpecularColor*spec*(.5 + .5*_Metallic)),
				_ELFogColor, pow(fogDepth, _ELFogParams.z)*_ELFogParams.y);
}

inline float3 computeParticleLighting(v2f i, out fixed4 fog) {
	//worldspace look direction and depth used for fog
	float3 camPos = i.wpos - _WorldSpaceCameraPos;
	half3 reflDir = normalize(camPos);
	float fogDepth = min(1., length(camPos) / _ELFogParams.x);

	float3 diff, spec;
#if SHADER_API_D3D9 || SHADER_API_D3D11_9X
	diff = float3(0.1, 0.1, 0.1);
	spec = float3(0.1, 0.1, 0.1);
#else
	if (_ELGIOnly > 0.5) {
		diff = 0;
		spec = 0;
	}
	else {
		diff = pow(sampleGlobalIlluminationVolume(i.wpos, float3(0, 0, 0)), .6);//sample gi volume
		spec = tex2Dlod(_ELReflection, float4((atan2(reflDir.x, reflDir.z) / 6.283185 + .5), reflDir.y*.5 + .5, 0., 2. + _Roughness*5.));//sample reflection probe
		spec = lerp(spec, diff, max(_ELGIOnly, (_Roughness - .7)*3.3));
	}
#endif

	//global directional light
	if (_WorldSpaceLightPos0.w < 1.) {
		half3 la = _LightColor0*LIGHT_ATTENUATION(i);
		diff += la;
		spec += la*pow(max(0., dot(reflDir, _WorldSpaceLightPos0.xyz)), 8. - _Roughness*7.)*(2. - _Roughness*1.5);
	}

	//final sum and fog mix
	fog = fixed4(_ELFogColor, pow(fogDepth, _ELFogParams.z)*_ELFogParams.y);
	return _DiffuseColor*_Emission + _DiffuseColor*diff*(.5 - .5*_Metallic) + _SpecularColor*spec*(.5 + .5*_Metallic);
}

inline float3 computeDiffuseOnlyLighting(v2f i, fixed3 albedo) {
	//depth used for fog
	float fogDepth = min(1., length(i.wpos - _WorldSpaceCameraPos) / _ELFogParams.x);

	float3 diff;
#if SHADER_API_D3D9 || SHADER_API_D3D11_9X
	diff = float3(0.1, 0.1, 0.1);
#else
	if (_ELGIOnly > 0.5) {
		diff = 0;
	} else {
		diff = pow(sampleGlobalIlluminationVolume(i.wpos, i.normal), .6);//sample gi volume
	}
#endif

	//global directional light
	if (_WorldSpaceLightPos0.w < 1.) {
		half3 la = _LightColor0*LIGHT_ATTENUATION(i);
		diff += la*max(0., dot(i.normal, _WorldSpaceLightPos0.xyz));
	}

	//final sum and fog mix
	return lerp(albedo*(_DiffuseColor*_Emission + _DiffuseColor*diff),
		_ELFogColor, pow(fogDepth, _ELFogParams.z)*_ELFogParams.y);
}