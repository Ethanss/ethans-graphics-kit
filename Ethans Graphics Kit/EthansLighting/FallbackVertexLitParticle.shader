﻿Shader "Ethans Lighting/VertexLit Fallback Particle" {
	Properties{
		_MainTex("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_AlphaCutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5
	}

		SubShader{
		LOD 100
		Tags{ "Queue" = "AlphaTest+1"
		"RenderType" = "Transparent" }

		//Single pass
		Pass
		{

			CGPROGRAM
			//defines
			#pragma vertex vert
			#pragma fragment frag

			//material parameters
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _AlphaCutoff;


			//includes
#include "UnityCG.cginc"
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 light : TEXCOORD1;
			};
			v2f vert(appdata_base i)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, i.vertex);
				o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
				//static diffuse vertex lighting
				o.light = float4(lerp(_DiffuseColor.xyz, _SpecularColor, 0.5 + _Metallic*0.5),
					max(0.0, 1.0 + dot(i.normal, normalize(float3(1.0, 1.0, 1.0)))) / 2.0);
				o.light.xyz *= o.light.w;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				//apply texture color tint and lighting
				fixed4 s = tex2D(_MainTex, i.uv);
				clip(s.w - _AlphaCutoff);
				
				fixed3 c = s.xyz*i.light.xyz;
				return fixed4(c, 1);
			}
				ENDCG
		}

		//no particle shadows
	}

}
