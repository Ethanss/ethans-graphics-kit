﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Hidden/GI"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100


		//pass 0 - apply gi slice to volume
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"


			sampler2D _MainTex;
			float _Alpha;
			float4x4 _SliceMatrix;
			float3 _VolumeParams, _VolumeOrigin, _SliceOrigin;
			

			fixed4 frag(v2f_img i) : SV_Target
			{
				float3 vpos;
				vpos.xy = floor(i.uv*_VolumeParams.yx);
				vpos.z = floor(vpos.x / _VolumeParams.x);
				vpos.x -= vpos.z*_VolumeParams.x;

				float4 cpos = mul(_SliceMatrix, float4(vpos*_VolumeParams.z+_VolumeOrigin-_SliceOrigin, 1));
				cpos.xy = cpos.xy / (_VolumeParams.x*_VolumeParams.z) + .5;

				return fixed4(tex2D(_MainTex, cpos.xy + .5/_VolumeParams.x).xyz, _Alpha*clamp(2. - abs(cpos.z/_VolumeParams.z)*2., 0., 1.));
			}
			ENDCG
		}


			//pass 1 - copy gi volume to new position
			Pass {
				CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag

				#include "UnityCG.cginc"


				sampler2D _MainTex;
				float3 _Offset;
				float3 _VolumeParams;

				fixed4 frag(v2f_img i) : SV_Target
				{
					float3 vpos;
					vpos.xy = floor(i.uv*_VolumeParams.yx);
					vpos.z = floor(vpos.x / _VolumeParams.x);
					vpos.x -= vpos.z*_VolumeParams.x;

					vpos = clamp(vpos+_Offset,0.,_VolumeParams.x-1.);
					
					return tex2D(_MainTex, float2(vpos.x+vpos.z*_VolumeParams.x+.5,vpos.y+.5)/_VolumeParams.yx);
				}
				ENDCG
			}
	}
}
