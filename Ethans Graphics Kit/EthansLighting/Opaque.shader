﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Opaque"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_Roughness("Roughness", Range(0.0, 1.0)) = 0.0
		_Emission("Emission", Range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Opaque" }

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
			#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			


			#include "UnityCG.cginc"

			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;


#if defined(EGK_NOLIGHTING)
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float3 normal : TEXCOORD2;
				LIGHTING_COORDS(3, 4)
			};

			#include "EthansLighting.cginc"
			

			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply lighting
				col.xyz = computeLighting(i,col.xyz);
				return col;
			}
#endif

#if defined(EGK_DEFERREDGI)
#include "EGKVolume.cginc"

			struct vertexOutput
			{
				float4 vertex : SV_POSITION;
				float3 normalWorld : TEXCOORD0;
				float2 uv : TEXCOORD1;
			};

			vertexOutput vert(appdata_base i)
			{
				vertexOutput o;
				o.vertex = mul(UNITY_MATRIX_MVP, i.vertex);
				o.normalWorld = UnityObjectToWorldNormal(i.normal);
				o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
				return o;
			}

			float4 frag(vertexOutput i) : SV_Target
			{
				//diffuse texture and tint
				fixed3 diffuse = pow(tex2D(_MainTex, i.uv).xyz, 2.2);

				fixed3 normal = normalize(i.normalWorld)*0.5 + 0.5;

				//pack material data into high precision float color
				return float4(encodeFixed3(diffuse*_DiffuseColor.xyz),
					encodeFixed3(diffuse*_SpecularColor.xyz),
					encodeFixed3(fixed3(_Metallic, _Roughness, _Emission)),
					encodeFixed3(normal));
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/VertexLit Fallback"
}
