﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Hidden/Reflection"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass {
				Blend SrcAlpha OneMinusSrcAlpha

				CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag

				#include "UnityCG.cginc"


				samplerCUBE _MainCube;
				float _Alpha;

				fixed4 frag(v2f_img i) : SV_Target
				{
					i.uv.x *= 6.283185;
					float3 rd = normalize(float3(-sin(i.uv.x), i.uv.y*2. - 1., -cos(i.uv.x)));
					return fixed4(texCUBE(_MainCube, rd).xyz, _Alpha);
				}
				ENDCG
			}
	}
}
