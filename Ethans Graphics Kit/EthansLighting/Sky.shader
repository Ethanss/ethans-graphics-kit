﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Sky"
{
	Properties
	{
		_SkyColor("Sky Color", Color) = (.2, .4, .6, 1)
		_SunsetColor("Sunset Color", Color) = (.8, .5, .4, 1)
		_NightColor("Night Color", Color) = (0, 0, 0.1, 1)
		_SunSize("Sun Size", Range(0.0, 1.0)) = 0.9
		[Space(15)]
		_Ground("Ground", Range(0.0,1.0)) = 0.0
		_GroundBrightness("Ground Brightness", Range(0.0, 1.0)) = 0.1
		_GroundAmbient("Ground Ambience", Range(0.0, 1.0)) = 0.5
		[Space(15)]
		_Background("Background Texture", 2D) = "clear" {}
		_BackgroundTint("Background Tint", Color) = (1,1,1,0)
		_BackgroundRotation("Background Rotation", Vector) = (0,0,0,0)
	}
	SubShader
	{
		Tags{ "Queue" = "Background" "RenderType" = "Background" "PreviewType" = "Skybox" }
		Cull Off ZWrite Off

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			
			#include "UnityCG.cginc"
			#include "Lighting.cginc"

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 wpos : TEXCOORD0;
			};

			sampler2D _Background;
			float3 _SkyColor, _SunsetColor, _NightColor;
			float4 _BackgroundTint;
			float2 _BackgroundRotation;
			float _Ground, _SunSize, _GroundAmbient, _GroundBrightness;
			
			v2f vert (float4 vertex : POSITION)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, vertex);
				o.wpos = mul(_Object2World, vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				float3 d = normalize(i.wpos-_WorldSpaceCameraPos);

				float y = _WorldSpaceLightPos0.y,
					s = length(d - _WorldSpaceLightPos0);
				
				fixed3 c = max(lerp(_SkyColor*(.6 + y*.4),
					_SunsetColor, (1. - abs(y))*(1. - max(0., d.y)) - max(0.,pow(s, 2.)*(.5-y*.5))), //horizon scattering
					_NightColor); //night
				fixed g = clamp((d.y+_Ground)*1e2, 0., 1.);
				c += g*max(0., 1. - s*100.*(1.-_SunSize))*_LightColor0;//sun
				fixed4 bg = tex2D(_Background, float2(abs(atan2(d.x, d.z) / 6.283185 + .5 + _BackgroundRotation.x)%1.0, abs(d.y*.5 + .5 +_BackgroundRotation.y)%1.0))*_BackgroundTint;
				c = lerp(c, bg.xyz, bg.w);
				c = lerp(lerp(c,fixed3(1,1,1),_GroundAmbient)*_GroundBrightness,c,g);

				return fixed4(c, 1);
			}
			ENDCG
		}
	}

	Fallback Off
}
