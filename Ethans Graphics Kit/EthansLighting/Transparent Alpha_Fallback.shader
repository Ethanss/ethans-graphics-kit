﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Hidden/Transparent Alpha_Fallback"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_Emission("Emission", Range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Transparent" "Queue" = "AlphaTest+1" "IgnoreProjector" = "True" }

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
			#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			
			#include "UnityCG.cginc"

			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;


#if defined(EGK_NOLIGHTING)
#include "Lighting.cginc"
#include "AutoLight.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float3 normal : TEXCOORD2;
				LIGHTING_COORDS(3, 4)
			};

			#include "EthansLighting.cginc"
			

			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply lighting
				col.xyz = computeDiffuseOnlyLighting(i,col.xyz);

				col.w *= _DiffuseColor.w;
				return col;
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/VertexLit Fallback Cutout"
}
