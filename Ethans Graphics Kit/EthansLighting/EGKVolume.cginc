//Ethan Alexander Shulman 2017

//Helper functions for lighting

sampler3D _EGKMaterial, _EGKDistance, _EGKLighting;

float3 _EGKOrigin;
float4 _EGKParams;
int2 _EGKGIParams;//x = ray trace steps, y = number of bounces/paths

samplerCUBE _EGKBackgroundCube;
float4 _EGKBackgroundColor;
float _EGKBackgroundType;

float4 _EGKLastCameraPos;
float4x4 _EGKLastWorldToCamera, _EGKCameraToWorld, _EGKWorldToCamera;





//random float3 with values from 0-1 seeded from float3
float3 hash33(float3 p) {
	return abs(cos(p*.19487)*9284.3459 + cos(p.zxy*29.97612)*37.92384) % 1.0;
}

//random float from 0-1 seeded from float3
float hash13(float3 s) {
	return (abs(cos(dot(s, float3(7, 157, 113))))*43758.5453) % 1.0;
}

//encode fixed3(clamped 0-1) to hdr float
float encodeFixed3(fixed3 c) {
	return floor(c.x*255.) / 256. +
		floor(c.y*255.) +
		floor(c.z*255.)*256.;
}
//decode hdr float to fixed3
fixed3 decodeFixed3(float v) {
	float q = v;
	float3 c;

	c.z = floor(q / 256.0);
	q -= c.z*256.0;

	c.y = floor(q);
	q -= c.y;

	c.x = floor(q*256.0);
	return c/255.0;
}

//normal in distance volume, wp is position in voxel space
float3 distanceNormalLight(float3 wp) {
	const float3 NE = float3(1.0,0.0,0.0);
	#if defined(EGK_LODCASCADE_ON)
	float3 divX = _EGKParams.x*float3(2,1,1);
	return float3(tex3Dlod(_EGKLighting, float4((wp+NE)/divX, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE)/divX, 0.0)).w,
					tex3Dlod(_EGKLighting, float4((wp+NE.yxz)/divX, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE.yxz)/divX, 0.0)).w,
					tex3Dlod(_EGKLighting, float4((wp+NE.zyx)/divX, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE.yzx)/divX, 0.0)).w);
	#endif
	#if defined(EGK_LODCASCADE_OFF)
	return float3(tex3Dlod(_EGKLighting, float4((wp+NE)/_EGKParams.x, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE)/_EGKParams.x, 0.0)).w,
					tex3Dlod(_EGKLighting, float4((wp+NE.yxz)/_EGKParams.x, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE.yxz)/_EGKParams.x, 0.0)).w,
					tex3Dlod(_EGKLighting, float4((wp+NE.zyx)/_EGKParams.x, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE.yzx)/_EGKParams.x, 0.0)).w);
	#endif

	return float3(0, 1, 0);
}
float3 distanceNormalLight2(float3 wp) {
	const float3 NE = float3(3.75, 0.0, 0.0);
#if defined(EGK_LODCASCADE_ON)
	float3 divX = _EGKParams.x*float3(2, 1, 1);
	return float3(tex3Dlod(_EGKLighting, float4((wp + NE) / divX, 0.0)).w* - tex3Dlod(_EGKLighting, float4((wp - NE) / divX, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.yxz) / divX, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yxz) / divX, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.zyx) / divX, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yzx) / divX, 0.0)).w);
#endif
#if defined(EGK_LODCASCADE_OFF)
	return float3(tex3Dlod(_EGKLighting, float4((wp + NE) / _EGKParams.x, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE) / _EGKParams.x, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.yxz) / _EGKParams.x, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yxz) / _EGKParams.x, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.zyx) / _EGKParams.x, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yzx) / _EGKParams.x, 0.0)).w);
#endif
	return float3(0, 1, 0);

}
//normal in light volume
float3 lightNormalLight(float3 wp) {
	const float3 NE = float3(1.0, 0.0, 0.0);
#if defined(EGK_LODCASCADE_ON)
	float3 divX = _EGKParams.x*float3(2, 1, 1);
	return float3(tex3Dlod(_EGKLighting, float4((wp + NE) / divX, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE) / divX, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.yxz) / divX, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yxz) / divX, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.zyx) / divX, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yzx) / divX, 0.0)).w);
#endif
#if defined(EGK_LODCASCADE_OFF)
	return float3(length(tex3Dlod(_EGKLighting, float4((wp + NE) / _EGKParams.x, 0.0)).xyz) - length(tex3Dlod(_EGKLighting, float4((wp - NE) / _EGKParams.x, 0.0)).xyz),
		length(tex3Dlod(_EGKLighting, float4((wp + NE.yxz) / _EGKParams.x, 0.0)).xyz) - length(tex3Dlod(_EGKLighting, float4((wp - NE.yxz) / _EGKParams.x, 0.0)).xyz),
		length(tex3Dlod(_EGKLighting, float4((wp + NE.zyx) / _EGKParams.x, 0.0)).xyz) - length(tex3Dlod(_EGKLighting, float4((wp - NE.yzx) / _EGKParams.x, 0.0)).xyz));
#endif
	return float3(0, 1, 0);

}

//normal in distance volume, wp is position in voxel space
float3 distanceNormalLightOffset(float3 wp) {
	const float3 NE = float3(1.0,0.0,0.0);
	#if defined(EGK_LODCASCADE_ON)
	float3 divX = _EGKParams.x*float3(2,1,1);
	float cval = 1-1/_EGKParams.x;
	const float3 shift = float3(0.5,0,0);
	return float3(tex3Dlod(_EGKLighting, float4(clamp((wp+NE)/divX,0,cval)+shift, 0.0)).w-tex3Dlod(_EGKLighting, float4(clamp((wp-NE)/divX,0,cval)+shift, 0.0)).w,
					tex3Dlod(_EGKLighting, float4(clamp((wp+NE.yxz)/divX,0,cval)+shift, 0.0)).w-tex3Dlod(_EGKLighting, float4(clamp((wp-NE.yxz)/divX,0,cval)+shift, 0.0)).w,
					tex3Dlod(_EGKLighting, float4(clamp((wp+NE.zyx)/divX,0,cval)+shift, 0.0)).w-tex3Dlod(_EGKLighting, float4(clamp((wp-NE.yzx)/divX,0,cval)+shift, 0.0)).w);
	#endif
	#if defined(EGK_LODCASCADE_OFF)
	return float3(tex3Dlod(_EGKLighting, float4((wp+NE)/_EGKParams.x, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE)/_EGKParams.x, 0.0)).w,
					tex3Dlod(_EGKLighting, float4((wp+NE.yxz)/_EGKParams.x, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE.yxz)/_EGKParams.x, 0.0)).w,
					tex3Dlod(_EGKLighting, float4((wp+NE.zyx)/_EGKParams.x, 0.0)).w-tex3Dlod(_EGKLighting, float4((wp-NE.yzx)/_EGKParams.x, 0.0)).w);
	#endif
	return float3(0, 1, 0);

}
//normal in distance volume, wp is position in voxel space
float3 distanceNormalLightOffset2(float3 wp) {
	const float3 NE = float3(4.0, 0.0, 0.0);
#if defined(EGK_LODCASCADE_ON)
	float3 divX = _EGKParams.x*float3(2, 1, 1);
	float cval = 1 - 1 / _EGKParams.x;
	const float3 shift = float3(0.5, 0, 0);
	return float3(tex3Dlod(_EGKLighting, float4(clamp((wp + NE) / divX, 0, cval) + shift, 0.0)).w - tex3Dlod(_EGKLighting, float4(clamp((wp - NE) / divX, 0, cval) + shift, 0.0)).w,
		tex3Dlod(_EGKLighting, float4(clamp((wp + NE.yxz) / divX, 0, cval) + shift, 0.0)).w - tex3Dlod(_EGKLighting, float4(clamp((wp - NE.yxz) / divX, 0, cval) + shift, 0.0)).w,
		tex3Dlod(_EGKLighting, float4(clamp((wp + NE.zyx) / divX, 0, cval) + shift, 0.0)).w - tex3Dlod(_EGKLighting, float4(clamp((wp - NE.yzx) / divX, 0, cval) + shift, 0.0)).w);
#endif
#if defined(EGK_LODCASCADE_OFF)
	return float3(tex3Dlod(_EGKLighting, float4((wp + NE) / _EGKParams.x, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE) / _EGKParams.x, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.yxz) / _EGKParams.x, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yxz) / _EGKParams.x, 0.0)).w,
		tex3Dlod(_EGKLighting, float4((wp + NE.zyx) / _EGKParams.x, 0.0)).w - tex3Dlod(_EGKLighting, float4((wp - NE.yzx) / _EGKParams.x, 0.0)).w);
#endif
	return float3(0, 1, 0);

}
