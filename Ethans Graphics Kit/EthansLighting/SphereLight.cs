﻿//Ethan Alexander Shulman 2017

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

namespace EthansGraphicsKit.Lighting
{
    /// <summary>
    /// Sphere light.
    /// </summary>
    public class SphereLight : MonoBehaviour
    {

        /// <summary>
        /// The color of the light.
        /// </summary>
        [Tooltip("The color of the light.")]
        public Color color = Color.white;

        /// <summary>
        /// The strength/intensity of the light.
        /// </summary>
        [Tooltip("The strength/intensity of the light.")]
        [Range(0.1f, 1000.0f)]
        public float intensity = 10.0f;

        /// <summary>
        /// If enabled it will not contribute to the light map and only apply the dynamic lighting.
        /// </summary>
        [Tooltip("If enabled it will not contribute to the light map and only apply the dynamic lighting.")]
        public bool dynamicOnly = false;

        /// <summary>
        /// Cookie texture for sphere light.
        /// </summary>
        [Tooltip("Cookie texture for sphere light.")]
        public Texture2D cookieTexture = null;

        /// <summary>
        /// Cookie texture scale and rotation for sphere light(x,y = x,y scale and z,w = rotation angles in radians.
        /// </summary>
        [Tooltip("Cookie texture scale and rotation for sphere light(x,y = x,y scale and z,w = rotation angles in radians.")]
        public Vector4 cookieTextureTransform = new Vector4(1, 1, 0, 0);


        private static List<SphereLight> sphereLightsList = new List<SphereLight>();

        void OnEnable()
        {
            sphereLightsList.Add(this);
            if (LightingSystem.lightingSystem != null) LightingSystem.lightingSystem.UpdateLightingPass();
        }

        void OnDisable()
        {
            sphereLightsList.Remove(this);
            if (LightingSystem.lightingSystem != null) LightingSystem.lightingSystem.UpdateLightingPass();
        }

        /// <summary>
        /// Get color multiplied by intensity divided by 40.
        /// </summary>
        /// <returns></returns>
        public Color GetColorWithIntensity()
        {
            return color * (intensity / 40.0f);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = color;
            Gizmos.DrawWireSphere(transform.position,0.05f);
        }

        /// <summary>
        /// Get list of all SphereLight's.
        /// </summary>
        /// <returns></returns>
        public static List<SphereLight> GetSphereLights()
        {
            return sphereLightsList;
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/Ethans Graphics Kit/Sphere Light", false, 10)]
        static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            GameObject go = new GameObject("Sphere Light");
            go.AddComponent<SphereLight>();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
#endif
    }
}