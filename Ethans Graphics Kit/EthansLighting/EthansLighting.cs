﻿//Ethan Alexander Shulman 2017

using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace EthansLighting
{
    public class EthansLighting : MonoBehaviour
    {
        public static EthansLighting ethansLighting;


        public int globalIlluminationVolumeResolution = 32;
        public float globalIlluminationVolumeSize = 100.0f;
        [Range(0.0f,1.0f)]
        public float globalIlluminationUpdateSmoothness = 0.9f;
        public LayerMask globalIlluminationCullingMask = -1;

        [Space(20)]
        public int reflectionResolution = 32;
        public LayerMask reflectionCullingMask = -1;
        public float reflectionUpdateDelay = 0.1f;
        [Range(0.0f,1.0f)]
        public float reflectionUpdateSmoothness = 0.9f;

        [Space(20)]
        public Color fogColor = Color.black;
        public float fogDepth = 1000.0f;
        [Range(0.0f, 1.0f)]
        public float fogIntensity = 1.0f;
        public float fogPower = 1.0f;


        [HideInInspector]
        public Material reflectionShader, giShader;

        [HideInInspector]
        public Transform reflectionOrigin,
                         globalIlluminationOrigin;

        private Transform giCameraTransform;
        private Camera reflectionCamera, giCamera;
        private RenderTexture reflectionCubemap, reflectionTexture, giSliceTexture, giTexture, giTexture2;
        private CommandBuffer giCb;
        private int giX,giY,giZ;
        private float giVoxel;

        private int SliceOriginID, SliceMatrixID;


        void Awake()
        {
            SliceOriginID = Shader.PropertyToID("_SliceOrigin");
            SliceMatrixID = Shader.PropertyToID("_SliceMatrix");
        }

        void OnEnable() {
            ethansLighting = this;

            //allocate textures
            reflectionCubemap = new RenderTexture(reflectionResolution, reflectionResolution, 0, RenderTextureFormat.ARGB32);
            reflectionCubemap.isCubemap = true;
            reflectionTexture = new RenderTexture(reflectionResolution*4, reflectionResolution*4, 0, RenderTextureFormat.ARGB32);
            reflectionTexture.useMipMap = true;
            reflectionTexture.wrapMode = TextureWrapMode.Repeat;
            Shader.SetGlobalTexture("_ELReflection", reflectionTexture);

            giSliceTexture = new RenderTexture(globalIlluminationVolumeResolution, globalIlluminationVolumeResolution, 0, RenderTextureFormat.ARGB32);
            giTexture = new RenderTexture(globalIlluminationVolumeResolution * globalIlluminationVolumeResolution, globalIlluminationVolumeResolution, 0, RenderTextureFormat.ARGB32);
            giTexture2 = new RenderTexture(globalIlluminationVolumeResolution * globalIlluminationVolumeResolution, globalIlluminationVolumeResolution, 0, RenderTextureFormat.ARGB32);
            Shader.SetGlobalTexture("_ELGI", giTexture);
            Shader.SetGlobalFloat("_ELGIOnly", 0.0f);

            //setup reflection sphere rendering
            Camera mc = Camera.main;
            GameObject cgo = new GameObject("ELReflectionCamera");
            cgo.SetActive(false);
            reflectionCamera = cgo.AddComponent<Camera>();
            reflectionCamera.CopyFrom(mc);
            reflectionCamera.cullingMask = reflectionCullingMask;

            reflectionOrigin = new GameObject("ELReflectionOrigin").transform;
            reflectionOrigin.SetParent(mc.transform, false);

            //setup global illumination rendering
            cgo = new GameObject("ELGICamera");
            giCamera = cgo.AddComponent<Camera>();
            giCamera.CopyFrom(mc);
            giCamera.cullingMask = globalIlluminationCullingMask;
            giCamera.orthographic = true;
            giCamera.orthographicSize = globalIlluminationVolumeSize*0.5f;
            giCamera.nearClipPlane = 0.1f;
            giCamera.farClipPlane = globalIlluminationVolumeSize;
            giCamera.targetTexture = giSliceTexture;
            giCameraTransform = giCamera.transform;

            globalIlluminationOrigin = new GameObject("ELGIOrigin").transform;
            globalIlluminationOrigin.SetParent(mc.transform, false);

            giVoxel = globalIlluminationVolumeSize / globalIlluminationVolumeResolution;
            Shader.SetGlobalVector("_VolumeParams", new Vector3(globalIlluminationVolumeResolution, globalIlluminationVolumeResolution*globalIlluminationVolumeResolution, giVoxel));
            Shader.SetGlobalVector("_VolumeOrigin", Vector3.one * globalIlluminationVolumeSize * -0.5f);

            giCb = new CommandBuffer();
            giCb.name = "GI";
            giShader.SetFloat("_Alpha", 1.0f-globalIlluminationUpdateSmoothness);
            giShader.SetTexture("_MainTex", giSliceTexture);
            giCb.Blit((Texture)null, giTexture, giShader, 0);
            giCamera.AddCommandBuffer(CameraEvent.AfterForwardAlpha, giCb);
            cgo.AddComponent<GlobalIlluminationCamera>();

            ApplySettings();
        }

        void Start()
        {
            StartCoroutine(autoUpdateReflection());
        }

        void OnDisable()
        {
            GameObject.Destroy(reflectionCamera);
            GameObject.Destroy(giCamera);
            if (reflectionOrigin != null) GameObject.Destroy(reflectionOrigin.gameObject);
            if (globalIlluminationOrigin != null) GameObject.Destroy(globalIlluminationOrigin.gameObject);
            GameObject.DestroyImmediate(reflectionCubemap);
            GameObject.DestroyImmediate(reflectionTexture);
            GameObject.DestroyImmediate(giTexture);
            GameObject.DestroyImmediate(giTexture2);
            GameObject.DestroyImmediate(giSliceTexture);
            giCb.Dispose();
#if UNITY_EDITOR
            Shader.SetGlobalTexture("_ELReflection", EditorGUIUtility.whiteTexture);
            Shader.SetGlobalTexture("_ELGI", EditorGUIUtility.whiteTexture);
#endif
        }


        private IEnumerator autoUpdateReflection()
        {
            while (true)
            {
                float delay = reflectionUpdateDelay / 7.0f;

                reflectionCamera.transform.position = reflectionOrigin.position;
                for (int i = 0; i < 6; i++)
                {
                    reflectionCamera.RenderToCubemap(reflectionCubemap, 1 << i);
                    yield return new WaitForSeconds(delay);
                }
             
                reflectionShader.SetTexture("_MainCube", reflectionCubemap);
                reflectionShader.SetFloat("_Alpha", 1.0f-reflectionUpdateSmoothness);
                Graphics.Blit(null, reflectionTexture, reflectionShader, 0);

                yield return new WaitForSeconds(delay);
            }
        }

        public void UpdateGI()
        {
            //update gi volume position
            Vector3 cpos = globalIlluminationOrigin.position;
            int x = Mathf.FloorToInt(cpos.x / giVoxel),
                y = Mathf.FloorToInt(cpos.y / giVoxel),
                z = Mathf.FloorToInt(cpos.z / giVoxel),
                ox = x - giX,
                oy = y - giY,
                oz = z - giZ;

            if (ox * ox + oy * oy + oz * oz > globalIlluminationVolumeResolution * 0.5f)
            {
                giX = x;
                giY = y;
                giZ = z;
                Shader.SetGlobalVector("_VolumeOrigin", new Vector3(giX, giY, giZ) * giVoxel - Vector3.one * globalIlluminationVolumeSize * 0.5f);

                giShader.SetVector("_Offset", new Vector3(ox, oy, oz));
                Graphics.Blit(giTexture, giTexture2, giShader, 1);

                RenderTexture buf = giTexture;
                giTexture = giTexture2;
                giTexture2 = buf;

                Shader.SetGlobalTexture("_ELGI", giTexture);
                giCb.Clear();
                giCb.Blit((Texture)null, giTexture, giShader, 0);
                giShader.SetTexture("_MainTex", giSliceTexture);
            }

            //randomize gi slice 
            Quaternion rdir = Random.rotationUniform;
            Vector3 so = globalIlluminationOrigin.position + (rdir * Vector3.forward) * (Random.value - 0.5f) * globalIlluminationVolumeSize;
            giCameraTransform.localPosition = so;
            giCameraTransform.localRotation = rdir;
            giShader.SetVector(SliceOriginID, so);
            giShader.SetMatrix(SliceMatrixID, giCamera.cameraToWorldMatrix.transpose);
        }

        public void ApplySettings()
        {
            Shader.SetGlobalColor("_ELFogColor", fogColor);
            Shader.SetGlobalVector("_ELFogParams", new Vector3(fogDepth, fogIntensity, fogPower));
        }


        public int GetGPUMemoryUsage()
        {
            return ((int)Mathf.Pow(globalIlluminationVolumeResolution, 3.0f) * 2 + (int)Mathf.Pow(globalIlluminationVolumeResolution, 2.0f) +
                   (int)Mathf.Pow(reflectionResolution, 2.0f) * 6 + (int)Mathf.Pow(reflectionResolution * 4, 2.0f) * 6)*4;
        }


#if UNITY_EDITOR
        private long autoSettingsUpdateEditor = 0;
        void OnDrawGizmosSelected()
        {
            //visualize gi volume
            Gizmos.color = Color.yellow;
            if (globalIlluminationOrigin == null) Gizmos.DrawWireCube(transform.position, Vector3.one * globalIlluminationVolumeSize);
            else
            {
                Vector3 p = globalIlluminationOrigin.position;
                p.x = Mathf.FloorToInt(p.x / giVoxel) * giVoxel;
                p.y = Mathf.FloorToInt(p.y / giVoxel) * giVoxel;
                p.z = Mathf.FloorToInt(p.z / giVoxel) * giVoxel;
                Gizmos.DrawWireCube(p, Vector3.one * globalIlluminationVolumeSize);
            }

            //auto apply settings in editor
            long time = System.DateTime.Now.Ticks / 10000;
            if (time - autoSettingsUpdateEditor > 1000)
            {
                ApplySettings();
                autoSettingsUpdateEditor = time;
            }
        }
#endif
    }
}