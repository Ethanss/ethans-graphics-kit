﻿//Ethan Alexander Shulman 2016
Shader "Hidden/EGKDeferredLightingPass"
{
	Properties {
		[HDR] _RenderBuffer ("Render Buffer", 2D) = ""
	}

	SubShader
	{
		LOD 100


		//computed gi progressive deferred lighting pass, pass id = 0
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma target 5.0

			
			//#pragma multi_compile EGK_HARDSHADOWSOFF EGK_HARDSHADOWSON
			#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			#pragma multi_compile EGK_RTFINALGATHER_OFF EGK_RTFINALGATHER_ON


			#include "UnityCG.cginc"
			#include "EGKVolume.cginc"

			sampler2D_float _RenderBuffer, _LastRenderBuffer, _LastCameraDepthTexture;
			sampler2D_half _LastFrame;


			float3 polarToCartesian(float sinTheta, float cosTheta, 
									float sinPhi, float cosPhi) {
				return float3(sinTheta * cosPhi, sinTheta * sinPhi, cosTheta);
			}
			void calculateTangentAndBinormal(float3 normal, out float3 tangent, out float3 binormal)
			{
				if (abs(normal.x) > abs(normal.y))
				{
					tangent = normalize(float3(-normal.z, 0., normal.x));
				}
				else
				{
					tangent = normalize(float3(0., normal.z, -normal.y));
				}
    
				binormal = cross(normal, tangent);
			}
			float3 brdf(float3 seed, float3 norm, float rough, float3 view, float3 tangent, float3 binormal) {
				const float TWO_PI = 6.28318;
           
				float3 rnd = hash33(seed);
				
				float cosTheta = pow(max(0., rnd.x), 1.0-rough*0.999);
				float sinTheta = sqrt(max(0., 1. - cosTheta * cosTheta));
				float phi = rnd.y * TWO_PI;
    
				float3 ldir = polarToCartesian(sinTheta, cosTheta, sin(phi), cos(phi));

				float3 dir = ldir.x*tangent + ldir.y*binormal + ldir.z*norm;
				return reflect(view, dir*(step(0.,dot(dir,-view))*2.-1.));
			}
			float3 montecarloBRDF(float3 seed,
								  float3 norm,
								  float specExp,
								  float3 incdir,
								  float3 tangent,
								  float3 binormal)
			{
				const float TWO_PI = 6.28318;
           
				float3 u12 = hash33(seed);
    
				float cosTheta = pow(max(0., u12.x), 1./(specExp+1.));
				float sinTheta = sqrt(max(0., 1. - cosTheta * cosTheta));
				float phi = u12.y * TWO_PI;
    
				float3 whLocal = polarToCartesian(sinTheta, cosTheta, sin(phi), cos(phi));

				float3 wh = whLocal.x * tangent + whLocal.y * binormal + whLocal.z * norm;
    
				float3 wo = -incdir;
				if (dot(wo, wh) < 0.)
				{
				   wh *= -1.;
				}
            
				return reflect(incdir, wh);
			}
			float3 randomHemisphereDirection(float2 r, float3 n) {
				float3 uu = normalize(cross(n, float3(0.0, 1.0, 1.0)));
				float3 vv = cross(uu, n);

				float ra = sqrt(r.y);
				float rx = ra*cos(6.2831*r.x);
				float ry = ra*sin(6.2831*r.x);
				float rz = sqrt(1.0 - r.y);
				float3  rr = float3(rx*uu + ry*vv + rz*n);

				return normalize(rr);
			}

			float4 frag (v2f_img i) : SV_Target
			{
			
				float2 uv = i.uv;
				

				//setup deferred data, gbuffer, depth/world pos, ray dir
				float depthSamp = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));
				float4 matbuf = tex2D(_RenderBuffer, i.uv);

				float3 worldPos = mul(_EGKCameraToWorld, float4(float3((i.uv * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * depthSamp, 1)).xyz,
					   rayDir = normalize(worldPos-_WorldSpaceCameraPos);

				fixed3 diff = decodeFixed3(matbuf.x),
					   spec = decodeFixed3(matbuf.y),
					   mre = decodeFixed3(matbuf.z),
					   norm = normalize(decodeFixed3(matbuf.w)*2.0-1.0);

				float specExp = floor(max(1., (1. - pow(mre.y, 1.0/10.0)) * 4e3));
				
				fixed3 tang,binorm;
				calculateTangentAndBinormal(norm,tang,binorm);

				float3 rd = norm;
				worldPos -= _EGKOrigin;
				worldPos /= _EGKParams.z;

				float3 wpos = worldPos+norm*1.5;

				if (depthSamp/(_ProjectionParams.z-_ProjectionParams.y) < .999) {//check if background


					float3 lmapsamp = 0.0;
					#if defined(EGK_LODCASCADE_ON)
					float cascadeBase;
					float3 cascadeDst = abs(wpos/_EGKParams.x-0.5),
							cascadePos = wpos;
					if (max(cascadeDst.x,max(cascadeDst.y,cascadeDst.z)) > 0.5-0.5/_EGKParams.z) {
						cascadeBase = 0.5;
						cascadePos = (wpos-_EGKParams.x/2.0)/(_EGKParams.w/_EGKParams.z)+_EGKParams.x/2.0+norm;
					} else {
						cascadeBase = 0.0;
					}
					#if defined(EGK_RTFINALGATHER_OFF)
					float3 ps = (cascadePos + norm*2.0) / _EGKParams.x;
					lmapsamp = pow(tex3Dlod(_EGKLighting, float4(ps.x*0.5+cascadeBase, ps.y, ps.z, 1.)).xyz,2.2);
					#endif
					#endif
					#if defined(EGK_LODCASCADE_OFF)
					#if defined(EGK_RTFINALGATHER_OFF)
					lmapsamp = pow(tex3Dlod(_EGKLighting, float4((wpos + norm*2.0) / _EGKParams.x, 1.)).xyz,2.2);
					#endif
					#endif


					//using distance field derivative to move ray outside of voxel so it can reflection trace freely
					#if defined(EGK_LODCASCADE_ON)
					if (cascadeBase < 0.3) {
						float3 dnorm = normalize(distanceNormalLight(wpos));
						if (abs(length(dnorm)-1.) < 1e-3) wpos += dnorm*sign(max(0.,dot(dnorm,norm)));
					
						dnorm = normalize(distanceNormalLight(wpos));
						if (abs(length(dnorm)-1.) < 1e-3) wpos += dnorm*sign(max(0.,dot(dnorm,norm)));
					} else {
						wpos += norm*1.5*(_EGKParams.w/_EGKParams.z);
					}
					#endif

					#if defined(EGK_LODCASCADE_OFF)
					float3 dnorm = normalize(distanceNormalLight(wpos));
					if (abs(length(dnorm)-1.) < 1e-3) wpos += dnorm*sign(max(0.,dot(dnorm,norm)));
					
					dnorm = normalize(distanceNormalLight(wpos));
					if (abs(length(dnorm)-1.) < 1e-3) wpos += dnorm*sign(max(0.,dot(dnorm,norm)));
					#endif


					float3 reflsamp = 0.0, startPos = wpos;
					#if defined(EGK_RTFINALGATHER_OFF)
					int npaths = max(1.0, _EGKGIParams.y*pow(mre.y,1.0/2.0));
					if (mre.y > 0.985) npaths = 0;
					#endif
					#if defined(EGK_RTFINALGATHER_ON)
					float3 difsamp = 0.0;
					int difpaths = ceil((0.5-mre.x*0.5)*_EGKGIParams.y),
						npaths = difpaths+(int)max(1.0,_EGKGIParams.y*(0.5+mre.x*0.5)*pow(mre.y,1.0/1.5));
					#endif
					for (int b = 0; b < npaths; b++) {
						//random ray dir and type
						float3 rseed = float(b+1)*0.023845+float(b+1)*2.984*wpos+cos(wpos+_Time*12.239480+_Time.zwxy*.18594)*32.54984;
						

						float s = 0.0;

						#if defined(EGK_RTFINALGATHER_ON)
						float rexp = 1.0;
						if (b >= difpaths) {
							rexp = specExp;
						}
						rd = montecarloBRDF(rseed, norm, rexp, rayDir, tang, binorm);
						//rd = brdf(rseed, norm, rexp, rayDir, tang, binorm);
						#endif
						#if defined(EGK_RTFINALGATHER_OFF)
						rd = montecarloBRDF(rseed, norm, specExp, rayDir, tang, binorm);
						//rd = brdf(rseed, norm, mre.y, rayDir, tang, binorm);
						#endif
						
						//trace with second level of detail cascade if LOD_CASCADE enabled
						#if defined(EGK_LODCASCADE_ON)
						wpos = startPos;

						for (int i = 0; i < _EGKGIParams.x; i++) {
							float3 pos = (wpos+rd*s)/_EGKParams.x;
							if (max(pos.x,max(pos.y,pos.z)) >= 1-1/_EGKParams.x || min(pos.x,min(pos.y,pos.z)) < 1/_EGKParams.x) break;//check if out of bounds

							float dst = tex3Dlod(_EGKLighting, float4(float3(pos.x*0.5,pos.y,pos.z), 0)).w*16.0;
							if (dst < 1.25) break;

							s += dst;
						}

						//if out of bounds, do second LOD trace
						float3 pos = (wpos+rd*s)/_EGKParams.x;
						if (max(pos.x, max(pos.y, pos.z)) >= 1-1/_EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1/_EGKParams.x) {
		
							//low res second cascade trace
							pos -= 0.5;
							if (s > 0.) {
								float3 ap = abs(pos) - (0.5 - 0.5 / _EGKParams.x);
								float ma;
								if (ap.x > ap.y) {
									if (ap.x > ap.z) {
										ma = ap.x / abs(rd.x);
									}
									else {
										ma = ap.z / abs(rd.z);
									}
								}
								else {
									if (ap.y > ap.z) {
										ma = ap.y / abs(rd.y);
									}
									else {
										ma = ap.z / abs(rd.z);
									}
								}
								pos -= rd*ma;
							}
							wpos = ((pos) / (_EGKParams.w / _EGKParams.z) + 0.5)*_EGKParams.x;
							s = 0.0;

							float3 spos = wpos / _EGKParams.x,
									sdst = abs(spos-0.5);
							if ((cascadeBase > 0.0 || tex3Dlod(_EGKLighting, float4(float3(spos.x*0.5+0.5, spos.y, spos.z), 0)).w*16.0 < 1.25) && max(sdst.x,max(sdst.y,sdst.z)) < 0.5) {
								float3 dnorm = normalize(distanceNormalLightOffset(wpos));
								if (abs(length(dnorm)-1.) < 1e-3) wpos += dnorm*sign(max(0.,dot(dnorm,norm)));
					
								dnorm = normalize(distanceNormalLightOffset(wpos));
								if (abs(length(dnorm)-1.) < 1e-3) wpos += dnorm*sign(max(0.,dot(dnorm,norm)));
							}

							for (int i = 0; i < _EGKGIParams.x; i++) {
								float3 pos = (wpos + rd*s) / _EGKParams.x;
								if (max(pos.x, max(pos.y, pos.z)) > 1-1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1/_EGKParams.x) break;//check if out of bounds

								float dst = tex3Dlod(_EGKLighting, float4(float3(pos.x*0.5+0.5, pos.y, pos.z), 0)).w*16.0;
								if (dst < 1.25) break;

								s += dst;
							}

							//if out of bounds, apply sky light and one dir light
							float3 lam = lerp(_EGKBackgroundColor.xyz, pow(texCUBElod(_EGKBackgroundCube, float4(rd, 0.0)).xyz,2.2),
								_EGKBackgroundType)*_EGKBackgroundColor.w +//ambient sky light
								0.0;

							float3 pos = (wpos + rd*s) / _EGKParams.x;

							float dst = tex3Dlod(_EGKLighting, float4( float3(pos.x*0.5+0.5,pos.y,pos.z), 0)).w*16.0;

							wpos += rd*(s - (2.0-dst));
							wpos = clamp(wpos / _EGKParams.x, 0, 1 - 1 / _EGKParams.x);
							wpos.x = 0.5 + wpos.x*0.5;

							//float dst = tex3Dlod(_EGKLighting, float4(wpos, 0)).w*16.0;

							fixed4 mat = tex3Dlod(_EGKMaterial, float4(wpos, 0));
							//if (dst > 1.25) mat.xyz = 0.0;
							float bgmask = 0.0;
							if (max(pos.x,max(pos.y,pos.z)) > 1.0-1/_EGKParams.x || min(pos.x,min(pos.y,pos.z)) < 1/_EGKParams.x) {
								bgmask = 1.0;
							} else if (dst >= 1.25) {
								mat.xyz = 0.0;
							}
							mat.xyz = lerp(mat.xyz*(floor(mat.w*256.0 - 1.0) / 127.0 +//material emission
								pow(tex3Dlod(_EGKLighting, float4(wpos, 0)).xyz,2.2)),//computed lightmap emission
								lam,
								bgmask);

							#if defined(EGK_RTFINALGATHER_OFF)
							reflsamp += mat.xyz;
							#endif
							#if defined(EGK_RTFINALGATHER_ON)
							if (b >= difpaths) reflsamp += mat.xyz;
							else difsamp += mat.xyz;
							#endif
						} else {
							float dst = tex3Dlod(_EGKLighting, float4( float3(pos.x*0.5,pos.y,pos.z), 0)).w*16.0;
							//reflection hit
							if (dst < 1.25) {
								wpos += rd*(s-(2.0-dst));
								wpos = clamp(wpos/_EGKParams.x, 0, 1-1/_EGKParams.x);
								wpos.x *= 0.5;

								fixed4 mat = tex3Dlod(_EGKMaterial, float4(wpos, 0));
								mat.xyz = mat.xyz*(floor(mat.w*256.0-1.0)/127.0+//material emission
											 pow(tex3Dlod(_EGKLighting, float4(wpos, 0)).xyz,2.2));//computed lightmap emission

								#if defined(EGK_RTFINALGATHER_OFF)
								reflsamp += mat.xyz;
								#endif
								#if defined(EGK_RTFINALGATHER_ON)
								if (b >= difpaths) reflsamp += mat.xyz;
								else difsamp += mat.xyz;
								#endif							
							}
						}
						#endif
						


						//normal tracing without lod cascade
						#if defined(EGK_LODCASCADE_OFF)
						wpos = startPos;//+distanceNormalLight(worldPos);

						for (int i = 0; i < _EGKGIParams.x; i++) {
							float3 pos = (wpos+rd*s)/_EGKParams.x;
							if (max(pos.x,max(pos.y,pos.z)) > 1.0-1.0/_EGKParams.x || min(pos.x,min(pos.y,pos.z)) < 1/_EGKParams.x) break;//check if out of bounds

							float dst = tex3Dlod(_EGKLighting, float4(pos, 0)).w*16.0;
							if (dst < 1.25) break;

							s += dst;
						}

						float3 tpos = (wpos+rd*s)/_EGKParams.x;

						//if out of bounds, apply sky light and one dir light
						float dst = tex3Dlod(_EGKLighting, float4(tpos, 0)).w*16.0;

						wpos += rd*(s - (2.0 - dst));
						wpos /= _EGKParams.x;

						fixed4 mat = tex3Dlod(_EGKMaterial, float4(wpos, 0));
						float bgmask = max(tpos.x, max(tpos.y, tpos.z)) > 1.0 - 1.0 / _EGKParams.x || min(tpos.x, min(tpos.y, tpos.z)) < 1 / _EGKParams.x;
						mat.xyz *= dst < 1.25;

						mat.xyz = lerp(mat.xyz*(floor(mat.w*256.0-1.0)/127.0+//material emission
									 pow(tex3Dlod(_EGKLighting, float4(wpos, 0)).xyz,2.2)),//computed lightmap emission
									 lerp(_EGKBackgroundColor.xyz, pow(texCUBElod(_EGKBackgroundCube, float4(rd, 0.0)).xyz,2.2),
									 _EGKBackgroundType)*_EGKBackgroundColor.w,
									bgmask);
						
						
							#if defined(EGK_RTFINALGATHER_OFF)
							reflsamp += mat.xyz;
							#endif
							#if defined(EGK_RTFINALGATHER_ON)
							if (b >= difpaths) reflsamp += mat.xyz;
							else difsamp += mat.xyz;
							#endif
						#endif
						
					}



					#if defined(EGK_RTFINALGATHER_OFF)
					if (mre.y > 0.985) npaths = 1;
					 float3 o = diff*lmapsamp*(0.5-mre.x*0.5) +
						       spec*(
									lerp(reflsamp/(float)npaths, lmapsamp, pow(clamp((mre.y-0.9)*10.01,0.0,1.0),5.)))*(0.5+mre.x*0.5) +
									lerp(diff, spec, 0.5 + mre.x*0.5)*mre.z;
					#endif
					#if defined(EGK_RTFINALGATHER_ON)
					float3 o = diff*(difsamp/max(1.0,difpaths))*(0.5-mre.x*0.5) +
						      spec*(reflsamp/(float)(npaths-difpaths))*(0.5+mre.x*0.5) +
								 lerp(diff,spec,0.5+mre.x*0.5)*mre.z;
					#endif
					
					return float4(o, 1.0);
				} else {
					return float4(lerp(_EGKBackgroundColor.xyz, pow(texCUBElod(_EGKBackgroundCube, float4(rayDir, 0.0)).xyz,2.2), _EGKBackgroundType), 1.0);
				}
			}
			ENDCG
		}


		//copy hdr render texture, pass id = 1
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			
			#include "UnityCG.cginc"


			sampler2D_float _RenderBuffer;
			
			float4 frag (v2f_img i) : SV_Target
			{
				float4 col = tex2D(_RenderBuffer, i.uv);
				return col;
			}
			ENDCG
		}

		//copy scene render texture and recode depth,  pass id = 2
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			
			#include "UnityCG.cginc"
			#include "EGKVolume.cginc"

			sampler2D_float _RenderBuffer, _LastCameraDepthTexture;
			
			float4 frag (v2f_img i) : SV_Target
			{
				float4 col = tex2D(_RenderBuffer, i.uv);
				col.x = encodeFixed3(decodeFixed3(col.x)*0.5+decodeFixed3(col.y)*0.5);//combine diff and spec
				col.y = col.z;
				col.z = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));
				return col;
			}
			ENDCG
		}


		//convolution x for traced buffer, pass id = 3
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma target 5.0
			
			#include "UnityCG.cginc"
			#include "EGKVolume.cginc"

			sampler2D_float _RenderBuffer, _LastCameraDepthTexture, _LastRenderBuffer;
			sampler2D_half _LastFrame;

			
			float4 frag (v2f_img i) : SV_Target
			{
				float4 mat = tex2D(_RenderBuffer, i.uv);
				float depthSamp = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));

				fixed3 diff = decodeFixed3(mat.x),//gamma -> linear
					   spec = decodeFixed3(mat.y),//gamma -> linear
					   mre = decodeFixed3(mat.z),
					   norm = normalize(decodeFixed3(mat.w)*2.0-1.0);

				float4 sum = 0.0;
				
				//convolute
				for (int x = -4; x < 5; x++) {
					float2 suv = i.uv+float2((float)x/(float)_ScreenParams.x,0.0);

					float4 oldMat = tex2D(_RenderBuffer, suv);
					float dpth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, suv));
					fixed3 oldMre = decodeFixed3(oldMat.z);
						
					float alpha = ((max(0.0,dot(normalize(decodeFixed3(oldMat.w)*2.0-1.0), norm)-0.95)/(1.0-0.95))*max(0.0,1.0-length((decodeFixed3(oldMat.x)*0.5+decodeFixed3(oldMat.y)*0.5)-(diff*0.5+spec*0.5))*40.0)*max(0.0,1.0-length(oldMre.xy-mre.xy)*40.0)*
								  max(0.0, 1.0-abs(depthSamp-dpth)/(depthSamp*10.0)))*
								  max(0.0, 1.0-abs(float(x)/5.0));
					
					sum += tex2D(_LastFrame, suv)*alpha;
				}
				
				sum = lerp(sum, float4(tex2D(_LastFrame, i.uv).xyz, 1.0), floor(depthSamp/_ProjectionParams.z+_ProjectionParams.y));//dont blur background
				//sum = tex2D(_LastFrame, i.uv);
				return float4(sum.xyz/max(0.00001,sum.w), 1.0);
			}
			ENDCG
		}


		//convolution y for traced buffer, pass id = 4
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma target 5.0
			
			#include "UnityCG.cginc"
			#include "EGKVolume.cginc"

			sampler2D_float _RenderBuffer, _LastCameraDepthTexture, _LastRenderBuffer;
			sampler2D_half _LastFrame;

			
			float4 frag (v2f_img i) : SV_Target
			{
				float4 mat = tex2D(_RenderBuffer, i.uv);
				float depthSamp = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));

				fixed3 diff = decodeFixed3(mat.x),
					   spec = decodeFixed3(mat.y),
					   mre = decodeFixed3(mat.z),
					   norm = normalize(decodeFixed3(mat.w)*2.0-1.0);

				float4 sum = 0.0;
				
				//convolute
				for (int y = -4; y < 5; y++) {
					float2 suv = i.uv+float2(0.0,(float)y/(float)_ScreenParams.y);

					float4 oldMat = tex2D(_RenderBuffer, suv);
					float dpth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, suv));
					fixed3 oldMre = decodeFixed3(oldMat.z);
						
					float alpha = ((max(0.0,dot(normalize(decodeFixed3(oldMat.w)*2.0-1.0), norm)-0.95)/(1.0-0.95))*max(0.0,1.0-length((decodeFixed3(oldMat.x)*0.5+decodeFixed3(oldMat.y)*0.5)-(diff*0.5+spec*0.5))*40.0)*max(0.0,1.0-length(oldMre.xy-mre.xy)*40.0)*
								  max(0.0, 1.0-abs(depthSamp-dpth)/(depthSamp*10.0)))*
								  max(0.0, 1.0-abs(float(y)/5.0));
					
					sum += tex2D(_LastFrame, suv)*alpha;
				}

				sum = lerp(sum, float4(tex2D(_LastFrame, i.uv).xyz, 1.0), floor(depthSamp/_ProjectionParams.z+_ProjectionParams.y));
				//sum = tex2D(_LastFrame, i.uv);
				return float4(sum.xyz/max(0.00001,sum.w), 1.0);
			}
			ENDCG
		}



		//draw display texture to screen, pass id = 5
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma target 5.0
			
			#include "UnityCG.cginc"


			sampler2D_float _RenderBuffer;


			fixed4 frag (v2f_img i) : SV_Target
			{
				return fixed4(tex2D(_RenderBuffer, i.uv).xyz,1.0);
			}
			ENDCG
		}

		//copy, apply fog, scale, gamma correct hdr render texture, pass id = 6
		Pass
		{
			Tags {"Queue"="Transparent-1"}

			Fog { Mode Off }
			ZWrite Off
			ZTest Off
			Blend Off
			Cull Back

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma target 5.0
			
			#include "UnityCG.cginc"


			sampler2D_float _RenderBuffer, _LastCameraDepthTexture;
			float3 _EGKDBrightnessScale;
			float3 _EGKFogColor, _EGKFogParams;
			
			float4 frag (v2f_img i) : SV_Target
			{
				float4 col = tex2D(_RenderBuffer, i.uv);
				float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));
				if (depth/(_ProjectionParams.z-_ProjectionParams.y) < .999) {
					float depthSamp = min(1., depth/_EGKFogParams.x);
					return float4(pow(lerp(col.xyz/col.w, _EGKFogColor, pow(depthSamp, _EGKFogParams.z)*_EGKFogParams.y) * _EGKDBrightnessScale, 1.0/2.2),1.0);
				} else {
					return float4(col.xyz/col.w,1);
				}
			}
			ENDCG
		}


		//directional light, pass id = 7
		Pass
			{
				Tags{ "Queue" = "Transparent-1" }

				Fog{ Mode Off }
				ZWrite Off
				ZTest Off
				Blend Off
				Cull Back

				CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma target 5.0
				#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
				#pragma multi_compile EGK_RTFINALGATHER_OFF EGK_RTFINALGATHER_ON
				#pragma multi_compile EGK_SSDIRECTSHADOWS_OFF EGK_SSDIRECTSHADOWS_ON



				#include "UnityCG.cginc"
				#include "EGKVolume.cginc"


				sampler2D_float _RenderBuffer, _LastCameraDepthTexture;
				sampler2D_half _LastFrame;
				float4 _Light;
				float3 _LightColor;


				float2 _SSShadowParams;


				//GGX light BRDF 
				float G1V(float NdotV, float k)
				{
					return 1.0 / (NdotV*(1.0 - k) + k);
				}
				float SpecGGX(float3 N, float3 V, float3 L, float roughness, float F0)
				{
					float SqrRoughness = roughness / (1 + pow(roughness, 1.0 / 5.0)*(1 - roughness)*1e3);//max(5e-4,pow(roughness,2.0+roughness*6.0));

					float3 H = normalize(V + L);

					float NdotL = clamp(dot(N, L), 0.0, 1.0);
					float NdotV = clamp(dot(N, V), 0.0, 1.0);
					float NdotH = clamp(dot(N, H), 0.0, 1.0);
					float LdotH = clamp(dot(L, H), 0.0, 1.0);

					float RoughnessPow4 = SqrRoughness*SqrRoughness;
					float pi = 3.14159;
					float denom = NdotH * NdotH * (RoughnessPow4 - 1.0) + 1.0;
					float D = RoughnessPow4 / (pi * denom * denom);

					float LdotH5 = 1.0 - LdotH;
					LdotH5 = LdotH5*LdotH5*LdotH5*LdotH5*LdotH5;
					float F = F0 + (1.0 - F0)*(LdotH5);

					float specular = NdotL * D * F;

					return clamp(specular, 0.0, 1.0);
				}


				float4 frag(v2f_img i) : SV_Target
				{
					float2 uv = i.uv;

					//setup deferred data, gbuffer, depth/world pos, ray dir
					float depthSamp = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));

					float4 matbuf = tex2Dlod(_RenderBuffer, float4(i.uv,0,0));

					float3 wpos = mul(_EGKCameraToWorld, float4(float3((i.uv * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * depthSamp, 1)).xyz,
						rayDir = normalize(wpos - _WorldSpaceCameraPos),
						worldPos = wpos;

					fixed3 diff = decodeFixed3(matbuf.x),
						spec = decodeFixed3(matbuf.y),
						mre = decodeFixed3(matbuf.z),
						norm = normalize(decodeFixed3(matbuf.w)*2.0 - 1.0);

					float3 rd = normalize(-_Light.xyz);
					float dp = dot(rd, norm),
						dl = ceil(max(0.0, dp - 1e-6));

					if (floor(depthSamp/_ProjectionParams.z+_ProjectionParams.y) >= 1.0) {//check if background
						return tex2Dlod(_LastFrame, float4(uv, 0, 0));
					}
					
					if (dl > 0.) {
						wpos -= _EGKOrigin;
						wpos /= _EGKParams.z;

						wpos += norm*2.0;

						//trace light
						float s = 2.0;
						#if defined(EGK_LODCASCADE_ON)
						float ldst = 2.0;
						for (int t = 0; t < _EGKGIParams.x; t++) {
							float3 pos = (wpos + rd*s) / _EGKParams.x;
							if (max(pos.x, max(pos.y, pos.z)) > 1.0 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1/_EGKParams.x) break;//check if out of bounds

							float dst = tex3Dlod(_EGKLighting, float4(pos.x*0.5, pos.y, pos.z, 0)).w*16.0;
							if (dst < 1.25) {
								ldst = dst;
								break;
							}

							s += dst;
						}
						//second lod cascade trace
						float3 pos = (wpos + rd*s) / _EGKParams.x;
						if (ldst < 1.25) {
							dl = 0.0;
						}
						else if (max(pos.x, max(pos.y, pos.z)) > 1.0 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1/_EGKParams.x) {
							wpos = ((pos - 0.5) / (_EGKParams.w / _EGKParams.z) + 0.5);
							float3 spos = wpos;
							wpos *= _EGKParams.x;
							s = 0.0;
							if (tex3Dlod(_EGKLighting, float4(float3(spos.x*0.5 + 0.5, spos.y, spos.z), 0)).w*16.0 < 1.25) {
								float3 dnorm = normalize(distanceNormalLightOffset(wpos));
								if (length(dnorm) < 1. + 1e-5) wpos += dnorm;
								dnorm = normalize(distanceNormalLightOffset(wpos));
								if (length(dnorm) < 1. + 1e-5) wpos += dnorm;
							}
							for (int t = 0; t < _EGKGIParams.x; t++) {
								float3 pos = (wpos + rd*s) / _EGKParams.x;
								if (max(pos.x, max(pos.y, pos.z)) > 1.0 || min(pos.x, min(pos.y, pos.z)) < 1/_EGKParams.x) break;//check if out of bounds

								float dst = tex3Dlod(_EGKLighting, float4(pos.x*0.5 + 0.5, pos.y, pos.z, 0)).w*16.0;
								if (dst < 1.25) {
									dl = 0.0;
									break;
								}

								s += dst;
							}
						}

						#if defined(EGK_SSDIRECTSHADOWS_ON)
						if (dl > 0.) {
							//screen-space trace using 2d tile tracing
							//get screen space direction towards light by derivative
							float2 ssdir = -normalize((mul(_EGKWorldToCamera, float4(worldPos - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5) -
								(mul(_EGKWorldToCamera, float4(worldPos + rd*0.05 - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5)),
								ssres = 1. / _ScreenParams.xy,
								tuv = uv*_ScreenParams.xy;
							float2 rp = floor(tuv),
								dd = 1. / max(1e-8, abs(ssdir)),
								st = sign(ssdir);


							float2 dst = (st*(rp - tuv) + (st*.5) + .5)*dd,
								mask;
							for (int i = 0; i < _SSShadowParams.x; i++) {
								mask = step(dst, dst.yx);

								dst += mask*dd;
								rp += mask*st;

								float2 msdst = abs(rp / _ScreenParams.xy - 0.5)*(1. + 1. / _ScreenParams.xy);
								if (max(msdst.x, msdst.y) >= 0.5) break;//off screen


								float2 spos = (floor(rp) + 0.5) / _ScreenParams.xy;

								float sdepth = LinearEyeDepth(tex2Dlod(_LastCameraDepthTexture, float4(spos, 0, 0)));
								if (sdepth - _ProjectionParams.y > _ProjectionParams.z - 1e-2) continue;

								float3 swdir = mul(_EGKCameraToWorld, float4(float3((spos * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * sdepth, 1));


								float3 wnorm = normalize(decodeFixed3(tex2Dlod(_RenderBuffer, float4(spos, 0, 0)).w)*2.0 - 1.0);

								float dp = dot(wnorm, norm),
									wlen = length(swdir - worldPos);
								if (abs(dot(rd, normalize(swdir - worldPos))) > 0.95 && length((worldPos + rd*wlen) - swdir) < _SSShadowParams.y) {
									dl = 0.;
									break;
								}
							}
						}
						#endif
						#endif
						#if defined(EGK_LODCASCADE_OFF)
						for (int t = 0; t < _EGKGIParams.x; t++) {
							float3 pos = (wpos + rd*s) / _EGKParams.x;
							if (max(pos.x, max(pos.y, pos.z)) > 1.0-1/_EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1/_EGKParams.x) break;//check if out of bounds

							float dst = tex3Dlod(_EGKLighting, float4(pos, 0)).w*16.0;
							if (dst < 1.25) {
								dl = 0.0;
								break;
							}

							s += dst;
						}
						
						#if defined(EGK_SSDIRECTSHADOWS_ON)
						if (dl > 0.) {
							//screen-space trace using 2d tile tracing
							//get screen space direction towards light by derivative
							float2 ssdir = -normalize((mul(_EGKWorldToCamera, float4(worldPos - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5) -
								(mul(_EGKWorldToCamera, float4(worldPos + rd*0.05 - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5)),
								ssres = 1. / _ScreenParams.xy,
								tuv = uv*_ScreenParams.xy;
							float2 rp = floor(tuv),
								dd = 1. / max(1e-8, abs(ssdir)),
								st = sign(ssdir);


							float2 dst = (st*(rp - tuv) + (st*.5) + .5)*dd,
								mask;
							for (int i = 0; i < _SSShadowParams.x; i++) {
								mask = step(dst, dst.yx);

								dst += mask*dd;
								rp += mask*st;

								float2 msdst = abs(rp / _ScreenParams.xy - 0.5)*(1. + 1. / _ScreenParams.xy);
								if (max(msdst.x, msdst.y) >= 0.5) break;//off screen


								float2 spos = (floor(rp) + 0.5) / _ScreenParams.xy;

								float sdepth = LinearEyeDepth(tex2Dlod(_LastCameraDepthTexture, float4(spos, 0, 0)));
								if (sdepth - _ProjectionParams.y > _ProjectionParams.z - 1e-2) continue;

								float3 swdir = mul(_EGKCameraToWorld, float4(float3((spos * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * sdepth, 1));


								float3 wnorm = normalize(decodeFixed3(tex2Dlod(_RenderBuffer, float4(spos, 0, 0)).w)*2.0 - 1.0);

								float dp = dot(wnorm, norm),
									wlen = length(swdir - worldPos);
								if (abs(dot(rd, normalize(swdir - worldPos))) > 0.95 && length((worldPos + rd*wlen) - swdir) < _SSShadowParams.y) {
									dl = 0.;
									break;
								}
							}
						}
						#endif
						#endif
					}
					return tex2Dlod(_LastFrame, float4(uv, 0, 0)) +
float4( (SpecGGX(-norm, rayDir, normalize(_Light.xyz), mre.y*.3 + 0.7, mre.x*.9 + .1)*(pow(max(0., 4.0 - pow(mre.y,4.0)*4.0),2.2))*(mre.x*0.5 + 0.5)*spec +
		 max(0.,dp)*0.25*(diff*(0.5-mre.x*0.5)+spec*0.5*pow(max(0.,mre.y-0.9)*10.0,5.0)))*dl*_LightColor*_Light.w, 0);
/*
#if defined(EGK_RTFINALGATHER_OFF)
					return tex2Dlod(_LastFrame, float4(uv, 0, 0)) +
float4(dl*SpecGGX(-norm, rayDir, normalize(_Light.xyz), mre.y*.3 + 0.7, mre.x*.9 + .1)*_LightColor*_Light.w * pow(max(0., 3.99 - pow(mre.y,4.0)*4.0),2.2)*(mre.x*0.5 + 0.5)*spec, 0);
#endif
#if defined(EGK_RTFINALGATHER_ON)
					return tex2Dlod(_LastFrame, float4(uv, 0, 0)) +
float4( (SpecGGX(-norm, rayDir, normalize(_Light.xyz), mre.y*.3 + 0.7, mre.x*.9 + .1)*(pow(max(0., 4.0 - pow(mre.y,4.0)*4.0),2.2))*(mre.x*0.5 + 0.5)*spec +
		 max(0.,dp)*0.25*(diff*(0.5-mre.x*0.5)+spec*0.5*pow(max(0.,mre.y-0.9)*10.0,5.0)))*dl*_LightColor*_Light.w, 0);
#endif*/
				}
				
				ENDCG
			}




			//sphere light , pass id = 8
			Pass
			{
				Tags{ "Queue" = "Transparent-1" }

				Fog{ Mode Off }
				ZWrite Off
				ZTest Off
				Blend Off
				Cull Back

				CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag
#pragma target 5.0
#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
#pragma multi_compile EGK_RTFINALGATHER_OFF EGK_RTFINALGATHER_ON
#pragma multi_compile EGK_SSDIRECTSHADOWS_OFF EGK_SSDIRECTSHADOWS_ON

#include "UnityCG.cginc"
#include "EGKVolume.cginc"


				sampler2D_float _RenderBuffer, _LastCameraDepthTexture;
				sampler2D_half _LastFrame;
				float4 _Light, _LightColor;
				float _Cookie;
				sampler2D _CookieTexture;
				float4 _CookieTransform;

				float2 _SSShadowParams;

				//GGX light BRDF 
				float G1V(float NdotV, float k)
				{
					return 1.0 / (NdotV*(1.0 - k) + k);
				}
				float SpecGGX(float3 N, float3 V, float3 L, float roughness, float F0)
				{
					float SqrRoughness = roughness / (1 + pow(roughness, 1.0 / 5.0)*(1 - roughness)*1e3);//max(5e-4,pow(roughness,2.0+roughness*6.0));

					float3 H = normalize(V + L);

					float NdotL = clamp(dot(N, L), 0.0, 1.0);
					float NdotV = clamp(dot(N, V), 0.0, 1.0);
					float NdotH = clamp(dot(N, H), 0.0, 1.0);
					float LdotH = clamp(dot(L, H), 0.0, 1.0);

					float RoughnessPow4 = SqrRoughness*SqrRoughness;
					float pi = 3.14159;
					float denom = NdotH * NdotH * (RoughnessPow4 - 1.0) + 1.0;
					float D = RoughnessPow4 / (pi * denom * denom);

					float LdotH5 = 1.0 - LdotH;
					LdotH5 = LdotH5*LdotH5*LdotH5*LdotH5*LdotH5;
					float F = F0 + (1.0 - F0)*(LdotH5);

					float specular = NdotL * D * F;

					return clamp(specular, 0.0, 1.0);
				}


				float4 frag(v2f_img i) : SV_Target
				{
					float2 uv = i.uv;


					//setup deferred data, gbuffer, depth/world pos, ray dir
					float depthSamp = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));
					float4 matbuf = tex2D(_RenderBuffer, i.uv);

					float3 wpos = mul(_EGKCameraToWorld, float4(float3((i.uv * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * depthSamp, 1)).xyz,
						rayDir = normalize(wpos - _WorldSpaceCameraPos),
						worldPos = wpos;

					fixed3 diff = decodeFixed3(matbuf.x),
						spec = decodeFixed3(matbuf.y),
						mre = decodeFixed3(matbuf.z),
						norm = normalize(decodeFixed3(matbuf.w)*2.0 - 1.0);

					float3 lr = wpos-_Light.xyz,
						   rd = normalize(lr);
					float dp = dot(rd, norm),
						dl = ceil(max(0.0, dp - 1e-6))*max(0.,1./pow(length(lr),2.)),//diffuse quadratic falloff
						ld = length(lr);
					if (dl > 0.) {
						wpos -= _EGKOrigin;
						wpos /= _EGKParams.z;

						wpos += norm*2.0;

						//trace light
						float s = 2.0;
#if defined(EGK_LODCASCADE_ON)
						float ldst = 2.0;
						for (int t = 0; t < _EGKGIParams.x; t++) {
							float3 pos = (wpos + rd*s) / _EGKParams.x;
							if (max(pos.x, max(pos.y, pos.z)) > 1.0 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1 / _EGKParams.x || s*_EGKParams.z >= ld) break;//check if out of bounds

							float dst = tex3Dlod(_EGKLighting, float4(pos.x*0.5, pos.y, pos.z, 0)).w*16.0;
							if (dst < 1.25) {
								ldst = dst;
								break;
							}

							s += dst;
						}
						//second lod cascade trace
						float3 pos = (wpos + rd*s) / _EGKParams.x;
						if (s < ld) {
							if (ldst < 1.25) {
								dl = 0.0;
							}
							else if (max(pos.x, max(pos.y, pos.z)) > 1.0 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1 / _EGKParams.x) {
								wpos = ((pos - 0.5) / (_EGKParams.w / _EGKParams.z) + 0.5);
								float3 spos = wpos;
								wpos *= _EGKParams.x;
								float ols = s*_EGKParams.z;
								s = 0.0;
								if (tex3Dlod(_EGKLighting, float4(float3(spos.x*0.5 + 0.5, spos.y, spos.z), 0)).w*16.0 < 1.25) {
									float3 dnorm = normalize(distanceNormalLightOffset(wpos));
									if (length(dnorm) < 1. + 1e-5) wpos += dnorm;
									dnorm = normalize(distanceNormalLightOffset(wpos));
									if (length(dnorm) < 1. + 1e-5) wpos += dnorm;
								}
								for (int t = 0; t < _EGKGIParams.x; t++) {
									float3 pos = (wpos + rd*s) / _EGKParams.x;
									if (max(pos.x, max(pos.y, pos.z)) > 1.0 || min(pos.x, min(pos.y, pos.z)) < 1 / _EGKParams.x || ols+s*_EGKParams.w >= ld) break;//check if out of bounds

									float dst = tex3Dlod(_EGKLighting, float4(pos.x*0.5 + 0.5, pos.y, pos.z, 0)).w*16.0;
									if (dst < 1.25) {
										dl = 0.0;
										break;
									}

									s += dst;
								}
							}
						}

						#if defined(EGK_SSDIRECTSHADOWS_ON)
						if (dl > 0.) {
							//screen-space trace using 2d tile tracing
							//get screen space direction towards light by derivative
							float2 ssdir = -normalize((mul(_EGKWorldToCamera, float4(worldPos - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5) -
								(mul(_EGKWorldToCamera, float4(worldPos + rd*0.05 - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5)),
								ssres = 1. / _ScreenParams.xy,
								tuv = uv*_ScreenParams.xy;
							float2 rp = floor(tuv),
								dd = 1. / max(1e-8, abs(ssdir)),
								st = sign(ssdir);


							float2 dst = (st*(rp - tuv) + (st*.5) + .5)*dd,
								mask;
							for (int i = 0; i < _SSShadowParams.x; i++) {
								mask = step(dst, dst.yx);

								dst += mask*dd;
								rp += mask*st;

								float2 msdst = abs(rp / _ScreenParams.xy - 0.5)*(1. + 1. / _ScreenParams.xy);
								if (max(msdst.x, msdst.y) >= 0.5) break;//off screen


								float2 spos = (floor(rp) + 0.5) / _ScreenParams.xy;

								float sdepth = LinearEyeDepth(tex2Dlod(_LastCameraDepthTexture, float4(spos, 0, 0)));
								if (sdepth - _ProjectionParams.y > _ProjectionParams.z - 1e-2) continue;

								float3 swdir = mul(_EGKCameraToWorld, float4(float3((spos * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * sdepth, 1));
								float wlen = length(swdir - worldPos);
								if (wlen >= ld) continue;//hit/past point light

								float3 wnorm = normalize(decodeFixed3(tex2Dlod(_RenderBuffer, float4(spos, 0, 0)).w)*2.0 - 1.0);

								float dp = dot(wnorm, norm);
								if (abs(dot(rd, normalize(swdir - worldPos))) > 0.95 && length((worldPos + rd*wlen) - swdir) < _SSShadowParams.y) {
									dl = 0.;
									break;
								}
							}
						}
						#endif
#endif
#if defined(EGK_LODCASCADE_OFF)
						for (int t = 0; t < _EGKGIParams.x; t++) {
							float3 pos = (wpos + rd*s) / _EGKParams.x;
							if (max(pos.x, max(pos.y, pos.z)) > 1.0 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 1 / _EGKParams.x || s*_EGKParams.z >= ld) break;//check if out of bounds

							float dst = tex3Dlod(_EGKLighting, float4(pos, 0)).w*16.0;
							if (dst < 1.25) {
								dl = 0.0;
								break;
							}

							s += dst;
						}


						#if defined(EGK_SSDIRECTSHADOWS_ON)
						if (dl > 0.) {
							//screen-space trace using 2d tile tracing
							//get screen space direction towards light by derivative
							float2 ssdir = -normalize((mul(_EGKWorldToCamera, float4(worldPos - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5) -
								(mul(_EGKWorldToCamera, float4(worldPos + rd*0.05 - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5)),
								ssres = 1. / _ScreenParams.xy,
								tuv = uv*_ScreenParams.xy;
							float2 rp = floor(tuv),
								dd = 1. / max(1e-8, abs(ssdir)),
								st = sign(ssdir);


							float2 dst = (st*(rp - tuv) + (st*.5) + .5)*dd,
								mask;
							for (int i = 0; i < _SSShadowParams.x; i++) {
								mask = step(dst, dst.yx);

								dst += mask*dd;
								rp += mask*st;

								float2 msdst = abs(rp / _ScreenParams.xy - 0.5)*(1. + 1. / _ScreenParams.xy);
								if (max(msdst.x, msdst.y) >= 0.5) break;//off screen


								float2 spos = (floor(rp) + 0.5) / _ScreenParams.xy;

								float sdepth = LinearEyeDepth(tex2Dlod(_LastCameraDepthTexture, float4(spos, 0, 0)));
								if (sdepth - _ProjectionParams.y > _ProjectionParams.z - 1e-2) continue;

								float3 swdir = mul(_EGKCameraToWorld, float4(float3((spos * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * sdepth, 1));
								float wlen = length(swdir - worldPos);
								if (wlen >= ld) continue;//hit/past point light
								
								float3 wnorm = normalize(decodeFixed3(tex2Dlod(_RenderBuffer, float4(spos, 0, 0)).w)*2.0 - 1.0);

								float dp = dot(wnorm, norm);
								if (abs(dot(rd, normalize(swdir - worldPos))) > 0.95 && length((worldPos + rd*wlen) - swdir) < _SSShadowParams.y) {
									dl = 0.;
									break;
								}
							}
						}
						#endif
#endif
					}
					return tex2Dlod(_LastFrame, float4(uv, 0, 0)) +
						float4((SpecGGX(-norm, rayDir, normalize(_Light.xyz), mre.y*.3 + 0.7, mre.x*.9 + .1)*(pow(max(0., 3.99 - pow(mre.y, 4.0)*4.0), 2.2))*(mre.x*0.5 + 0.5)*spec +
						max(0., dp)*0.25*(diff*(0.5 - mre.x*0.5) + spec*0.5*pow(max(0., mre.y - 0.9)*10.0, 5.0)))*dl*_LightColor*_Light.w, 0);
/*#if defined(EGK_RTFINALGATHER_OFF)
					return tex2Dlod(_LastFrame, float4(uv, 0, 0)) +
						float4(dl*SpecGGX(-norm, rayDir, normalize(_Light.xyz), mre.y*.3 + 0.7, mre.x*.9 + .1)*_LightColor*_Light.w * pow(max(0., 3.99 - pow(mre.y, 4.0)*4.0), 2.2)*(mre.x*0.5 + 0.5)*spec, 0);
#endif
#if defined(EGK_RTFINALGATHER_ON)
					return tex2Dlod(_LastFrame, float4(uv, 0, 0)) +
						float4((SpecGGX(-norm, rayDir, normalize(_Light.xyz), mre.y*.3 + 0.7, mre.x*.9 + .1)*(pow(max(0., 3.99 - pow(mre.y, 4.0)*4.0), 2.2))*(mre.x*0.5 + 0.5)*spec +
						max(0., dp)*0.25*(diff*(0.5 - mre.x*0.5) + spec*0.5*pow(max(0., mre.y - 0.9)*10.0, 5.0)))*dl*_LightColor*_Light.w, 0);
#endif*/
				}

					ENDCG
			}


			//temporal blend/reprojection, pass id = 9
			Pass
			{
				Tags{ "Queue" = "Transparent-1" }

				Fog{ Mode Off }
				ZWrite Off
				ZTest Off
				Blend Off
				Cull Back

				CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma target 5.0


				#include "UnityCG.cginc"
#include "EGKVolume.cginc"


				sampler2D_float _RenderBuffer, _LastRenderBuffer, _LastCameraDepthTexture, _LastFrame, _CurrentFrame;

				
				float4 frag(v2f_img i) : SV_Target
				{
					//return tex2D(_CurrentFrame, i.uv);

					float depthSamp = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_LastCameraDepthTexture, i.uv));
					float4 matbuf = tex2D(_RenderBuffer, i.uv);
					fixed3 diff = decodeFixed3(matbuf.x),
						spec = decodeFixed3(matbuf.y),
						mre = decodeFixed3(matbuf.z),
						norm = normalize(decodeFixed3(matbuf.w)*2.0 - 1.0);

					float3 worldPos = mul(_EGKCameraToWorld, float4(float3((i.uv * 2 - 1) / float2(unity_CameraProjection._11, unity_CameraProjection._22), -1) * depthSamp, 1)).xyz;


					//temporal reprojection
					float2 ruv = ((((mul(_EGKLastWorldToCamera, float4(worldPos - _EGKLastCameraPos, 1)).xy / depthSamp)* float2(unity_CameraProjection._11, unity_CameraProjection._22) / 2.0 + 0.5)));// *_ScreenParams.xy)) / _ScreenParams.xy;
					
					//return float4(pow(ruv,2.2), 0.0, 1.0);
					//-normalize((mul(_EGKWorldToCamera, float4(worldPos - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5) -(mul(_EGKWorldToCamera, float4(worldPos + rd*0.05 - _WorldSpaceCameraPos, 1)).xy / depthSamp*(0.25 + 0.25*_ScreenParams.yx / _ScreenParams.xy) + 0.5)),


					//add to last frame
					float4 oldMat = tex2D(_LastRenderBuffer, ruv);
					fixed3 mat = decodeFixed3(oldMat.y);

					float alpha = clamp((max(0.0, dot(normalize(decodeFixed3(oldMat.w)*2.0 - 1.0), norm) - 0.9) / (1.0 - 0.9))*max(0.0, 1.0 - length(decodeFixed3(oldMat.x) - (diff*0.5 + spec*0.5))*10.0)*max(0.0, 1.0 - length(mat.xy - mre.xy)*10.0)*
						max(0.0, 1.0 - abs(depthSamp - oldMat.z) / (depthSamp*0.01)), 0., 1.);

					//return float4(abs(depthSamp - oldMat.z), 0., 0., 1.);// abs(depthSamp - (oldMat.z / 2048.)*(_ProjectionParams.z));
					//return float2(alpha, 1.).xxxy;
					//return float4(abs(depthSamp - oldMat.z), 0., 0., 1.);

					float4 last = tex2D(_LastFrame, ruv),
						o = tex2D(_CurrentFrame, i.uv);

					return clamp(lerp(o, last, clamp(alpha, 0., 0.9 /*+ 0.1*depthSamp / _ProjectionParams.z*/)), 0.0, 1.0);
				}
				ENDCG
			}
	}
}
