﻿//Ethan Alexander Shulman 2017
using UnityEngine;

namespace EthansGraphicsKit.Lighting
{
    /// <summary>
    /// Deferred rendering ImageEffet, you need to attach this to the desired camera link this with LightingSystem for deferred rendering.
    /// </summary>
    public class LightingSystemCameraDisplay : MonoBehaviour
    {

        public static LightingSystemCameraDisplay lscd;


        void Awake()
        {
            lscd = this;
        }



        void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (LightingSystem.lightingSystem == null) return;
            LightingSystem.lightingSystem.DisplayRenderResult(source, destination);
        }
    }
}