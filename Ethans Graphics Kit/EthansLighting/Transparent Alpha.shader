﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Transparent Alpha"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_Emission("Emission", Range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Transparent" "Queue" = "AlphaTest+1" "IgnoreProjector" = "True" }

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
			#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			
			#include "UnityCG.cginc"

			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;


#if defined(EGK_NOLIGHTING)
#include "Lighting.cginc"
#include "AutoLight.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float3 normal : TEXCOORD2;
				LIGHTING_COORDS(3, 4)
			};

			#include "EthansLighting.cginc"
			

			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply lighting
				col.xyz = computeDiffuseOnlyLighting(i,col.xyz);

				col.w *= _DiffuseColor.w;
				return col;
			}
#endif

#if defined(EGK_DEFERREDGI)
#pragma target 5.0
#include "EGKVolume.cginc"

			float3 _ELFogColor, _ELFogParams;


			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 normalWorld : TEXCOORD1;
				float3 world : TEXCOORD2;
			};


			vertexOutput vert(vertexInput i)
			{
				vertexOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, i.vertex);
				o.normalWorld = UnityObjectToWorldNormal(i.normal);
				o.world = mul(_Object2World, i.vertex).xyz;
				o.uv = TRANSFORM_TEX(i.uv, _MainTex);
				return o;
			}

			fixed4 frag(vertexOutput i) : SV_Target
			{
				//diffuse texture with tint
				fixed4 diffuse = tex2D(_MainTex, i.uv)*_DiffuseColor;//gamma->linear then tint

				float3 viewDir = i.world - _WorldSpaceCameraPos;
				float vdepth = pow(length(viewDir) / _ELFogParams.x, _ELFogParams.z)*_ELFogParams.y;

#if defined(EGK_LODCASCADE_ON)
				//sample hi/lo res light volume cascade, no blending
				float hiLen = _EGKParams.y*_EGKParams.z;
				float3 centerOrigin = _EGKOrigin + hiLen,
					mdst = abs(i.world - centerOrigin),
					worldPos;
				if (max(mdst.x, max(mdst.y, mdst.z)) > hiLen) {//if outside hi res volume
					//convert world to lores light volume coord
					worldPos = clamp(((i.world - (centerOrigin - _EGKParams.y*_EGKParams.w)) / _EGKParams.w + normalize(i.normalWorld)*2.0) / _EGKParams.x, 0, 1 - 1 / _EGKParams.x);
					worldPos.x = worldPos.x*0.5 + 0.5;
				}
				else {
					//convert world pos to hires light volume coord
					worldPos = clamp(((i.world - _EGKOrigin) / _EGKParams.z + normalize(i.normalWorld)*2.0) / _EGKParams.x, 0, 1 - 1 / _EGKParams.x);
					worldPos.x *= 0.5;
				}

				//sample light volume
				float3 o = tex3Dlod(_EGKLighting, float4(worldPos, 0.0)).xyz;
#endif

#if defined(EGK_LODCASCADE_OFF)
				//sample light map 2 voxels away from the normal
				float3 worldPos = (i.world - _EGKOrigin) / _EGKParams.z + normalize(i.normalWorld)*2.0;
				float3 o = tex3Dlod(_EGKLighting, float4(worldPos / _EGKParams.x, 0.0)).xyz;
#endif

				return fixed4(lerp(diffuse.xyz * o, _ELFogColor, vdepth), diffuse.w);//final lighting+emission then linear -> gamma
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/Hidden/Transparent Alpha_Fallback"
}
