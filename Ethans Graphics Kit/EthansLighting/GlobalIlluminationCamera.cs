﻿//Ethan Alexander Shulman 2017

using UnityEngine;


namespace EthansLighting
{
    public class GlobalIlluminationCamera : MonoBehaviour
    {

        private int GIOnlyId;

        void Awake()
        {
            GIOnlyId = Shader.PropertyToID("_ELGIOnly");
        }

        void OnPreRender()
        {
            Shader.SetGlobalFloat(GIOnlyId, 1.0f);
        }
        void OnPostRender()
        {
            EthansLighting.ethansLighting.UpdateGI();
            Shader.SetGlobalFloat(GIOnlyId, 0.0f);
        }
    }
}