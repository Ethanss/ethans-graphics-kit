﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Transparent Glass"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_Roughness("Roughness", Range(0.0, 1.0)) = 0.0
		_Emission("Emission", Range(0.0, 1.0)) = 0.0
		_TintColor("Tint Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Transparent" "Queue" = "AlphaTest+1" "IgnoreProjector" = "True" }

		//pass 0 - tint pass
		Pass
		{
			ZWrite Off
			Blend DstColor Zero

			CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"


			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _TintColor;


			float4 vert(float4 vertex : POSITION) : SV_POSITION
			{
				return mul(UNITY_MATRIX_MVP, vertex);
			}

			fixed4 frag() : SV_Target
			{
				return _TintColor;
			}
			ENDCG
		}

		//pass 1 - reflection pass
		Pass
		{
			Tags{ "LightMode" = "ForwardBase"  "PassFlags" = "OnlyDirectional" }

			ZWrite Off
			Blend One One
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			
			#include "UnityCG.cginc"

			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;

#if defined(EGK_NOLIGHTING)
#include "Lighting.cginc"
#include "AutoLight.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float3 normal : TEXCOORD2;
				LIGHTING_COORDS(3, 4)
			};

			#include "EthansLighting.cginc"
			

			v2f vert (appdata_base v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply lighting
				col.xyz = computeLighting(i,col.xyz);
				return col;
			}
#endif

#if defined(EGK_DEFERREDGI)
#pragma target 5.0
#include "EGKVolume.cginc"

			float3 _ELFogColor, _ELFogParams;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD0;
			};
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 worldNormal : TEXCOORD2;
				float3 world : TEXCOORD3;
			};


			vertexOutput vert(vertexInput i)
			{
				vertexOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, i.vertex);
				o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
				o.worldNormal = UnityObjectToWorldNormal(i.normal);
				o.world = mul(_Object2World, i.vertex).xyz;
				return o;
			}

			fixed4 frag(vertexOutput i) : SV_Target
			{
				float3 o = 0;

				float2 uv = i.uv;
				float3 viewDir = i.world - _WorldSpaceCameraPos;
				float vdepth = pow(length(viewDir) / _ELFogParams.x, _ELFogParams.z)*_ELFogParams.y;
				viewDir = normalize(viewDir);

				float3 worldPos = (i.world - _EGKOrigin) / _EGKParams.z,
					norm = normalize(i.worldNormal),
					rayDir = reflect(viewDir, norm);

				float fres = _Metallic + (1.0 - _Metallic)*pow(1.0 - max(0.0, dot(-norm, viewDir)), 5.0);

				worldPos += norm*2.0;

#if defined(EGK_LODCASCADE_OFF)
				float dst;
				for (int i = 0; i < _EGKGIParams.x; i++) {
					float3 pos = worldPos / _EGKParams.x;
					if (max(pos.x, max(pos.y, pos.z)) > 1.0 || min(pos.x, min(pos.y, pos.z)) < 0.0) break;//check if out of bounds

					dst = tex3Dlod(_EGKLighting, float4(pos, 0)).w*16.0;
					if (dst < 1.25) break;

					worldPos += rayDir*dst;
				}

				float3 pos = worldPos / _EGKParams.x;
				if (max(pos.x, max(pos.y, pos.z)) > 1.0 || min(pos.x, min(pos.y, pos.z)) < 0.0) {
					//out of bounds, reflect background/light
					o.xyz = lerp(_EGKBackgroundColor.xyz, texCUBElod(_EGKBackgroundCube, float4(rayDir, 0.0)).xyz,
						_EGKBackgroundType)*_EGKBackgroundColor.w*fres;

				}
				else if (dst < 1.25) {
					//hit, shade
					fixed4 mat = tex3Dlod(_EGKMaterial, float4(pos, 0));
					o.xyz = mat.xyz*(floor(mat.w*256.0 - 1.0) / 255.0 +//material emission
						pow(tex3Dlod(_EGKLighting, float4((worldPos - (2.0 - dst)*rayDir) / _EGKParams.x, 0)).xyz, 2.2))*fres;
				}
#endif


#if defined(EGK_LODCASCADE_ON)
				for (int i = 0; i < _EGKGIParams.x; i++) {
					float3 pos = worldPos / _EGKParams.x;
					if (max(pos.x, max(pos.y, pos.z)) > 1 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 0.0) break;//check if out of bounds

					float dst = tex3Dlod(_EGKLighting, float4(float3(pos.x*0.5, pos.y, pos.z), 0)).w*16.0;
					if (dst < 1.25) break;

					worldPos += rayDir*dst;
				}

				float3 pos = worldPos / _EGKParams.x;
				if (max(pos.x, max(pos.y, pos.z)) > 1 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 0.0) {
					//out of bounds, second trace
					worldPos = ((pos - 0.5) / (_EGKParams.w / _EGKParams.z) + 0.5)*_EGKParams.x;
					for (int i = 0; i < _EGKGIParams.x; i++) {
						float3 pos = worldPos / _EGKParams.x;
						if (max(pos.x, max(pos.y, pos.z)) > 1 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 0.0) break;//check if out of bounds

						float dst = tex3Dlod(_EGKLighting, float4(float3(pos.x*0.5 + 0.5, pos.y, pos.z), 0)).w*16.0;
						if (dst < 1.25) break;

						worldPos += rayDir*dst;
					}

					float3 pos = worldPos / _EGKParams.x;
					if (max(pos.x, max(pos.y, pos.z)) > 1 - 1 / _EGKParams.x || min(pos.x, min(pos.y, pos.z)) < 0.0) {
						o.xyz = lerp(_EGKBackgroundColor.xyz, texCUBElod(_EGKBackgroundCube, float4(rayDir, 0.0)).xyz,
							_EGKBackgroundType)*_EGKBackgroundColor.w*fres;
					}
					else if (tex3Dlod(_EGKLighting, float4(float3(pos.x*0.5 + 0.5, pos.y, pos.z), 0)).w*16.0 < 1.25) {
						//hit, shade
						fixed4 mat = tex3Dlod(_EGKMaterial, float4(float3(pos.x*0.5 + 0.5, pos.y, pos.z), 0));
						o.xyz = mat.xyz*(floor(mat.w*256.0 - 1.0) / 255.0 +//material emission
							pow(tex3Dlod(_EGKLighting, float4(((worldPos - rayDir*2.0) / _EGKParams.x)*float3(0.5, 1, 1) + float3(0.5, 0, 0), 0)).xyz, 2.2))*fres;
					}
				}
				else if (tex3Dlod(_EGKLighting, float4(float3(pos.x*0.5, pos.y, pos.z), 0)).w*16.0 < 1.25) {
					//hit, shade
					fixed4 mat = tex3Dlod(_EGKMaterial, float4(float3(pos.x*0.5, pos.y, pos.z), 0));
					o.xyz = mat.xyz*(floor(mat.w*256.0 - 1.0) / 255.0 +//material emission
						pow(tex3Dlod(_EGKLighting, float4(((worldPos - rayDir*2.0) / _EGKParams.x)*float3(0.5, 1, 1), 0)).xyz, 2.2))*fres;
				}
#endif

				return fixed4(lerp(pow(o, 1.0 / 2.2)*_SpecularColor.xyz*tex2D(_MainTex, uv).xyz, _ELFogColor, vdepth), 1.);
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/VertexLit Fallback Cutout"
}
