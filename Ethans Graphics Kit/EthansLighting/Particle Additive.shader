//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Particle Additive"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_Roughness("Roughness", Range(0.0, 1.0)) = 0.0
		_Emission("Emission", Range(0.0, 1.0)) = 0.0
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Transparent"
			  "Queue"="AlphaTest+1" }

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
			Lighting On
			ZWrite Off
			Blend One One

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			
			#include "UnityCG.cginc"

			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;

#if defined(EGK_NOLIGHTING)
			#pragma target 3.0
			#include "Lighting.cginc"
			#include "ParticleAutoLight.cginc"
			struct vs {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float4 color : COLOR;
			};
			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float3 normal : TEXCOORD2;
				LIGHTING_COORDS(3, 4)
				float4 fog : TEXCOORD5;
			};

			#include "EthansLighting.cginc"
			

			v2f vert (vs v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				o.normal = float3(0, 0, 0);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				o.color = v.color;
				o.color.xyz *= computeParticleLighting(o,o.fog);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv)*i.color;
				return lerp(col,i.fog,i.fog.w)*col.w;
			}
#endif

#if defined(EGK_DEFERREDGI)
#pragma target 5.0
#include "EGKVolume.cginc"

			struct vertexInput
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
			};
			struct vertexOutput
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float2 uv : TEXCOORD0;
				float4 projPos : TEXCOORD1;
				float4 fog : TEXCOORD2;
			};

			float3 _ELFogColor, _ELFogParams;

			float _InvFade;
			sampler2D_float _CameraDepthTexture;

			vertexOutput vert(vertexInput v)
			{
				vertexOutput o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.projPos = ComputeScreenPos(o.vertex);
				COMPUTE_EYEDEPTH(o.projPos.z);

				float3 iworld = mul(_Object2World, v.vertex).xyz;
				o.fog = float4(_ELFogColor, pow(min(1., length(_WorldSpaceCameraPos - iworld) / _ELFogParams.x), _ELFogParams.z)*_ELFogParams.y);

				//lighting
#if defined(EGK_LODCASCADE_ON)
				//sample hi/lo res light volume cascade, no blending
				float hiLen = _EGKParams.y*_EGKParams.z;
				float3 centerOrigin = _EGKOrigin + hiLen,
					mdst = abs(iworld - centerOrigin),
					worldPos;
				if (max(mdst.x, max(mdst.y, mdst.z)) > hiLen) {//if outside hi res volume
					//convert world to lores light volume coord
					worldPos = clamp(((iworld - (centerOrigin - _EGKParams.y*_EGKParams.w)) / _EGKParams.w) / _EGKParams.x, 0, 1 - 1 / _EGKParams.x);
					worldPos.x = worldPos.x*0.5 + 0.5;
				}
				else {
					//convert world pos to hires light volume coord
					worldPos = clamp(((iworld - _EGKOrigin) / _EGKParams.z) / _EGKParams.x, 0, 1 - 1 / _EGKParams.x);
					worldPos.x *= 0.5;
				}

				//sample light volume
				float3 oc = tex3Dlod(_EGKLighting, float4(worldPos, 0.0)).xyz;
#endif

#if defined(EGK_LODCASCADE_OFF)
				//sample light map 2 voxels away from the normal
				float3 worldPos = (iworld - _EGKOrigin) / _EGKParams.z;
				float3 oc = tex3Dlod(_EGKLighting, float4(worldPos / _EGKParams.x, 0.0)).xyz;
#endif

				o.color = v.color*float4(oc, 1.0);
				return o;
			}

			fixed4 frag(vertexOutput i) : SV_Target
			{
				//soft particle fade
				float sceneZ = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
				float partZ = i.projPos.z;
				float fade = saturate(_InvFade * (sceneZ - partZ));
				i.color *= fade;

				fixed4 c = tex2D(_MainTex, i.uv)*_SpecularColor*i.color;
				return lerp(c,i.fog,i.fog.w)*c.w;
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/Hidden/Particle Additive_Fallback"
}
