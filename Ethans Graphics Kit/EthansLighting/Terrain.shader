﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Terrain"
{
	Properties
	{
		// set by terrain engine
		[HideInInspector] _Control("Control (RGBA)", 2D) = "red" {}
		[HideInInspector] _Splat3("Layer 3 (A)", 2D) = "white" {}
		[HideInInspector] _Splat2("Layer 2 (B)", 2D) = "white" {}
		[HideInInspector] _Splat1("Layer 1 (G)", 2D) = "white" {}
		[HideInInspector] _Splat0("Layer 0 (R)", 2D) = "white" {}
		[HideInInspector] _Normal3("Normal 3 (A)", 2D) = "bump" {}
		[HideInInspector] _Normal2("Normal 2 (B)", 2D) = "bump" {}
		[HideInInspector] _Normal1("Normal 1 (G)", 2D) = "bump" {}
		[HideInInspector] _Normal0("Normal 0 (R)", 2D) = "bump" {}
		[HideInInspector][Gamma] _Metallic0("Metallic 0", Range(0.0, 1.0)) = 0.0
		[HideInInspector][Gamma] _Metallic1("Metallic 1", Range(0.0, 1.0)) = 0.0
		[HideInInspector][Gamma] _Metallic2("Metallic 2", Range(0.0, 1.0)) = 0.0
		[HideInInspector][Gamma] _Metallic3("Metallic 3", Range(0.0, 1.0)) = 0.0
		[HideInInspector] _Smoothness0("Smoothness 0", Range(0.0, 1.0)) = 1.0
		[HideInInspector] _Smoothness1("Smoothness 1", Range(0.0, 1.0)) = 1.0
		[HideInInspector] _Smoothness2("Smoothness 2", Range(0.0, 1.0)) = 1.0
		[HideInInspector] _Smoothness3("Smoothness 3", Range(0.0, 1.0)) = 1.0

		// used in fallback on old cards & base map
		[HideInInspector] _MainTex("BaseMap (RGB)", 2D) = "white" {}
		[HideInInspector] _Color("Main Color", Color) = (1, 1, 1, 1)

		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Opaque" }

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;


#if defined(EGK_NOLIGHTING)
#include "Lighting.cginc"
#include "AutoLight.cginc"
#include "CustomTerrainSplatmapCommon.cginc"
#include "EthansLighting.cginc"

			v2f vert (appdata_tan v)
			{
				v2f o;
				SplatmapVert(v, o);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col;
				fixed metal;
				half4 defaultSmoothness = half4(_Smoothness0, _Smoothness1, _Smoothness2, _Smoothness3);
				SplatmapMix(i, defaultSmoothness, col, metal, i.normal);
				_DiffuseColor = col;
				_SpecularColor = col;
				_Metallic = metal;
				_Roughness = col.w;
				_Emission = 0.;
				col.w = 0.0;
				// apply lighting
				col.xyz = computeLighting(i,col.xyz);
				return col;
			}
#endif

#if defined(EGK_DEFERREDGI)
#include "EGKVolume.cginc"
#include "CustomTerrainSplatmapCommon.cginc"

			v2f vert(appdata_tan v)
			{
				v2f o;
				SplatmapVert(v, o);
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			float4 frag(v2f i) : SV_Target
			{
				fixed4 col;
				fixed metal;
				half4 defaultSmoothness = half4(_Smoothness0, _Smoothness1, _Smoothness2, _Smoothness3);
				SplatmapMix(i, defaultSmoothness, col, metal, i.normal);

				i.normal = normalize(i.normal)*0.5 + 0.5;

				//pack material data into high precision float color
				return float4(encodeFixed3(col),
					encodeFixed3(col),
					encodeFixed3(fixed3(metal, col.w, 0.)),
					encodeFixed3(i.normal));
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/VertexLit Fallback"
}
