//Ethan Alexander Shulman 2017
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace EthansGraphicsKit.Lighting {
    /// <summary>
    /// Lighting system.
    /// </summary>
	public class LightingSystem : MonoBehaviour {

        /// <summary>
        /// Static LightingSystem reference.
        /// </summary>
        public static LightingSystem lightingSystem;


		public enum RenderingPath {
			ForwardUnlit = 0,
			Deferred = 1
		}
		public enum GlobalIlluminationQuality {
			VeryLow = 32,
			Low = 64,
			Medium = 128,
			High = 256,
			VeryHigh = 512
		}
        public enum GlobalIlluminationUpdateMode
        {
            NoUpdating = 0,//updating isn't done automatically and is only called manually using UpdateGlobalIllumination()
            Delayed = 1,//attempts to update as fast as possible without reducing framerate
            Realtime = 2//attempts to update at 60fps
        }


        
        /// <summary>
        /// Screen render scale.
        /// </summary>
        [Tooltip("Screen resolution scale, for downscaling/upscaling.")]
        public float renderScale = 1.0f;

        /// <summary>
        /// Render scale texture filter.
        /// </summary>
        [Tooltip("Render scale texture filter.")]
        public FilterMode renderScaleFilterMode = FilterMode.Bilinear;

        /// <summary>
        /// Rendering path.
        /// </summary>
        [Tooltip("Rendering path.")]
		public RenderingPath renderingPath = RenderingPath.Deferred;

        /// <summary>
        /// LightingSystemCameraDisplay component attached to main camera, used for deferred rendering.
        /// </summary>
        [Tooltip("LightingSystemCameraDisplay component attached to main camera, used for deferred rendering.")]
        public LightingSystemCameraDisplay deferredCameraDisplay = null;


        [Space(15)]

        /// <summary>
        /// Global illumination lightmap quality.
        /// </summary>
        [Tooltip("Global illumination lightmap quality.")]
        public GlobalIlluminationQuality quality = GlobalIlluminationQuality.Low;

        /// <summary>
        /// Global illumination lightmap distance(similiar to shadow distance).
        /// </summary>
        [Tooltip("Global illumination lightmap distance(similiar to shadow distance).")]
		public float lightmapDistance = 512.0f;

        /// <summary>
        /// Use half of the lightmap resolution to create a lower resolution GI map to cover more view distance.
        /// </summary>
        [Tooltip("Use half of the lightmap resolution to create a lower resolution GI map to cover more view distance.")]
        public bool levelOfDetailCascade = false;

        /// <summary>
        /// Scale of lower resolution lightmap cascade.
        /// </summary>
        [Tooltip("Scale of lower resolution lightmap cascade.")]
        public float levelOfDetailCascadeScale = 16.0f;

        [Space(15)]

        /// <summary>
        /// Global illumination voxel map update priority.
        /// </summary>
        [Tooltip("Global illumination voxel map update priority.")]
        public GlobalIlluminationUpdateMode updateMode = GlobalIlluminationUpdateMode.Delayed;

        /// <summary>
        /// Global illumination culling mask, objects in this culling mask will effect global illumination.
        /// </summary>
        [Tooltip("Global illumination culling mask, objects in this culling mask will effect global illumination.")]
        public LayerMask cullingMask = -1;

        /// <summary>
        /// Intensity of the light from the background(skybox or clear color).
        /// </summary>
        [Tooltip("Intensity of the light from the background(skybox or clear color).")]
        public float backgroundLightIntensity = 1.0f;

        /// <summary>
        /// Brightness scale.
        /// </summary>
        [Tooltip("Brightness scale.")]
        public Vector3 brightnessScale = Vector3.one;

        [Space(15)]

        /// <summary>
        /// Real-time diffuse global illumination through a real-time final gather, very expensive.
        /// </summary>
        [Tooltip("Real-time diffuse global illumination through a real-time final gather, very expensive.")]
        public bool realtimeFinalGather = true;

        /// <summary>
        /// Number of denoising iterations applied to screen, a cheap way of reducing reflection fuzziness.
        /// </summary>
        [Tooltip("Number of denoising iterations applied to screen, a cheap way of reducing reflection fuzziness.")]
        [Range(0, 2)]
        public int denoiseIterations = 1;

        [Space(15)]

        /// <summary>
        /// Screen-space shadows for direct lighting, works best with real-time final gather.
        /// </summary>
        [Tooltip("Screen-space shadows for direct lighting, works best with real-time final gather.")]
        public bool screenSpaceShadows = false;
        
        /// <summary>
        /// The distance in pixels of the screen-space shadows, larger is more expensive.
        /// </summary>
        [Tooltip("The distance in pixels of the screen-space shadows, larger is more expensive.")]
        public int screenSpaceShadowIterations = 8;

        /// <summary>
        /// The distance in world units that considers a hit when screen space shadow tracing.
        /// </summary>
        [Tooltip("The distance in world units that considers a hit when screen space shadow tracing.")]
        public float screenSpaceShadowDistance = 1.0f;

        [Space(15)]

        /// <summary>
        /// Number of computed global illumination bounces.
        /// </summary>
        [Tooltip("Number of computed global illumination bounces.")]
        [Range(0,8)]
        public int bounces = 2;

        /// <summary>
        /// Number of computed global illumination paths.
        /// </summary>
        [Tooltip("Number of computed global illumination paths.")]
        public int computedPaths = 32;

        /// <summary>
        /// Number of computed global illumination ray-trace steps(GI view distance).
        /// </summary>
        [Tooltip("Number of computed global illumination ray-trace steps(GI view distance).")]
        public int computedSteps = 8;

        /// <summary>
        /// Number of shadow ray-trace steps in computed GI. Shadow view distance for Direction/Point lights.
        /// </summary>
        [Tooltip("Number of shadow ray-trace steps in computed GI. Shadow view distance for Direction/Point lights.")]
        public int shadowRayTraceSteps = 16;

        /// <summary>
        /// Max number of real-time paths, higher reduces soft reflection fuzziness.
        /// </summary>
        [Tooltip("Max number of real-time paths, higher reduces soft reflection fuzziness.")]
        public int realtimePaths = 8;

        /// <summary>
        /// Number of real-time ray-trace steps(higher means longer view distance in reflections).
        /// </summary>
        [Tooltip("Number of real-time ray-trace steps(higher means longer view distance in reflections).")]
        public int realtimeSteps = 8;

        /// <summary>
        /// Number of lightmap convolution steps in computed global illumination, can cause light bleeding problems with more then 2 steps.
        /// </summary>
        [Tooltip("Number of lightmap convolutions steps in computed global illumination, can cause light bleeding problems with more then 2 steps.")]
        [Range(0,4)]
        public int lightMapConvolutionSteps = 1;

        [Space(20)]
        public Color fogColor = Color.black;
        public float fogDepth = 1000.0f;
        [Range(0.0f, 1.0f)]
        public float fogIntensity = 1.0f;
        public float fogPower = 1.0f;



        //needed compute shaders
        [HideInInspector]
		public ComputeShader addSliceToVolumeCS, generateGeometryVolumeCS, lightingVolumeCS;

        [HideInInspector]
        public Shader voxelMaterialPassShader;

        [HideInInspector]
        public Shader lightingPassShader;

        [HideInInspector]
        public Color originBackgroundColor;

        [HideInInspector]
        public List<PrelightingEffect> prelightingPostEffects = new List<PrelightingEffect>();


		private LayerMask mainCamerasOldCullingMask;

        private float voxelSize, loVoxelSize;

		private int qualityValue, qualityHalfValue,
		addSliceToVolumeKernelIndex, initGeoVolumeKernelIndex, sweepGeoVolumeKernelIndex,
        emptyVolumeKernelIndex, directionalLightKernelIndex, sphereLightKernelIndex, copyVolumeKernelIndex,
        pathTraceKernelIndex, addBounceKernelIndex, balanceLightKernelIndex,
        pathTraceKernelIndexLOD, directionalLightKernelIndexLOD, sphereLightKernelIndexLOD, lightPenetrationKernelIndex;

        private int renderScreenWidth, renderScreenHeight;

		private bool overridePath = false, 
		updatingGlobalIllumination = false;
		private RenderingPath newPath = RenderingPath.ForwardUnlit;
        private long ramUsed = 0;

		private Material deferredLightingPassMaterial;

		private CommandBuffer deferredLightingPass, mainDisplayPass;

        private RenderTexture displayTexture, renderBuffer2, renderTexture, renderBuffer, lastFrame, lastFrame2, volumeSliceTexture,
        materialVolume, distanceVolume, lightingVolume, distanceBuffer, volumeBuffer, loMaterialVolume, loLightingVolume,
        displayedMaterialVolume, displayedLightingVolume;

        private Transform mainCamTransform;
		private Camera mainCamera, proxyCamera, volumeSliceCamera;


        void Awake()
        {
            lightingSystem = this;
        }


		void OnEnable() {
            updatingGlobalIllumination = false;
            UpdateSettings();
            StartCoroutine(UpdateGlobalIlluminationLoop());
        }
		void OnDisable() {
			//enable forwardlighting rendering in editor(this also cleans up and destroys rendertextures)
            StopAllCoroutines();
            updatingGlobalIllumination = false;

			overridePath = true;
			newPath = RenderingPath.ForwardUnlit;
			UpdateSettings();


			//cleanup
			if (deferredLightingPassMaterial != null) {
				GameObject.Destroy(deferredLightingPassMaterial);
			}
			if (proxyCamera != null) {
				GameObject.Destroy(proxyCamera.gameObject);
			}
			if (volumeSliceCamera != null) {
				GameObject.Destroy(volumeSliceCamera.gameObject);
				GameObject.DestroyImmediate(volumeSliceTexture);
			}
            Camera.onPreRender -= OnPreRenderMainCamera;
		}

        private IEnumerator UpdateGlobalIlluminationLoop()
        {
            while (true)
            {
                if ((renderingPath == RenderingPath.Deferred) &&
                    updateMode != GlobalIlluminationUpdateMode.NoUpdating && !updatingGlobalIllumination)
                {
                    UpdateGlobalIllumination();
                }
                if (updateMode == GlobalIlluminationUpdateMode.NoUpdating)
                {
                    yield return new WaitForSeconds(1.5f);
                }

                yield return new WaitForSeconds(1.0f / 60.0f);
            }
        }

		//refreshs and updates lighting system settings and quality
        /// <summary>
        /// Updates/refreshes settings.
        /// </summary>
		public void UpdateSettings() {

            ramUsed = 0;

            mainCamera = Camera.main;
            if (mainCamera != null) mainCamTransform = mainCamera.transform;

			//clean up
            if (proxyCamera != null)
            {
               mainCamera.cullingMask = mainCamerasOldCullingMask;
               proxyCamera.RemoveCommandBuffer(CameraEvent.BeforeImageEffectsOpaque, deferredLightingPass);
               GameObject.Destroy(proxyCamera.gameObject);
               proxyCamera = null;
            }

            GameObject.DestroyImmediate(renderBuffer2);
		    GameObject.DestroyImmediate(displayTexture);
            GameObject.DestroyImmediate(renderTexture);
            GameObject.DestroyImmediate(renderBuffer);
            GameObject.DestroyImmediate(lastFrame);
            GameObject.DestroyImmediate(lastFrame2);
            GameObject.DestroyImmediate(volumeSliceTexture);
	        GameObject.DestroyImmediate(materialVolume);
            GameObject.DestroyImmediate(distanceVolume);
            GameObject.DestroyImmediate(lightingVolume);
            GameObject.DestroyImmediate(volumeBuffer);
            GameObject.DestroyImmediate(distanceBuffer);
            GameObject.DestroyImmediate(loLightingVolume);
            GameObject.DestroyImmediate(loMaterialVolume);
            GameObject.DestroyImmediate(displayedMaterialVolume);
            GameObject.DestroyImmediate(displayedLightingVolume);
            if (deferredLightingPass != null) deferredLightingPass.Dispose();

		    Camera.onPreRender -= OnPreRenderMainCamera;


            //check new rendering path then init settings
            RenderingPath oldPath = renderingPath;
			if (overridePath) renderingPath = newPath;

			if (renderingPath == RenderingPath.ForwardUnlit) {
                if (deferredCameraDisplay != null) deferredCameraDisplay.enabled = false;
                
                //enable directional lights
                Light[] dl = Object.FindObjectsOfType<Light>();
                foreach (Light light in dl) {
                    if (light.type == LightType.Directional) light.enabled = true;
                }

                //toggle unlit forward
                Shader.EnableKeyword("EGK_NOLIGHTING");
				Shader.DisableKeyword("EGK_DEFERREDGI");
                return;
			}

                //check if device has all requirements for deferred gi
                if (!IsDGISupported())
                {
                    //if not supported reload with forward
                    Debug.LogError("Ethans Lighting System - Cannot enable deferred global illumination because it is not supported on this platform, falling back to forward unlit.");
                    renderingPath = RenderingPath.ForwardUnlit;
                    UpdateSettings();
                    return;
                }

                if (screenSpaceShadows)
                {
                    Shader.EnableKeyword("EGK_SSDIRECTSHADOWS_ON");
                    Shader.DisableKeyword("EGK_SSDIRECTSHADOWS_OFF");
                }
                else
                {
                    Shader.EnableKeyword("EGK_SSDIRECTSHADOWS_OFF");
                    Shader.DisableKeyword("EGK_SSDIRECTSHADOWS_ON");
                }

                if (realtimeFinalGather)
                {
                    Shader.EnableKeyword("EGK_RTFINALGATHER_ON");
                    Shader.DisableKeyword("EGK_RTFINALGATHER_OFF");
                }
                else
                {
                    Shader.EnableKeyword("EGK_RTFINALGATHER_OFF");
                    Shader.DisableKeyword("EGK_RTFINALGATHER_ON");
                }


                //disable directional lights
            Light[] dll = Object.FindObjectsOfType<Light>();
                foreach (Light light in dll)
                {
                    if (light.type == LightType.Directional) light.enabled = false;
                }

            //set fog params
            Shader.SetGlobalColor("_EGKFogColor", fogColor);
            Shader.SetGlobalVector("_EGKFogParams", new Vector3(fogDepth, fogIntensity, fogPower));

            //init
            renderScreenWidth = (int)(Screen.width * renderScale);
            renderScreenHeight = (int)(Screen.height * renderScale);

            qualityValue = (int)quality;
            qualityHalfValue = qualityValue / 2;
            if (levelOfDetailCascade)
            {
                qualityValue = qualityHalfValue;
                qualityHalfValue = qualityValue/2;
                loVoxelSize = (lightmapDistance / (float)qualityHalfValue)*levelOfDetailCascadeScale;

                Shader.EnableKeyword("EGK_LODCASCADE_ON");
                Shader.DisableKeyword("EGK_LODCASCADE_OFF");
            }
            else
            {
                Shader.EnableKeyword("EGK_LODCASCADE_OFF");
                Shader.DisableKeyword("EGK_LODCASCADE_ON");
            }
            voxelSize = lightmapDistance / (float)qualityHalfValue;



            UpdateBrightnessScale();
            UpdateBackgroundSettings();

            //init material, mesh, shader and texture objects
            addSliceToVolumeKernelIndex = addSliceToVolumeCS.FindKernel("AddSliceToVolumeKernel");

            initGeoVolumeKernelIndex = generateGeometryVolumeCS.FindKernel("InitializeGeometryVolume");
            sweepGeoVolumeKernelIndex = generateGeometryVolumeCS.FindKernel("SweepGeometryVolume");

            emptyVolumeKernelIndex = lightingVolumeCS.FindKernel("EmptyVolumeKernel");
            copyVolumeKernelIndex = lightingVolumeCS.FindKernel("CopyVolumeKernel");
            directionalLightKernelIndex = lightingVolumeCS.FindKernel("DirectionalLightKernel");
            pathTraceKernelIndex = lightingVolumeCS.FindKernel("PathTraceKernel");
            directionalLightKernelIndexLOD = lightingVolumeCS.FindKernel("DirectionalLightKernelLOD");
            pathTraceKernelIndexLOD = lightingVolumeCS.FindKernel("PathTraceKernelLOD");
            addBounceKernelIndex = lightingVolumeCS.FindKernel("BounceBrightenKernel");
            lightPenetrationKernelIndex = lightingVolumeCS.FindKernel("LightPenetrationKernel");
            balanceLightKernelIndex = lightingVolumeCS.FindKernel("BalanceLightKernel");
            sphereLightKernelIndex = lightingVolumeCS.FindKernel("SphereLightKernel");
            sphereLightKernelIndexLOD = lightingVolumeCS.FindKernel("SphereLightKernelLOD");


            //voxel volumes
            volumeSliceTexture = new RenderTexture(qualityValue, qualityValue, 0, RenderTextureFormat.ARGB32);
            ramUsed += qualityValue * qualityValue * 4;


            int volumeWidth = qualityValue;
            if (levelOfDetailCascade) volumeWidth *= 2;

            materialVolume = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.ARGB32);
            ramUsed += qualityValue * qualityValue * qualityValue * 4;
            
            distanceVolume = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.R8);
            ramUsed += qualityValue * qualityValue * qualityValue * 1;
            
            distanceBuffer = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.R8);
            ramUsed += qualityValue * qualityValue * qualityValue * 1;

            displayedMaterialVolume = Create3DRenderTexture(volumeWidth, qualityValue, qualityValue, RenderTextureFormat.ARGB32);
            ramUsed += volumeWidth * qualityValue * qualityValue * 4;

            lightingVolume = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.ARGBHalf);
            ramUsed += qualityValue * qualityValue * qualityValue * 8;

            displayedLightingVolume = Create3DRenderTexture(volumeWidth, qualityValue, qualityValue, RenderTextureFormat.ARGBHalf);
            ramUsed += volumeWidth * qualityValue * qualityValue * 8;

            volumeBuffer = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.ARGBHalf);
            ramUsed += qualityValue * qualityValue * qualityValue * 8;

            if (levelOfDetailCascade)
            {
                loMaterialVolume = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.ARGB32);
                ramUsed += qualityValue * qualityValue * qualityValue * 4;

                loLightingVolume = Create3DRenderTexture(qualityValue, qualityValue, qualityValue, RenderTextureFormat.ARGBHalf);
                ramUsed += qualityValue * qualityValue * qualityValue * 8;
            }


            //setup volume slice rendering camera
            if (volumeSliceCamera == null)
            {
                GameObject camObject = new GameObject();
                volumeSliceCamera = camObject.AddComponent<Camera>();
                volumeSliceCamera.gameObject.SetActive(false);
                volumeSliceCamera.cullingMask = cullingMask;
                volumeSliceCamera.orthographic = true;
                volumeSliceCamera.aspect = 1.0f;
                volumeSliceCamera.nearClipPlane = 0.0f;
                volumeSliceCamera.clearFlags = CameraClearFlags.SolidColor;
                volumeSliceCamera.backgroundColor = new Color(0, 0, 0, 0);
                volumeSliceCamera.transform.localRotation = Quaternion.identity;
                camObject.name = "EGK Lighting System Volume Slice Camera";
            }
            volumeSliceCamera.orthographicSize = qualityHalfValue * voxelSize;
            volumeSliceCamera.farClipPlane = voxelSize;
            volumeSliceCamera.targetTexture = volumeSliceTexture;


                //setup deferred GI
                if (deferredLightingPassMaterial == null) deferredLightingPassMaterial = new Material(lightingPassShader);

                //render textures
                renderTexture = new RenderTexture(renderScreenWidth, renderScreenHeight, 24, RenderTextureFormat.ARGBFloat);
                renderTexture.filterMode = renderScaleFilterMode;
                ramUsed += renderScreenWidth * renderScreenHeight * 16;
                renderBuffer = new RenderTexture(renderScreenWidth, renderScreenHeight, 0, RenderTextureFormat.ARGBFloat);
                ramUsed += renderScreenWidth * renderScreenHeight * 16;
                lastFrame = new RenderTexture(renderScreenWidth, renderScreenHeight, 0, RenderTextureFormat.ARGBHalf);
                ramUsed += renderScreenWidth * renderScreenHeight * 8;
                lastFrame2 = new RenderTexture(renderScreenWidth, renderScreenHeight, 0, RenderTextureFormat.ARGBHalf);
                ramUsed += renderScreenWidth * renderScreenHeight * 8;
                displayTexture = new RenderTexture(renderScreenWidth, renderScreenHeight, 0, RenderTextureFormat.ARGBHalf);
                
                ramUsed += renderScreenWidth * renderScreenHeight * 8;

                //setup cameras for custom deferred rendering
                mainCamera.depth = 1;
                mainCamerasOldCullingMask = mainCamera.cullingMask;
                if (deferredCameraDisplay != null) deferredCameraDisplay.enabled = true;
                else deferredCameraDisplay = mainCamera.gameObject.AddComponent<LightingSystemCameraDisplay>();
                mainCamera.cullingMask = 0;

                if (proxyCamera == null)
                {
                    GameObject camObject = new GameObject();
                    camObject.transform.SetParent(mainCamera.transform, false);
                    proxyCamera = camObject.AddComponent<Camera>();
                    proxyCamera.name = "EGK Lighting System Proxy Camera";
                }
                proxyCamera.cullingMask = mainCamerasOldCullingMask;
                proxyCamera.fieldOfView = mainCamera.fieldOfView;
                proxyCamera.nearClipPlane = mainCamera.nearClipPlane;
                proxyCamera.farClipPlane = mainCamera.farClipPlane;
                proxyCamera.targetTexture = renderTexture;
                proxyCamera.depth = 0;
                proxyCamera.depthTextureMode = DepthTextureMode.Depth;
                proxyCamera.hdr = true;

                Camera.onPreRender += OnPreRenderMainCamera;


                //deferred lighting pass
                deferredLightingPass = new CommandBuffer();
                deferredLightingPass.name = "EGK Lighting System - Deferred Lighting";

                UpdateLightingPass();

               proxyCamera.AddCommandBuffer(CameraEvent.BeforeImageEffectsOpaque, deferredLightingPass);


                //enable deferred GI
                Shader.EnableKeyword("EGK_DEFERREDGI");
                Shader.DisableKeyword("EGK_NOLIGHTING");            

			if (overridePath) renderingPath = oldPath;
		}

        //update lighting pass command buffer
        /// <summary>
        /// Update deferred lighting pass(used for updating dynamic directional/point lights).
        /// </summary>
        public void UpdateLightingPass()
        {
            if (deferredLightingPass == null) return;//not initialized yet

            deferredLightingPass.Clear();
            
            //pre-lighting post-fx, alter renderbuffer to change global material look
            RenderTexture renderBuf1 = renderTexture,
                          renderBuf2 = renderBuffer2;
            deferredLightingPass.SetGlobalTexture("_RenderBuffer", renderBuf1);
            for (int i = 0; i < prelightingPostEffects.Count; i++)
            {
                if (renderBuffer2 == null)
                {
                    //allocate second render buffer
                    renderBuffer2 = new RenderTexture(renderScreenWidth, renderScreenHeight, 0, RenderTextureFormat.ARGBFloat);
                    ramUsed += renderScreenWidth * renderScreenHeight * 16;
                }
                
                deferredLightingPass.Blit((Texture)null, renderBuf2, prelightingPostEffects[i].material, prelightingPostEffects[i].passIndex);

                RenderTexture tempbuf = renderBuf1;
                renderBuf1 = renderBuf2;
                renderBuf2 = tempbuf;
                deferredLightingPass.SetGlobalTexture("_RenderBuffer", renderBuf1);
            }

            //lighting
            deferredLightingPass.SetGlobalVector("_EGKGIParams", new Vector2(realtimeSteps, realtimePaths));
            deferredLightingPass.Blit((Texture)null, lastFrame2, deferredLightingPassMaterial, 0);

            //temporal blend
            deferredLightingPass.SetGlobalTexture("_CurrentFrame", lastFrame2);
            deferredLightingPass.SetGlobalTexture("_LastRenderBuffer", renderBuffer);
            deferredLightingPass.SetGlobalTexture("_LastFrame", lastFrame);
            deferredLightingPass.Blit((Texture)null, displayTexture, deferredLightingPassMaterial, 9);

            //copy render buf to last buf
            deferredLightingPass.Blit((Texture)null, renderBuffer, deferredLightingPassMaterial, 2);

            //copy diffuse to last buf
            deferredLightingPass.SetGlobalTexture("_RenderBuffer", displayTexture);
            deferredLightingPass.Blit((Texture)null, lastFrame, deferredLightingPassMaterial, 1);

            //apply dynamic lights
            if (screenSpaceShadows) deferredLightingPass.SetGlobalVector("_SSShadowParams", new Vector2(screenSpaceShadowIterations,screenSpaceShadowDistance));

            deferredLightingPass.SetGlobalTexture("_RenderBuffer", renderTexture);
            RenderTexture rtb = lastFrame2, rta = displayTexture, rtc = rtb;
                             
            List<DirectionalLight> dirLights = DirectionalLight.GetDirectionalLights();
            for (int i = 0; i < dirLights.Count; i++)
            {
                DirectionalLight dl = dirLights[i];

                deferredLightingPass.SetGlobalTexture("_LastFrame", rta);
                Vector3 dir = dl.transform.forward;
                deferredLightingPass.SetGlobalVector("_Light", new Vector4(dir.x,dir.y,dir.z,dl.intensity));
                deferredLightingPass.SetGlobalVector("_LightColor", dl.color);
                deferredLightingPass.Blit((Texture)null, rtb, deferredLightingPassMaterial, 7);

                rtb = rta;
                rta = rtc;
                rtc = rtb;
            }

            
            List<SphereLight> sphereLights = SphereLight.GetSphereLights();
            for (int i = 0; i < sphereLights.Count; i++)
            {
                SphereLight dl = sphereLights[i];

                deferredLightingPass.SetGlobalTexture("_LastFrame", rta);
                Vector3 dir = dl.transform.position;
                deferredLightingPass.SetGlobalVector("_Light", new Vector4(dir.x, dir.y, dir.z, dl.intensity));
                deferredLightingPass.SetGlobalVector("_LightColor", new Vector4(dl.color.r, dl.color.g, dl.color.b, 0.0f));
                if (dl.cookieTexture)
                {
                    deferredLightingPassMaterial.SetTexture("_CookieTexture", dl.cookieTexture);
                    deferredLightingPass.SetGlobalFloat("_Cookie", 1.0f);
                    deferredLightingPass.SetGlobalVector("_CookieTransform", dl.cookieTextureTransform);
                }
                else
                {
                    deferredLightingPass.SetGlobalFloat("_Cookie", 0.0f);
                }
                deferredLightingPass.Blit((Texture)null, rtb, deferredLightingPassMaterial, 8);

                rtb = rta;
                rta = rtc;
                rtc = rtb;
            }
            
            //ss blur
            for (int i = 0; i < denoiseIterations; i++)
            {
                //horiz blur
                deferredLightingPass.SetGlobalTexture("_LastFrame", rta);
                deferredLightingPass.Blit((Texture)null, rtb, deferredLightingPassMaterial, 3);

                rtb = rta;
                rta = rtc;
                rtc = rtb;

                //vertical blur
                deferredLightingPass.SetGlobalTexture("_LastFrame", rta);
                deferredLightingPass.Blit((Texture)null, rtb, deferredLightingPassMaterial, 4);

                rtb = rta;
                rta = rtc;
                rtc = rtb;
            }

            //copy buffer over for transparent pass and gamma correct
            deferredLightingPass.SetGlobalTexture("_RenderBuffer", rta);
            deferredLightingPass.Blit((Texture)null, renderTexture, deferredLightingPassMaterial, 6);
        }

        /// <summary>
        /// Update background color/skybox changes made to camera/scene.
        /// </summary>
        public void UpdateBackgroundSettings()
        {
            if (mainCamera == null) return;
            bool clearSkybox = mainCamera.clearFlags == CameraClearFlags.Skybox;
            Texture skyboxCubemap = null;
            Color clearColor = Color.black;
            if (clearSkybox)
            {
                if (RenderSettings.skybox != null && RenderSettings.skybox.HasProperty("_Tex"))
                {
                    skyboxCubemap = RenderSettings.skybox.GetTexture("_Tex");
                }
                else
                {
                    clearSkybox = false;
                }
            }
            else
            {
                clearColor = mainCamera.backgroundColor;
                
                clearColor.r = Mathf.Pow(clearColor.r, 2.2f);
                clearColor.g = Mathf.Pow(clearColor.g, 2.2f);
                clearColor.b = Mathf.Pow(clearColor.b, 2.2f);
            }

            Shader.SetGlobalVector("_EGKBackgroundColor", new Vector4(clearColor.r, clearColor.g, clearColor.b, backgroundLightIntensity));
            Shader.SetGlobalFloat("_EGKBackgroundType", clearSkybox ? 1.0f : 0.0f);
            if (clearSkybox)
            {
                Shader.SetGlobalTexture("_EGKBackgroundCube", skyboxCubemap);
            }
        }



        private Matrix4x4 lastCameraToWorldMatrix;
        private Vector3 lastCameraPos;
        public void OnPreRenderMainCamera(Camera mainCam)
        {
            //update needed camera matrices
            if (proxyCamera != mainCam) return;
            Shader.SetGlobalMatrix("_EGKLastWorldToCamera", lastCameraToWorldMatrix);
            Shader.SetGlobalMatrix("_EGKCameraToWorld", mainCam.cameraToWorldMatrix);
            lastCameraToWorldMatrix = mainCam.cameraToWorldMatrix.transpose;
            Shader.SetGlobalMatrix("_EGKWorldToCamera", lastCameraToWorldMatrix);
            Vector3 newCameraPos = mainCamTransform.position;
            Shader.SetGlobalVector("_EGKLastCameraPos", lastCameraPos);
            lastCameraPos = newCameraPos;
        }


        /// <summary>
        /// Attempts to update global illumination, returns true if started updating successfully.
        /// </summary>
        /// <returns></returns>
		public bool UpdateGlobalIllumination() {
			if (updatingGlobalIllumination) return false;
			updatingGlobalIllumination = true;
			StartCoroutine(UpdateGlobalIllumination_BE());
            return true;
		}
		private IEnumerator UpdateGlobalIllumination_BE() {
            if (renderingPath == RenderingPath.ForwardUnlit) yield break;

			Vector3 origin = transform.position,
						buf;

            //setup background light
            bool clearSkybox = mainCamera.clearFlags == CameraClearFlags.Skybox;
            Texture skyboxCubemap = null;
            Color clearColor = Color.black;
            if (clearSkybox)
            {
                if (RenderSettings.skybox != null && RenderSettings.skybox.HasProperty("_Tex"))
                {
                    skyboxCubemap = RenderSettings.skybox.GetTexture("_Tex");
                }
                else
                {
                    clearSkybox = false;
                }
            } else {
                clearColor = mainCamera.backgroundColor;
                clearColor.r = Mathf.Pow(clearColor.r, 2.2f);
                clearColor.g = Mathf.Pow(clearColor.g, 2.2f);
                clearColor.b = Mathf.Pow(clearColor.b, 2.2f);
            }


            //lightmap voxel origin
            if (levelOfDetailCascade)
            {
                origin.x = Mathf.Floor(origin.x / loVoxelSize) * loVoxelSize;
                origin.y = Mathf.Floor(origin.y / loVoxelSize) * loVoxelSize;
                origin.z = Mathf.Floor(origin.z / loVoxelSize) * loVoxelSize;
            }
            else
            {
                origin.x = Mathf.Floor(origin.x / voxelSize) * voxelSize;
                origin.y = Mathf.Floor(origin.y / voxelSize) * voxelSize;
                origin.z = Mathf.Floor(origin.z / voxelSize) * voxelSize;
            }

            int workLoads = qualityValue/8;

			Shader.SetGlobalVector("_EGKTempOrigin", 
				new Vector3(origin.x-qualityHalfValue*voxelSize,
					origin.y-qualityHalfValue*voxelSize,
					origin.z-qualityHalfValue*voxelSize));
			Shader.SetGlobalVector("_EGKTempParams", 
				new Vector4(qualityValue,
					qualityHalfValue,
					voxelSize,
                    loVoxelSize));

            long timeRef = System.DateTime.Now.Ticks/10000,
                 nTimeRef;


            RenderTexture rta,rtb,rtc,dstb,mta,mtb,mtc;


			//generate voxel material volume
			addSliceToVolumeCS.SetTexture(addSliceToVolumeKernelIndex, "InputTexture", volumeSliceTexture);
			addSliceToVolumeCS.SetTexture(addSliceToVolumeKernelIndex, "OutputVolume", materialVolume);

			buf = origin;
			buf.z -= qualityHalfValue*voxelSize;
			for (int z = 0; z < qualityValue; z++) {
				volumeSliceCamera.transform.localPosition = buf;
                Shader.SetGlobalFloat("_EGKVolumeSlice", (float)z);
                volumeSliceCamera.RenderWithShader(voxelMaterialPassShader, null);

				addSliceToVolumeCS.SetInt("VolumeSlice", z);
				addSliceToVolumeCS.Dispatch(addSliceToVolumeKernelIndex, Mathf.Max(1,qualityValue/32), Mathf.Max(1,qualityValue/32), 1);

				buf.z += voxelSize;

                if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                {
                    nTimeRef = System.DateTime.Now.Ticks / 10000;
                    if (nTimeRef - timeRef > 2)
                    {
                        //if been generating volume slices for over 2 ms wait to give gpu time to render frame
                        timeRef = nTimeRef;
                        yield return null;
                    }
                }
			}

            if (levelOfDetailCascade)
            {
                Shader.SetGlobalVector("_EGKTempOrigin",
                    new Vector3(origin.x - qualityHalfValue * loVoxelSize,
                        origin.y - qualityHalfValue * loVoxelSize,
                        origin.z - qualityHalfValue * loVoxelSize));
                Shader.SetGlobalVector("_EGKTempParams",
                    new Vector4(qualityValue,
                        qualityHalfValue,
                        loVoxelSize,
                        loVoxelSize));
                
                //if level of detail cascade, generate lower quality volume
                volumeSliceCamera.orthographicSize = qualityHalfValue * loVoxelSize;
                volumeSliceCamera.farClipPlane = loVoxelSize;

                addSliceToVolumeCS.SetTexture(addSliceToVolumeKernelIndex, "OutputVolume", loMaterialVolume);

                buf = origin;
                buf.z -= qualityHalfValue * loVoxelSize;
                for (int z = 0; z < qualityValue; z++)
                {
                    volumeSliceCamera.transform.localPosition = buf;
                    Shader.SetGlobalFloat("_EGKVolumeSlice", (float)z);
                    volumeSliceCamera.RenderWithShader(voxelMaterialPassShader, null);
                    addSliceToVolumeCS.SetInt("VolumeSlice", z);
                    addSliceToVolumeCS.Dispatch(addSliceToVolumeKernelIndex, Mathf.Max(1,qualityValue / 32), Mathf.Max(1,qualityValue / 32), 1);

                    buf.z += loVoxelSize;

                    if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                    {
                        nTimeRef = System.DateTime.Now.Ticks / 10000;
                        if (nTimeRef - timeRef > 2)
                        {
                            //if been generating volume slices for over 2 ms wait to give gpu time to render frame
                            timeRef = nTimeRef;
                            yield return null;
                        }
                    }
                }

                volumeSliceCamera.orthographicSize = qualityHalfValue * voxelSize;
                volumeSliceCamera.farClipPlane = voxelSize;

                //generate geometry volume

                    //initialize, fill in with default values based on material/voxel volume
                    generateGeometryVolumeCS.SetTexture(initGeoVolumeKernelIndex, "MaterialInputVolume", loMaterialVolume);
                    generateGeometryVolumeCS.SetTexture(initGeoVolumeKernelIndex, "OutputVolume", distanceVolume);
                    generateGeometryVolumeCS.Dispatch(initGeoVolumeKernelIndex, workLoads, workLoads, workLoads);

                    //sweep 16 times to generate short distance field also transferring closest material colors up to 16 blocks away
                    rta = distanceVolume;
                    rtb = distanceBuffer;
                    rtc = rtb;
                    dstb = null;
                    mta = loMaterialVolume;
                    mtb = volumeBuffer;
                    mtc = mtb;

                    for (int i = 0; i < 16; i++)
                    {
                        generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "InputVolume", rta);
                        generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "MaterialInputVolume", mta);
                        generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "OutputVolume", rtb);
                        generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "MaterialOutputVolume", mtb);
                        generateGeometryVolumeCS.Dispatch(sweepGeoVolumeKernelIndex, workLoads, workLoads, workLoads);

                        //swap buffers
                        rtb = rta;
                        rta = rtc;
                        rtc = rtb;

                        mtb = mta;
                        mta = mtc;
                        mtc = mtb;

                        if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                        {
                            nTimeRef = System.DateTime.Now.Ticks / 10000;
                            if (nTimeRef - timeRef > 2)
                            {
                                timeRef = nTimeRef;
                                yield return null;
                            }
                        }
                    }


                    //setup empty lighting emissive volumes
                    lightingVolumeCS.SetTexture(emptyVolumeKernelIndex, "OutputVolume", lightingVolume);
                    lightingVolumeCS.Dispatch(emptyVolumeKernelIndex, workLoads, workLoads, workLoads);
                    lightingVolumeCS.SetTexture(emptyVolumeKernelIndex, "OutputVolume", volumeBuffer);
                    lightingVolumeCS.Dispatch(emptyVolumeKernelIndex, workLoads, workLoads, workLoads);


                    //render simple lights with shadows to emissive volume
                    dstb = rta;
                    rta = lightingVolume;
                    rtb = rtc = volumeBuffer;

                    lightingVolumeCS.SetTexture(directionalLightKernelIndex, "DistanceVolume", dstb);
                    lightingVolumeCS.SetTexture(directionalLightKernelIndex, "MaterialVolume", loMaterialVolume);

                    List<DirectionalLight> dirLights = DirectionalLight.GetDirectionalLights();
                    for (int i = 0; i < dirLights.Count; i++)
                    {
                        DirectionalLight dl = dirLights[i];
                        if (dl.dynamicOnly) continue;

                        lightingVolumeCS.SetTexture(directionalLightKernelIndex, "OutputVolume", rtb);
                        lightingVolumeCS.SetTexture(directionalLightKernelIndex, "InputVolume", rta);
                        lightingVolumeCS.SetVector("LightColor", (Vector4)dl.GetColorWithIntensity());
                        lightingVolumeCS.SetVector("LightParams", dl.transform.forward);
                        lightingVolumeCS.SetInt("PathTraceSteps", shadowRayTraceSteps);

                        lightingVolumeCS.Dispatch(directionalLightKernelIndex, workLoads, workLoads, workLoads);

                        rtb = rta;
                        rta = rtc;
                        rtc = rtb;

                        if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                        {
                            nTimeRef = System.DateTime.Now.Ticks / 10000;
                            if (nTimeRef - timeRef > 2)
                            {
                                timeRef = nTimeRef;
                                yield return null;
                            }
                        }
                    }


                    lightingVolumeCS.SetTexture(sphereLightKernelIndex, "DistanceVolume", dstb);
                    lightingVolumeCS.SetTexture(sphereLightKernelIndex, "MaterialVolume", loMaterialVolume);

                    List<SphereLight> sphLights = SphereLight.GetSphereLights();
                    lightingVolumeCS.SetFloat("VoxelScale", loVoxelSize);

                    for (int i = 0; i < sphLights.Count; i++)
                    {
                        SphereLight dl = sphLights[i];
                        if (dl.dynamicOnly) continue;

                        lightingVolumeCS.SetTexture(sphereLightKernelIndex, "OutputVolume", rtb);
                        lightingVolumeCS.SetTexture(sphereLightKernelIndex, "InputVolume", rta);
                        lightingVolumeCS.SetVector("LightColor", (Vector4)dl.GetColorWithIntensity());
                        Vector3 p = dl.transform.position;
                        p -= origin - Vector3.one * qualityHalfValue * loVoxelSize;
                        p /= loVoxelSize;
                        lightingVolumeCS.SetVector("LightParams", new Vector4(p.x,p.y,p.z,0.0f));
                        if (dl.cookieTexture != null)
                        {
                            lightingVolumeCS.SetTexture(sphereLightKernelIndex, "CookieTexture", dl.cookieTexture);
                            lightingVolumeCS.SetVector("CookieTransform", dl.cookieTextureTransform);
                            lightingVolumeCS.SetFloat("Cookie", 1.0f);
                        }
                        else
                        {
                            lightingVolumeCS.SetFloat("Cookie", 0.0f);
                        }
                        lightingVolumeCS.SetInt("PathTraceSteps", shadowRayTraceSteps);

                        lightingVolumeCS.Dispatch(sphereLightKernelIndex, workLoads, workLoads, workLoads);

                        rtb = rta;
                        rta = rtc;
                        rtc = rtb;

                        if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                        {
                            nTimeRef = System.DateTime.Now.Ticks / 10000;
                            if (nTimeRef - timeRef > 2)
                            {
                                timeRef = nTimeRef;
                                yield return null;
                            }
                        }
                    }

                    //balance light for bounces
                    if (bounces != 0 && (dirLights.Count > 0 || sphLights.Count > 0))
                    {
                        lightingVolumeCS.SetVector("PathBounceParams", new Vector4(0.0f, 0.0f, 0.0f, bounces));
                        lightingVolumeCS.SetTexture(balanceLightKernelIndex, "OutputVolume", rtb);
                        lightingVolumeCS.SetTexture(balanceLightKernelIndex, "InputVolume", rta);
                        lightingVolumeCS.Dispatch(balanceLightKernelIndex, workLoads, workLoads, workLoads);

                        rtb = rta;
                        rta = rtc;
                        rtc = rtb;
                    }

                    //scale number of low detail paths because they are farther away and need less detail
                    int loPaths = (int)(computedPaths / (levelOfDetailCascadeScale*2));
                    if (loPaths < 1) loPaths = 1;

                    //bounces/paths
                    lightingVolumeCS.SetTexture(pathTraceKernelIndex, "MaterialVolume", loMaterialVolume);
                    lightingVolumeCS.SetTexture(pathTraceKernelIndex, "DistanceVolume", dstb);
                    for (int b = 0; b < bounces; b++)
                    {
                        lightingVolumeCS.SetFloat("BackgroundType", clearSkybox ? 1.0f : 0.0f);
                        lightingVolumeCS.SetVector("BackgroundColor", new Vector4(clearColor.r, clearColor.g, clearColor.b, backgroundLightIntensity/bounces));
                        if (clearSkybox)
                        {
                            lightingVolumeCS.SetTexture(pathTraceKernelIndex, "BackgroundCube", skyboxCubemap);
                        }
                        lightingVolumeCS.SetVector("PathBounceParams", new Vector4(Mathf.Pow(2.0f, b), b * 64.0f + 1.0f, b + 1.0f, bounces));
                        lightingVolumeCS.SetTexture(pathTraceKernelIndex, "OutputVolume", rtb);
                        lightingVolumeCS.SetTexture(pathTraceKernelIndex, "InputVolume", rta);
                        lightingVolumeCS.SetInt("RayTraceSteps", computedSteps);
                        lightingVolumeCS.SetInt("NumberOfPaths", loPaths);
                        lightingVolumeCS.Dispatch(pathTraceKernelIndex, workLoads, workLoads, workLoads);

                        rtb = rta;
                        rta = rtc;
                        rtc = rtb;

                        if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                        {
                            nTimeRef = System.DateTime.Now.Ticks / 10000;
                            if (nTimeRef - timeRef > 2)
                            {
                                timeRef = nTimeRef;
                                yield return null;
                            }
                        }
                    }

                    //light penetration for more convergance in fast forward rendering
                    for (int i = 0; i < lightMapConvolutionSteps; i++)
                    {
                        lightingVolumeCS.SetTexture(lightPenetrationKernelIndex, "OutputVolume", rtb);
                        lightingVolumeCS.SetTexture(lightPenetrationKernelIndex, "InputVolume", rta);
                        lightingVolumeCS.Dispatch(lightPenetrationKernelIndex, workLoads, workLoads, workLoads);

                        rtb = rta;
                        rta = rtc;
                        rtc = rtb;
                    }


                    //copy and prepare light volume 
                    lightingVolumeCS.SetInt("VolumeCascade", 0);
                    lightingVolumeCS.SetVector("PathBounceParams", new Vector2(Mathf.Max(bounces, 1.0f), 0.0f));
                    lightingVolumeCS.SetTexture(addBounceKernelIndex, "DistanceInputVolume", dstb);
                    lightingVolumeCS.SetTexture(addBounceKernelIndex, "OutputVolume", loLightingVolume);
                    lightingVolumeCS.SetTexture(addBounceKernelIndex, "InputVolume", rta);
                    lightingVolumeCS.Dispatch(addBounceKernelIndex, workLoads, workLoads, workLoads);
            
            
            }



			//generate geometry volume

			//initialize, fill in with default values based on material/voxel volume
			generateGeometryVolumeCS.SetTexture(initGeoVolumeKernelIndex, "MaterialInputVolume", materialVolume);
			generateGeometryVolumeCS.SetTexture(initGeoVolumeKernelIndex, "OutputVolume", distanceVolume);
			generateGeometryVolumeCS.Dispatch(initGeoVolumeKernelIndex, workLoads, workLoads, workLoads);

			//sweep 16 times to generate short distance field also transferring closest material colors up to 16 blocks away
			rta = distanceVolume;
			rtb = distanceBuffer;
			rtc = rtb;
            dstb = null;
            
            mta = materialVolume;
            mtb = volumeBuffer;
            mtc = mtb;

			for (int i = 0; i < 16; i++) {
				generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "InputVolume", rta);
                generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "MaterialInputVolume", mta);
				generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "OutputVolume", rtb);
                generateGeometryVolumeCS.SetTexture(sweepGeoVolumeKernelIndex, "MaterialOutputVolume", mtb);
				generateGeometryVolumeCS.Dispatch(sweepGeoVolumeKernelIndex, workLoads, workLoads, workLoads);

                //swap buffers
				rtb = rta;
				rta = rtc;
				rtc = rtb;

                mtb = mta;
                mta = mtc;
                mtc = mtb;

                if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                {
                    nTimeRef = System.DateTime.Now.Ticks / 10000;
                    if (nTimeRef - timeRef > 2)
                    {
                        timeRef = nTimeRef;
                        yield return null;
                    }
                }
			}


            //setup empty lighting emissive volumes
            lightingVolumeCS.SetTexture(emptyVolumeKernelIndex, "OutputVolume", lightingVolume);
            lightingVolumeCS.Dispatch(emptyVolumeKernelIndex, workLoads, workLoads, workLoads);
            lightingVolumeCS.SetTexture(emptyVolumeKernelIndex, "OutputVolume", volumeBuffer);
            lightingVolumeCS.Dispatch(emptyVolumeKernelIndex, workLoads, workLoads, workLoads);


            //render simple non-area lights(directional/point) with shadows to emissive volume
            dstb = rta;
            rta = lightingVolume;
            rtb = rtc = volumeBuffer;

            int dlki;
            if (levelOfDetailCascade) dlki = directionalLightKernelIndexLOD;
            else dlki = directionalLightKernelIndex;

            lightingVolumeCS.SetTexture(dlki, "DistanceVolume", dstb);
            lightingVolumeCS.SetTexture(dlki, "MaterialVolume", materialVolume);
            if (levelOfDetailCascade)
            {
                lightingVolumeCS.SetTexture(dlki, "LOLightVolume", loLightingVolume);
                lightingVolumeCS.SetTexture(dlki, "LOMaterialVolume", loMaterialVolume);
                lightingVolumeCS.SetFloat("LODCascadeScale", levelOfDetailCascadeScale);
            }
            List<DirectionalLight> dLights = DirectionalLight.GetDirectionalLights();
            for (int i = 0; i < dLights.Count; i++)
            {
                DirectionalLight dl = dLights[i];
                if (dl.dynamicOnly) continue;

                lightingVolumeCS.SetTexture(dlki, "OutputVolume", rtb);
                lightingVolumeCS.SetTexture(dlki, "InputVolume", rta);
                lightingVolumeCS.SetVector("LightColor", (Vector4)dl.GetColorWithIntensity());
                lightingVolumeCS.SetVector("LightParams", dl.transform.forward);
                lightingVolumeCS.SetInt("PathTraceSteps", shadowRayTraceSteps);

                lightingVolumeCS.Dispatch(dlki, workLoads, workLoads, workLoads);

                rtb = rta;
                rta = rtc;
                rtc = rtb;

                if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                {
                    nTimeRef = System.DateTime.Now.Ticks / 10000;
                    if (nTimeRef - timeRef > 2)
                    {
                        timeRef = nTimeRef;
                        yield return null;
                    }
                 }
             }

            if (levelOfDetailCascade) dlki = sphereLightKernelIndexLOD;
            else dlki = sphereLightKernelIndex;

            lightingVolumeCS.SetTexture(dlki, "DistanceVolume", dstb);
            lightingVolumeCS.SetTexture(dlki, "MaterialVolume", materialVolume);
            if (levelOfDetailCascade)
            {
                lightingVolumeCS.SetTexture(dlki, "LOLightVolume", loLightingVolume);
                lightingVolumeCS.SetTexture(dlki, "LOMaterialVolume", loMaterialVolume);
                lightingVolumeCS.SetFloat("LODCascadeScale", levelOfDetailCascadeScale);
            }
            List<SphereLight> spLights = SphereLight.GetSphereLights();
            lightingVolumeCS.SetFloat("VoxelScale", voxelSize);
            for (int i = 0; i < spLights.Count; i++)
            {
                SphereLight dl = spLights[i];
                if (dl.dynamicOnly) continue;

                lightingVolumeCS.SetTexture(dlki, "OutputVolume", rtb);
                lightingVolumeCS.SetTexture(dlki, "InputVolume", rta);
                lightingVolumeCS.SetVector("LightColor", (Vector4)dl.GetColorWithIntensity());
                Vector3 p = dl.transform.position;
                p -= origin - Vector3.one * qualityHalfValue * voxelSize;
                p /= voxelSize;
                lightingVolumeCS.SetVector("LightParams", new Vector4(p.x, p.y, p.z, 0.0f));
                if (dl.cookieTexture != null)
                {
                    lightingVolumeCS.SetTexture(dlki, "CookieTexture", dl.cookieTexture);
                    lightingVolumeCS.SetVector("CookieTransform", dl.cookieTextureTransform);
                    lightingVolumeCS.SetFloat("Cookie", 1.0f);
                }
                else
                {
                    lightingVolumeCS.SetFloat("Cookie", 0.0f);
                }
                lightingVolumeCS.SetInt("PathTraceSteps", shadowRayTraceSteps);

                lightingVolumeCS.Dispatch(dlki, workLoads, workLoads, workLoads);

                rtb = rta;
                rta = rtc;
                rtc = rtb;

                if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                {
                    nTimeRef = System.DateTime.Now.Ticks / 10000;
                    if (nTimeRef - timeRef > 2)
                    {
                        timeRef = nTimeRef;
                        yield return null;
                    }
                }
            }

             //balance light for bounces
            
             if (bounces != 0 && (dLights.Count > 0 || spLights.Count > 0))
             {
                 lightingVolumeCS.SetVector("PathBounceParams", new Vector4(0.0f, 0.0f, 0.0f, bounces));
                 lightingVolumeCS.SetTexture(balanceLightKernelIndex, "OutputVolume", rtb);
                 lightingVolumeCS.SetTexture(balanceLightKernelIndex, "InputVolume", rta);
                 lightingVolumeCS.Dispatch(balanceLightKernelIndex, workLoads, workLoads, workLoads);

                 rtb = rta;
                 rta = rtc;
                 rtc = rtb;
             }
                
             //bounces/paths
             if (levelOfDetailCascade) dlki = pathTraceKernelIndexLOD;
             else dlki = pathTraceKernelIndex;
             lightingVolumeCS.SetTexture(dlki, "MaterialVolume", materialVolume);
             lightingVolumeCS.SetTexture(dlki, "DistanceVolume", dstb);
             if (levelOfDetailCascade)
             {
                 lightingVolumeCS.SetTexture(dlki, "LOLightVolume", loLightingVolume);
                 lightingVolumeCS.SetTexture(dlki, "LOMaterialVolume", loMaterialVolume);
                 lightingVolumeCS.SetFloat("LODCascadeScale", levelOfDetailCascadeScale);
             }
             for (int b = 0; b < bounces; b++)
             {
                lightingVolumeCS.SetFloat("BackgroundType", clearSkybox?1.0f:0.0f);
                lightingVolumeCS.SetVector("BackgroundColor", new Vector4(clearColor.r, clearColor.g, clearColor.b, backgroundLightIntensity/bounces));
                if (clearSkybox)
                {
                    lightingVolumeCS.SetTexture(dlki, "BackgroundCube", skyboxCubemap);
                }
                lightingVolumeCS.SetVector("PathBounceParams", new Vector4(Mathf.Pow(2.0f,b), b*64.0f + 1.0f, b+1.0f, bounces));
                lightingVolumeCS.SetTexture(dlki, "OutputVolume", rtb);
                lightingVolumeCS.SetTexture(dlki, "InputVolume", rta);
                lightingVolumeCS.SetInt("RayTraceSteps", computedSteps);
                lightingVolumeCS.SetInt("NumberOfPaths", computedPaths);
                lightingVolumeCS.Dispatch(dlki, workLoads, workLoads, workLoads);

                rtb = rta;
                rta = rtc;
                rtc = rtb;

                if (updateMode != GlobalIlluminationUpdateMode.Realtime)
                {
                    nTimeRef = System.DateTime.Now.Ticks / 10000;
                    if (nTimeRef - timeRef > 2)
                    {
                        timeRef = nTimeRef;
                        yield return null;
                    }
                }
            }

             //light convolution for more convergance
             for (int i = 0; i < lightMapConvolutionSteps; i++)
             {
                 lightingVolumeCS.SetTexture(lightPenetrationKernelIndex, "OutputVolume", rtb);
                 lightingVolumeCS.SetTexture(lightPenetrationKernelIndex, "InputVolume", rta);
                 lightingVolumeCS.Dispatch(lightPenetrationKernelIndex, workLoads, workLoads, workLoads);

                 rtb = rta;
                 rta = rtc;
                 rtc = rtb;
             }
             

             if (levelOfDetailCascade)
             {

                 //copy light, material and distance volumes into displayed volume buffer and set new params
                 lightingVolumeCS.SetInt("VolumeCascade", 0);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "OutputVolume", displayedMaterialVolume);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "InputVolume", materialVolume);
                 lightingVolumeCS.Dispatch(copyVolumeKernelIndex, workLoads, workLoads, workLoads);

                 lightingVolumeCS.SetInt("VolumeCascade", 0);
                 lightingVolumeCS.SetVector("PathBounceParams", new Vector2(Mathf.Max(bounces, 1.0f), 0.0f));
                 lightingVolumeCS.SetTexture(addBounceKernelIndex, "DistanceInputVolume", dstb);
                 lightingVolumeCS.SetTexture(addBounceKernelIndex, "OutputVolume", displayedLightingVolume);
                 lightingVolumeCS.SetTexture(addBounceKernelIndex, "InputVolume", rta);
                 lightingVolumeCS.Dispatch(addBounceKernelIndex, workLoads, workLoads, workLoads);


                 
                 lightingVolumeCS.SetInt("VolumeCascade", qualityValue);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "OutputVolume", displayedMaterialVolume);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "InputVolume", loMaterialVolume);
                 lightingVolumeCS.Dispatch(copyVolumeKernelIndex, workLoads, workLoads, workLoads);

                 lightingVolumeCS.SetInt("VolumeCascade", qualityValue);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "OutputVolume", displayedLightingVolume);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "InputVolume", loLightingVolume);
                 lightingVolumeCS.Dispatch(copyVolumeKernelIndex, workLoads, workLoads, workLoads);
                 
                  
             }
             else
             {
                 //copy light, material and distance volumes into displayed volume buffer and set new params
                 lightingVolumeCS.SetInt("VolumeCascade", 0);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "OutputVolume", displayedMaterialVolume);
                 lightingVolumeCS.SetTexture(copyVolumeKernelIndex, "InputVolume", materialVolume);
                 lightingVolumeCS.Dispatch(copyVolumeKernelIndex, workLoads, workLoads, workLoads);
                 

                 lightingVolumeCS.SetVector("PathBounceParams", new Vector2(Mathf.Max(bounces, 1.0f), 0.0f));
                 lightingVolumeCS.SetTexture(addBounceKernelIndex, "DistanceInputVolume", dstb);
                 lightingVolumeCS.SetTexture(addBounceKernelIndex, "OutputVolume", displayedLightingVolume);
                 lightingVolumeCS.SetTexture(addBounceKernelIndex, "InputVolume", rta);
                 lightingVolumeCS.Dispatch(addBounceKernelIndex, workLoads, workLoads, workLoads);
             }

            
            


            //update background settings
            Shader.SetGlobalVector("_EGKBackgroundColor", new Vector4(clearColor.r, clearColor.g, clearColor.b, backgroundLightIntensity));
            Shader.SetGlobalFloat("_EGKBackgroundType", clearSkybox ? 1.0f : 0.0f);
            if (clearSkybox)
            {
                Shader.SetGlobalTexture("_EGKBackgroundCube", skyboxCubemap);
            }

            //update params
            Shader.SetGlobalVector("_EGKOrigin",
                                    new Vector3(origin.x - qualityHalfValue * voxelSize,
                                    origin.y - qualityHalfValue * voxelSize,
                                    origin.z - qualityHalfValue * voxelSize));
            Shader.SetGlobalVector("_EGKParams",
                                     new Vector4(qualityValue,
                                                            qualityHalfValue,
                                                            voxelSize,
                                                            loVoxelSize));
            Shader.SetGlobalTexture("_EGKMaterial", displayedMaterialVolume);
            Shader.SetGlobalTexture("_EGKLighting", displayedLightingVolume);


			updatingGlobalIllumination = false;
		}

        //display result of rendering
        public void DisplayRenderResult(RenderTexture src, RenderTexture dst)
        {
            //Shader.SetGlobalTexture("_RenderBuffer", displayTexture);
            Shader.SetGlobalTexture("_RenderBuffer", renderTexture);
            Graphics.Blit(null, dst, deferredLightingPassMaterial, 5);
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns>True if global illumination is currently updating.</returns>
        public bool IsGlobalIlluminationUpdating()
        {
            return updatingGlobalIllumination;
        }

		//returns whether or not deferred global illumination is supported on this device
        /// <summary>
        /// 
        /// </summary>
        /// <returns> True if deferred global illumination is supported on this device.</returns>
		public static bool IsDGISupported() {
			return (SystemInfo.supportsRenderTextures && SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBFloat) && SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf) && SystemInfo.supports3DTextures && SystemInfo.graphicsShaderLevel > 49 && SystemInfo.supportsComputeShaders);
		}

        /// <summary>
        /// Update brightness
        /// </summary>
        public void UpdateBrightnessScale()
        {
            Shader.SetGlobalVector("_EGKDBrightnessScale", brightnessScale);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>Estimated number of bytes used on the GPU by the lighting system.</returns>
		public long GetRAMUsage() {
            return ramUsed;
		}

		public RenderTexture Create3DRenderTexture(int width, int height, int depth, RenderTextureFormat rtf) {
			RenderTexture rt = new RenderTexture(width, height, 0, rtf);
            rt.wrapMode = TextureWrapMode.Clamp;
			rt.volumeDepth = depth;
			rt.isVolume = true;
			rt.enableRandomWrite = true;
			rt.Create();
			return rt;
		}


        public void OnDrawGizmosSelected()
        {
            //show how far lightmap reaches
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireCube(transform.position, Vector3.one * lightmapDistance * 2.0f);
            if (levelOfDetailCascade) {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(transform.position, Vector3.one * lightmapDistance * levelOfDetailCascadeScale * 2.0f);
            }
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/Ethans Graphics Kit/Lighting System", false, 10)]
        static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            GameObject go = new GameObject("Lighting System");
            go.AddComponent<LightingSystem>();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
#endif
	}
}

/// <summary>
/// PrelightingEffect, screen-space material alterations in deferred rendering.
/// </summary>
public struct PrelightingEffect
{
    public Material material;
    public int passIndex;

    public PrelightingEffect(Material mat, int pass)
    {
        material = mat;
        passIndex = pass;
    }
}