#ifndef TERRAIN_SPLATMAP_COMMON_CGINC_INCLUDED
#define TERRAIN_SPLATMAP_COMMON_CGINC_INCLUDED

struct v2f
{
	float4 pos : SV_POSITION;
	float2 uv_Splat0 : TEXCOORD0;
	float2 uv_Splat1 : TEXCOORD1;
	float2 uv_Splat2 : TEXCOORD2;
	float2 uv_Splat3 : TEXCOORD3;
	float2 tc_Control : TEXCOORD4;	// Not prefixing '_Contorl' with 'uv' allows a tighter packing of interpolators, which is necessary to support directional lightmap.
#if defined(EGK_NOLIGHTING)
	float3 wpos : TEXCOORD5;
#endif
	float3 normal : TEXCOORD6;
#if defined(EGK_NOLIGHTING)
	LIGHTING_COORDS(7, 8)
#endif
#ifdef _TERRAIN_NORMAL_MAP
	half3 tspace1 : TEXCOORD9;
	half3 tspace2 : TEXCOORD10;
#endif
};

sampler2D _Control, _Splat0, _Splat1, _Splat2, _Splat3;
float4 _Control_ST, _Splat0_ST, _Splat1_ST, _Splat2_ST, _Splat3_ST;

half _Metallic0;
half _Metallic1;
half _Metallic2;
half _Metallic3;

half _Smoothness0;
half _Smoothness1;
half _Smoothness2;
half _Smoothness3;

#ifdef _TERRAIN_NORMAL_MAP
	sampler2D _Normal0, _Normal1, _Normal2, _Normal3;
#endif

void SplatmapVert(inout appdata_tan v, out v2f data)
{
	UNITY_INITIALIZE_OUTPUT(v2f, data);

	data.tc_Control = TRANSFORM_TEX(v.texcoord, _Control);	// Need to manually transform uv here, as we choose not to use 'uv' prefix for this texcoord.
	data.uv_Splat0 = TRANSFORM_TEX(v.texcoord, _Splat0);
	data.uv_Splat1 = TRANSFORM_TEX(v.texcoord, _Splat1);
	data.uv_Splat2 = TRANSFORM_TEX(v.texcoord, _Splat2);
	data.uv_Splat3 = TRANSFORM_TEX(v.texcoord, _Splat3);

	float4 pos = mul (UNITY_MATRIX_MVP, v.vertex);

#ifdef _TERRAIN_NORMAL_MAP
	v.tangent.xyz = cross(v.normal, float3(0,0,1));
	v.tangent.w = -1;
	half3 wNormal = UnityObjectToWorldNormal(v.normal);
	half3 wTangent = UnityObjectToWorldDir(v.tangent.xyz);
	// compute bitangent from cross product of normal and tangent
	half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
	half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
	// output the tangent space matrix
	data.normal = half3(wTangent.x, wBitangent.x, wNormal.x);
	data.tspace1 = half3(wTangent.y, wBitangent.y, wNormal.y);
	data.tspace2 = half3(wTangent.z, wBitangent.z, wNormal.z);
#else
	data.normal = UnityObjectToWorldNormal(v.normal);;
#endif
}

void SplatmapMix(v2f IN, half4 defaultAlpha, out fixed4 mixedDiffuse, out fixed metallic, inout fixed3 mixedNormal)
{
	half4 splat_control = tex2D(_Control, IN.tc_Control);
	half weight = dot(splat_control, half4(1,1,1,1));

	#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
		clip(weight - 0.0039 /*1/255*/);
	#endif

	// Normalize weights before lighting and restore weights in final modifier functions so that the overal
	// lighting result can be correctly weighted.
	splat_control /= (weight + 1e-3f);

	mixedDiffuse = 0.0f;
	mixedDiffuse += splat_control.r * tex2D(_Splat0, IN.uv_Splat0) * half4(1.0, 1.0, 1.0, defaultAlpha.r);
	mixedDiffuse += splat_control.g * tex2D(_Splat1, IN.uv_Splat1) * half4(1.0, 1.0, 1.0, defaultAlpha.g);
	mixedDiffuse += splat_control.b * tex2D(_Splat2, IN.uv_Splat2) * half4(1.0, 1.0, 1.0, defaultAlpha.b);
	mixedDiffuse += splat_control.a * tex2D(_Splat3, IN.uv_Splat3) * half4(1.0, 1.0, 1.0, defaultAlpha.a);
	metallic = dot(splat_control, half4(_Metallic0, _Metallic1, _Metallic2, _Metallic3));

	#ifdef _TERRAIN_NORMAL_MAP
		fixed4 nrm = 0.0f;
		nrm += splat_control.r * tex2D(_Normal0, IN.uv_Splat0);
		nrm += splat_control.g * tex2D(_Normal1, IN.uv_Splat1);
		nrm += splat_control.b * tex2D(_Normal2, IN.uv_Splat2);
		nrm += splat_control.a * tex2D(_Normal3, IN.uv_Splat3);
		mixedNormal = UnpackNormal(nrm);

		half3 worldNormal;
		worldNormal.x = dot(IN.normal, mixedNormal);
		worldNormal.y = dot(IN.tspace1, mixedNormal);
		worldNormal.z = dot(IN.tspace2, mixedNormal);
		mixedNormal = worldNormal;
	#else
		mixedNormal = IN.normal;
	#endif
}

#ifndef TERRAIN_SURFACE_OUTPUT
	#define TERRAIN_SURFACE_OUTPUT SurfaceOutput
#endif

#endif // TERRAIN_SPLATMAP_COMMON_CGINC_INCLUDED
