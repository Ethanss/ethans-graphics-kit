﻿//Ethan Alexander Shulman 2017

Shader "Ethans Lighting/Opaque Bumped"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_Roughness("Roughness", Range(0.0, 1.0)) = 0.0
		_Emission("Emission", Range(0.0, 1.0)) = 0.0
		_BumpMap("Normal Map", 2D) = "bump" {}
	}
	SubShader
	{
		LOD 100
		Tags{ "RenderType" = "Opaque" }

		Pass
		{
			Tags{ "LightMode" = "ForwardBase" "PassFlags" = "OnlyDirectional" }
			Lighting On

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase
			#pragma multi_compile EGK_NOLIGHTING EGK_DEFERREDGI
			#pragma multi_compile EGK_LODCASCADE_OFF EGK_LODCASCADE_ON
			
			#include "UnityCG.cginc"

			
			sampler2D _MainTex, _BumpMap;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic, _Roughness, _Emission;

#if defined(EGK_NOLIGHTING)
			#include "Lighting.cginc"
			#include "AutoLight.cginc"
			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float3 wpos : TEXCOORD1;
				float3 normal : TEXCOORD2;// tangent.x, bitangent.x, normal.x
				LIGHTING_COORDS(3, 4)
				half3 tspace1 : TEXCOORD6; // tangent.y, bitangent.y, normal.y
				half3 tspace2 : TEXCOORD7; // tangent.z, bitangent.z, normal.z
			};

			#include "EthansLighting.cginc"
			

			v2f vert (appdata_tan v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				half3 wNormal = UnityObjectToWorldNormal(v.normal);
				half3 wTangent = UnityObjectToWorldDir(v.tangent.xyz);
				// compute bitangent from cross product of normal and tangent
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
				// output the tangent space matrix
				o.normal = half3(wTangent.x, wBitangent.x, wNormal.x);
				o.tspace1 = half3(wTangent.y, wBitangent.y, wNormal.y);
				o.tspace2 = half3(wTangent.z, wBitangent.z, wNormal.z);
				o.wpos = mul(_Object2World, v.vertex).xyz;
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the normal map, and decode from the Unity encoding
				half3 tnormal = UnpackNormal(tex2D(_BumpMap, i.uv));
				// transform normal from tangent to world space
				half3 worldNormal;
				worldNormal.x = dot(i.normal, tnormal);
				worldNormal.y = dot(i.tspace1, tnormal);
				worldNormal.z = dot(i.tspace2, tnormal);
				i.normal = worldNormal;
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				// apply lighting
				col.xyz = computeLighting(i,col.xyz);
				return col;
			}
#endif


#if defined(EGK_DEFERREDGI)
			#include "EGKVolume.cginc"
			#pragma target 5.0

			struct vertexOutput
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;

				half3 tspace0 : TEXCOORD1; // tangent.x, bitangent.x, normal.x
				half3 tspace1 : TEXCOORD2; // tangent.y, bitangent.y, normal.y
				half3 tspace2 : TEXCOORD3; // tangent.z, bitangent.z, normal.z
			};

			vertexOutput vert(appdata_tan i)
			{
				vertexOutput o;
				o.vertex = mul(UNITY_MATRIX_MVP, i.vertex);

				half3 wNormal = UnityObjectToWorldNormal(i.normal);
				half3 wTangent = UnityObjectToWorldDir(i.tangent.xyz);
				// compute bitangent from cross product of normal and tangent
				half tangentSign = i.tangent.w * unity_WorldTransformParams.w;
				half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
				// output the tangent space matrix
				o.tspace0 = half3(wTangent.x, wBitangent.x, wNormal.x);
				o.tspace1 = half3(wTangent.y, wBitangent.y, wNormal.y);
				o.tspace2 = half3(wTangent.z, wBitangent.z, wNormal.z);

				o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
				return o;
			}

			float4 frag(vertexOutput i) : SV_Target
			{
				//diffuse texture and tint
				fixed3 diffuse = pow(tex2D(_MainTex, i.uv).xyz, 2.2);

				// sample the normal map, and decode from the Unity encoding
				half3 tnormal = UnpackNormal(tex2D(_BumpMap, i.uv));
				// transform normal from tangent to world space
				half3 worldNormal;
				worldNormal.x = dot(i.tspace0, tnormal);
				worldNormal.y = dot(i.tspace1, tnormal);
				worldNormal.z = dot(i.tspace2, tnormal);

				//pack material data into high precision float color
				return float4(encodeFixed3(diffuse*_DiffuseColor.xyz),
							encodeFixed3(diffuse*_SpecularColor.xyz),
							encodeFixed3(fixed3(_Metallic, _Roughness, _Emission)),
							encodeFixed3(normalize(worldNormal)*0.5 + 0.5));
			}
#endif
			ENDCG
		}
	}

	Fallback "Ethans Lighting/Hidden/Opaque Bumped_Fallback"
}
