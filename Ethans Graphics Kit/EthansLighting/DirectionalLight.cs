﻿//Ethan Alexander Shulman 2017

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections.Generic;

namespace EthansGraphicsKit.Lighting
{
    /// <summary>
    /// Directional light for EthansGraphicsKit.Lighting.
    /// </summary>
    public class DirectionalLight : MonoBehaviour
    {
        
        /// <summary>
        /// Color of the light.
        /// </summary>
        [Tooltip("The color of the light.")]
        public Color color = Color.white;
        
        /// <summary>
        /// Strength/intensity of the light(0.1 - 20.0).
        /// </summary>
        [Tooltip("The strength/intensity of the light.")]
        [Range(0.1f, 20.0f)]
        public float intensity = 10.0f;

        /// <summary>
        /// If enabled it will not contribute to the light map and only apply the dynamic lighting.
        /// </summary>
        [Tooltip("If enabled it will not contribute to the light map and only apply the dynamic lighting.")]
        public bool dynamicOnly = false;


        private static List<DirectionalLight> directionalLightsList = new List<DirectionalLight>();

        void OnEnable()
        {
            directionalLightsList.Add(this);
            if (LightingSystem.lightingSystem != null) LightingSystem.lightingSystem.UpdateLightingPass();
        }

        void OnDisable()
        {
            directionalLightsList.Remove(this);
            if (LightingSystem.lightingSystem != null) LightingSystem.lightingSystem.UpdateLightingPass();
        }

        /// <summary>
        /// Get color multiplied by intensity divided by 40.
        /// </summary>
        /// <returns></returns>
        public Color GetColorWithIntensity()
        {
            return color * (intensity/40.0f);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = color;
            Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10.0f);
        }

        /// <summary>
        /// Get list of all DirectionaLight's.
        /// </summary>
        /// <returns></returns>
        public static List<DirectionalLight> GetDirectionalLights()
        {
            return directionalLightsList;
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/Ethans Graphics Kit/Directional Light", false, 10)]
        static void CreateCustomGameObject(MenuCommand menuCommand)
        {
            GameObject go = new GameObject("Directional Light");
            go.AddComponent<DirectionalLight>();
            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
#endif
    }
}