//Ethan Alexander Shulman 2016
Shader "Hidden/EGKVoxelMaterialPass"
{
	Properties
	{
		_MainTex ("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_Roughness("Roughness", Range(0.0, 1.0)) = 0.0
		_Emission ("Emission", Range(0.0,1.0)) = 0.0
		_Cutoff("Cutoff", Range(0.0,1.0)) = 0.5
	}
	SubShader
	{
		LOD 100

		Pass
		{
			Tags { "RenderType"="Opaque"}

			ZTest Always
			ZWrite Off
			Cull Off
			Blend Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 5.0


			#include "UnityCG.cginc"


	
			//output voxel material approx color + emission data
			struct vertexInput
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};
			struct vertexOutput
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD1;
			};



			//lighting properties
			float3 _EGKTempOrigin;
			float4 _EGKTempParams;
			float _EGKVolumeSlice;

			//material properties
			sampler2D _MainTex;
			float4 _MainTex_ST;
			half _Cutoff;

			half4 _MainColor, _SpecularColor;
			half _Metallic, _Roughness, _Emission;


			float bestSign(float a,float b) {
				if (abs(b) > abs(a)) return sign(b);
				return sign(a);
			}
			
			vertexOutput vert (vertexInput i)
			{
				vertexOutput o;
				i.normal = UnityObjectToWorldNormal(i.normal);

				float3 world = mul(_Object2World, i.vertex).xyz;
				world = (floor((world - _EGKTempOrigin) / _EGKTempParams.z)+float3(0.5,0.5,0.5))*_EGKTempParams.z + _EGKTempOrigin;

				float dir = dot(i.normal,float3(0,0,-1.));
				float2 dm = i.normal.xy;
				if (dir != 0.) dm *= sign(dir);
				
				world.xy += dm*(world.z - (_EGKTempOrigin.z + (_EGKVolumeSlice)*_EGKTempParams.z));
				o.pos = mul(UNITY_MATRIX_VP, float4(world,1));
				o.uv = TRANSFORM_TEX(i.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (vertexOutput i) : SV_Target
			{
				//diffuse texture with tint
				fixed4 tex = tex2D(_MainTex, i.uv);
				if (tex.w < _Cutoff) {
					clip(-1.);
					return 0.;
				}
				fixed3 diffuse = tex.xyz;

				//output approx material color and alpha > 0 if voxel is there and > 1 if emissive
				return fixed4(diffuse*lerp(_MainColor.xyz, _SpecularColor, 0.5 + _Metallic*0.5), 1.0 / 256.0 + (_Emission*255.0) / 256.0);
			}

			ENDCG
		}
	}
}

