﻿Shader "Ethans Lighting/VertexLit Fallback" {
	Properties{
		_MainTex("Albedo Texture", 2D) = "white" {}
		_DiffuseColor("Diffuse Color", Color) = (1, 1, 1, 1)
		_SpecularColor("Specular Color", Color) = (1, 1, 1, 1)
		_Metallic("Metallic", Range(0.0, 1.0)) = 0.0
	}

		SubShader{
		LOD 100

		//Single pass
		Pass
		{
			Tags{ "Queue" = "Geometry" 
				"RenderType" = "Opaque" }

			CGPROGRAM
			//defines
			#pragma vertex vert
			#pragma fragment frag

			//material parameters
			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _DiffuseColor, _SpecularColor;
			float _Metallic;


			//includes
#include "UnityCG.cginc"
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 light : TEXCOORD1;
			};
			v2f vert(appdata_base i)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, i.vertex);
				o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
				//static diffuse vertex lighting
				o.light = float4(lerp(_DiffuseColor.xyz, _SpecularColor, 0.5 + _Metallic*0.5),
					max(0.0, 1.0 + dot(UnityObjectToWorldNormal(i.normal), normalize(float3(1.0, 1.0, 1.0)))) / 2.0);
				o.light.xyz *= o.light.w;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				//apply texture color tint and lighting
				fixed3 c = tex2D(_MainTex, i.uv).xyz*i.light.xyz;
				return fixed4(c, 1);
			}
				ENDCG
		}

									// Pass to render object as a shadow caster
									Pass{
															Name "ShadowCaster"
															Tags{ "LightMode" = "ShadowCaster" }

															CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile_shadowcaster
#include "UnityCG.cginc"

															struct v2f {
																V2F_SHADOW_CASTER;
															};

															v2f vert(appdata_base v)
															{
																v2f o;
																TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
																return o;
															}

															float4 frag(v2f i) : SV_Target
															{
																SHADOW_CASTER_FRAGMENT(i)
															}
															ENDCG

														}

	}

}
