﻿//Ethan Alexander Shulman 2017
Shader "Hidden/EGKProceduralTexture"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		//pass #0 - checker
		Pass
		{
			CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


			float4 _Params,
			_ColorA,
			_ColorB;

			fixed4 frag(v2f_img i) : SV_Target
			{
				i.uv *= _Params.w;
				return lerp(_ColorA, _ColorB, (floor(i.uv.x) + floor(i.uv.y))%2.);
			}
			ENDCG
		}

		//pass #1 - dot pattern
		Pass
			{
				CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


				float4 _Params,
				_ColorA,
				_ColorB;

#define smoothDistanceMix(d) clamp(d*_Params.x,0.,1.)


				fixed4 frag(v2f_img i) : SV_Target
				{
					i.uv *= _Params.w;
					return lerp(_ColorA, _ColorB, smoothDistanceMix((length((i.uv)%1.0 - 0.5) - _Params.y)));
				}
					ENDCG
			}

			//pass #2 - diamond pattern
			Pass
				{
					CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


					float4 _Params,
					_ColorA,
					_ColorB;

#define PI 3.14159265
#define smoothDistanceMix(d) clamp(d*_Params.x,0.,1.)


					fixed4 frag(v2f_img i) : SV_Target
					{
						i.uv *= _Params.w;
						return lerp(_ColorA, _ColorB, smoothDistanceMix(length(max(abs(mul((i.uv) % 1.0 - 0.5, float2x2(sin(PI / 4.), cos(PI / 4.), -cos(PI / 4.), sin(PI / 4.)))) - _Params.yz, 0.))));
					}
						ENDCG
				}

				//pass #3 - grid pattern
				Pass
					{
						CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


						float4 _Params,
						_ColorA,
						_ColorB;

#define smoothDistanceMix(d) clamp(d*_Params.x,0.,1.)


						fixed4 frag(v2f_img i) : SV_Target
						{
							i.uv *= _Params.w;
							return lerp(_ColorA, _ColorB, smoothDistanceMix(length(max(abs(((i.uv)%1.0 - 0.5)) - _Params.yz, 0.))));
						}
							ENDCG
					}

					//pass #4 - flower pattern
					Pass
						{
							CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


							float4 _Params,
							_ColorA,
							_ColorB;

#define PI 3.14159265
#define smoothDistanceMix(d) clamp(d*_Params.x,0.,1.)


							fixed4 frag(v2f_img i) : SV_Target
							{
								i.uv *= _Params.w/10.0;
								return lerp(_ColorA, _ColorB, smoothDistanceMix((length(i.uv - 0.5) - 0.25 + _Params.z*sin(atan2(i.uv.y - 0.5, i.uv.x - 0.5)*_Params.y))));
							}
								ENDCG
						}

		//pass #5 - wave pattern
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			
			#include "UnityCG.cginc"


			float4 _Params,
					_ColorA,
					_ColorB;
			
			fixed4 frag (v2f_img i) : SV_Target
			{
				i.uv *= _Params.w;

				return lerp(_ColorA, _ColorB, sin(i.uv.x*3. + sin(i.uv.x*1.2 + i.uv.y*1.1)*2.0)*0.5 + 0.5);
			}
			ENDCG
		}

		//pass #6 - noise pattern
		Pass
			{
				CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


				float4 _Params;

				float3 hash33(float3 p) {
					return (abs(cos(p*.19487)*9284.3459 + cos(p.zxy*29.97612)*37.92384))%1.0;
				}
				fixed4 frag(v2f_img i) : SV_Target
				{
					i.uv *= _Params.w;

					float3 n = hash33(i.uv.xyy + i.uv.yxx*4.9);
					return fixed4(lerp(n.xxx, n, _Params.y), 1.); 
				}
					ENDCG
			}


			//pass #7 - voronoi pattern
			Pass
				{
					CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


					float4 _Params,
					_ColorA,
					_ColorB;

#define smoothDistanceMix(d) clamp(d*_Params.x,0.,1.)

					float repeatingCircle(float2 uv, float man) {
						float2 p = (abs(uv)%1.0) - 0.5;
						return lerp(dot(p, p), abs(p.x) / 3. + abs(p.y) / 3., man);
					}
					fixed4 frag(v2f_img i) : SV_Target
					{
						i.uv *= _Params.w/10.0;

						return lerp(_ColorA, _ColorB, smoothDistanceMix((
						min(repeatingCircle(i.uv, _Params.y),
						min(repeatingCircle(i.uv - float2(0.39, 0.22), _Params.y),
						repeatingCircle(i.uv - float2(0.77, 0.56), _Params.y))) / 3.
						))); 
					}
						ENDCG
				}


				//pass #8 - wood pattern
				Pass
					{
						CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"


						float4 _Params,
						_ColorA,
						_ColorB;

						fixed4 frag(v2f_img i) : SV_Target
						{
							i.uv *= _Params.w;

							return lerp(_ColorA, _ColorB, sin(i.uv.x + sin(i.uv.y*0.1)*7.0)*0.5 + 0.5);
						}
							ENDCG
					}
	}
}
