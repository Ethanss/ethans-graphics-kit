﻿//Ethan Alexander Shulman 2017
using UnityEngine;
using System.Threading;
using System.Collections;
using System.Collections.Generic;

namespace EthansGraphicsKit.ProceduralMesh
{
    
    /// <summary>
    /// Scene function sample.
    /// </summary>
    public struct SceneFunctionSample
    {
        public float distance;
        public int materialId;

        public SceneFunctionSample(float d, int m)
        {
            materialId = m;
            distance = d;
        }
    };

    
    /// <summary>
    /// Procedural scene function.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public delegate SceneFunctionSample ProceduralSceneFunction(float x, float y, float z);

    /// <summary>
    /// ProceduralScene, generate mesh from ProceduralSceneFunction.
    /// </summary>
    public class ProceduralScene : MonoBehaviour
    {
        public const int CHUNK_SIZE = 8;

        /// <summary>
        /// Target mesh destination.
        /// </summary>
        [Tooltip("Target mesh destination.")]
        public GameObject meshDestination;

        /// <summary>
        /// Material used.
        /// </summary>
        [Tooltip("Materials.")]
        public Material[] materials;

        /// <summary>
        /// Voxel size, smaller is higher resolution.
        /// </summary>
        [Tooltip("Voxel size in world space, smaller is higher resolution.")]
        public float voxelSize = 1.0f;


        /// <summary>
        /// Voxel netting distance.
        /// </summary>
        [Tooltip("Voxel netting distance.")]
        public float nettingDistance = 1.0f;

        /// <summary>
        /// Epsilon when calculating normals, lower is sharper.
        /// </summary>
        [Range(0.0f, 1.0f)]
        [Tooltip("Epsilon when calculating normals, lower is sharper.")]
        public float normalEpsilon = 1.0f;

        /// <summary>
        /// Generate MeshCollider's for generated meshes.
        /// </summary>
        [Tooltip("Generate MeshCollider's for generated meshes.")]
        public bool generateColliders = false;
        /// <summary>
        /// PhysicsMaterial for generated MeshCollider.
        /// </summary>
        [Tooltip("PhysicsMaterial for generated MeshCollider.")]
        public PhysicMaterial colliderMaterial;

        /// <summary>
        /// Delay in MS when generating mesh.
        /// </summary>
        [Tooltip("Delay in MS when generating mesh.")]
        public int generationDelay = 0;

        /// <summary>
        /// Should it recalculate mesh normals?
        /// </summary>
        [Tooltip("Should it recalculate mesh normals.")]
        public bool recalculateNormals = false;

        /// <summary>
        /// Smoothing angle when recalculating mesh normals.
        /// </summary>
        [Tooltip("Smoothing angle when recalculating mesh normals.")]
        public float recalculateNormalsSmoothingAngle = 60.0f;

        /// <summary>
        /// Calculate mesh tangents?
        /// </summary>
        [Tooltip("Should it calculate mesh tangents.")]
        public bool tangents = true;

        /// <summary>
        /// ProceduralSceneFunction that defines your procedural scene.
        /// </summary>
        public ProceduralSceneFunction sceneFunction = EmptyScene;

        private int widthInChunks, heightInChunks, lengthInChunks, lengthStride;
        private GameObject[] oldMeshes;

        private float epsilon,nepsilon;

        void OnEnable()
        {
            ResizeView();
        }

        void OnDisable()
        {
            //stop thread
            if (thread != null)
            {
                widthInChunks = 0;
                heightInChunks = 0;
                lengthInChunks = 0;
                thread.Join(-1);
            }

            //clear old meshes
            if (oldMeshes != null)
            {
                for (int i = 0; i < oldMeshes.Length; i++)
                {
                    GameObject.Destroy(oldMeshes[i]);
                }
            }
        }

        private bool updatingMesh = false, finaleUpdating = false;
        private Vector3 worldOrigin;
        private List<Vector3>[] bvertices, bnormals;
        private List<Vector2>[] buvs;
        private List<int>[][] btriangles;
        private Thread thread;
        
        /// <summary>
        /// Begin updating mesh.
        /// </summary>
        public void UpdateMesh()
        {
            StartCoroutine(UpdateMesh_BE());
        }
        private IEnumerator UpdateMesh_BE()
        {
            if (updatingMesh) yield break;
            updatingMesh = true;

            bvertices = new List<Vector3>[widthInChunks * lengthStride];
            bnormals = new List<Vector3>[widthInChunks * lengthStride];
            buvs = new List<Vector2>[widthInChunks * lengthStride];
            btriangles = new List<int>[widthInChunks * lengthStride][];

            worldOrigin = transform.position;

            finaleUpdating = false;
            thread = new Thread(updateMeshThread);
            thread.Start();

            while (!finaleUpdating) yield return new WaitForSeconds(0.1f);

            bool newGO = false;
            if (oldMeshes == null)
            {
                oldMeshes = new GameObject[widthInChunks * lengthStride];
                newGO = true;
            }

            for (int x = 0; x < widthInChunks; x++)
            {
                for (int y = 0; y < heightInChunks; y++)
                {
                    for (int z = 0; z < lengthInChunks; z++)
                    {
                        int id = x + y * widthInChunks + z * lengthStride;

                        if (bvertices[id] == null) continue;//skip empty

                        Mesh mesh;
                        if (newGO)
                        {
                            mesh = new Mesh();
                        }
                        else
                        {
                            mesh = oldMeshes[id].GetComponent<MeshFilter>().sharedMesh;
                        }
                        mesh.SetVertices(bvertices[id]);
                        mesh.SetUVs(0, buvs[id]);
                        List<int> activeMats = new List<int>();
                        List<Material> activeMat = new List<Material>();
                        for (int i = 0; i < materials.Length; i++)
                        {
                            if (btriangles[id][i].Count > 0)
                            {
                                activeMats.Add(i);
                                activeMat.Add(materials[i]);
                            }
                        }
                        mesh.subMeshCount = activeMats.Count;
                        for (int i = 0; i < activeMats.Count; i++)
                        {
                            mesh.SetTriangles(btriangles[id][activeMats[i]], i);
                        }
                        mesh.RecalculateBounds();
                        if (recalculateNormals)
                        {
                            if (recalculateNormalsSmoothingAngle > 0.0f)
                            {
                                mesh.RecalculateNormals(recalculateNormalsSmoothingAngle);
                            }
                            else
                            {
                                mesh.RecalculateNormals();
                            }
                        }
                        else
                        {
                            mesh.SetNormals(bnormals[id]);
                        }
                        if (tangents) TangentSolver.SolveTangents(mesh);
                        mesh.Optimize();
                        mesh.UploadMeshData(false);

                        if (newGO)
                        {
                            GameObject meshObj = new GameObject();
                            meshObj.AddComponent<MeshFilter>().sharedMesh = mesh;
                            meshObj.AddComponent<MeshRenderer>().sharedMaterials = activeMat.ToArray();
                            if (generateColliders && activeMat.Count > 0)
                            {
                                MeshCollider mc = meshObj.AddComponent<MeshCollider>();
                                mc.sharedMesh = mesh;
                                mc.sharedMaterial = colliderMaterial;
                            }
                            meshObj.transform.SetParent(meshDestination.transform, true);
                            oldMeshes[id] = meshObj;
                        }
                        else
                        {
                            GameObject meshObj = oldMeshes[id];
                            meshObj.GetComponent<MeshRenderer>().sharedMaterials = activeMat.ToArray();
                            if (generateColliders && activeMat.Count > 0)
                            {
                                MeshCollider mc = meshObj.GetComponent<MeshCollider>();
                                if (mc == null) mc = meshObj.AddComponent<MeshCollider>();
                                mc.sharedMesh = mesh;
                                mc.sharedMaterial = colliderMaterial;
                            }
                            oldMeshes[id] = meshObj;
                        }

                    }
                }
            }

            bvertices = null;
            bnormals = null;
            buvs = null;

            updatingMesh = false;
        }

        private void updateMeshThread()
        {
            //generate new mesh chunks
            epsilon = 0.0f;
            nepsilon = voxelSize * normalEpsilon;
            Vector3 halfOrigin = new Vector3(widthInChunks, heightInChunks, lengthInChunks) * CHUNK_SIZE * voxelSize / 2.0f,
                    halfVoxel = Vector3.one * voxelSize / 2.0f;
            halfOrigin -= halfVoxel;

            Vector3 vxTopBackLeft, vxTopBackRight, vxTopForwardLeft, vxTopForwardRight,
                    vxBottomBackLeft, vxBottomBackRight, vxBottomForwardLeft, vxBottomForwardRight,
                    nrmTopBackLeft = Vector3.one, nrmTopBackRight = Vector3.one, nrmTopForwardLeft = Vector3.one, nrmTopForwardRight = Vector3.one,
                    nrmBottomBackLeft = Vector3.one, nrmBottomBackRight = Vector3.one, nrmBottomForwardLeft = Vector3.one, nrmBottomForwardRight = Vector3.one;

            for (int x = 0; x < widthInChunks; x++)
            {
                for (int y = 0; y < heightInChunks; y++)
                {
                    for (int z = 0; z < lengthInChunks; z++)
                    {
                        Vector3 chunkWorldPos = worldOrigin + new Vector3(x, y, z) * voxelSize * CHUNK_SIZE - halfOrigin,
                                voxelWorldPos = chunkWorldPos+Vector3.one*voxelSize*CHUNK_SIZE/2.0f;

                        //try and early skip chunk
                        if (sceneFunction(voxelWorldPos.x, voxelWorldPos.y, voxelWorldPos.z).distance > voxelSize * CHUNK_SIZE * 1.05f)
                        {
                            bvertices[x + y * widthInChunks + z * lengthStride] = null;
                            continue;
                        }

                        List<Vector3> verts = new List<Vector3>();
                        List<Vector3> norms = new List<Vector3>();
                        List<Vector2> uvs = new List<Vector2>();
                        int vertCount = 0;

                        List<int>[] triangles = new List<int>[materials.Length];
                        for (int i = 0; i < triangles.Length; i++)
                        {
                            triangles[i] = new List<int>();
                        }

                        for (int vx = 0; vx < CHUNK_SIZE; vx++)
                        {
                            for (int vy = 0; vy < CHUNK_SIZE; vy++)
                            {
                                for (int vz = 0; vz < CHUNK_SIZE; vz++)
                                {
                                    voxelWorldPos = chunkWorldPos + new Vector3(vx, vy, vz) * voxelSize;

                                    SceneFunctionSample samp = sceneFunction(voxelWorldPos.x, voxelWorldPos.y, voxelWorldPos.z);
                                    if (samp.distance <= epsilon)
                                    {
                                        //box corners
                                        vxTopBackLeft = voxelWorldPos - halfVoxel;
                                        vxTopBackRight = voxelWorldPos - new Vector3(-halfVoxel.x, halfVoxel.y, halfVoxel.z);
                                        vxTopForwardLeft = voxelWorldPos - new Vector3(halfVoxel.x, halfVoxel.y, -halfVoxel.z);
                                        vxTopForwardRight = voxelWorldPos + new Vector3(halfVoxel.x, -halfVoxel.y, halfVoxel.z);
                                        vxBottomBackLeft = voxelWorldPos - new Vector3(halfVoxel.x, -halfVoxel.y, halfVoxel.z);
                                        vxBottomBackRight = voxelWorldPos - new Vector3(-halfVoxel.x, -halfVoxel.y, halfVoxel.z);
                                        vxBottomForwardLeft = voxelWorldPos - new Vector3(halfVoxel.x, -halfVoxel.y, -halfVoxel.z);
                                        vxBottomForwardRight = voxelWorldPos + new Vector3(halfVoxel.x, halfVoxel.y, halfVoxel.z);


                                        if (!recalculateNormals || nettingDistance > 0.0f)
                                        {
                                            //sample normals
                                            nrmTopBackLeft = SampleDistanceDerivative(vxTopBackLeft.x, vxTopBackLeft.y, vxTopBackLeft.z).normalized;
                                            nrmTopBackRight = SampleDistanceDerivative(vxTopBackRight.x, vxTopBackRight.y, vxTopBackRight.z).normalized;//1
                                            nrmTopForwardLeft = SampleDistanceDerivative(vxTopForwardLeft.x, vxTopForwardLeft.y, vxTopForwardLeft.z).normalized;//2
                                            nrmTopForwardRight = SampleDistanceDerivative(vxTopForwardRight.x, vxTopForwardRight.y, vxTopForwardRight.z).normalized;//3
                                            nrmBottomBackLeft = SampleDistanceDerivative(vxBottomBackLeft.x, vxBottomBackLeft.y, vxBottomBackLeft.z).normalized;//4
                                            nrmBottomBackRight = SampleDistanceDerivative(vxBottomBackRight.x, vxBottomBackRight.y, vxBottomBackRight.z).normalized;//5
                                            nrmBottomForwardLeft = SampleDistanceDerivative(vxBottomForwardLeft.x, vxBottomForwardLeft.y, vxBottomForwardLeft.z).normalized;//6
                                            nrmBottomForwardRight = SampleDistanceDerivative(vxBottomForwardRight.x, vxBottomForwardRight.y, vxBottomForwardRight.z).normalized;//7
                                        }
                                        if (nettingDistance > 0.0f)
                                        {
                                            //net corners to distance function
                                            vxTopBackLeft -= nrmTopBackLeft * nettingDistance * (sceneFunction(vxTopBackLeft.x, vxTopBackLeft.y, vxTopBackLeft.z).distance);//0
                                            vxTopBackRight -= nrmTopBackRight * nettingDistance * (sceneFunction(vxTopBackRight.x, vxTopBackRight.y, vxTopBackRight.z).distance);//1
                                            vxTopForwardLeft -= nrmTopForwardLeft * nettingDistance * (sceneFunction(vxTopForwardLeft.x, vxTopForwardLeft.y, vxTopForwardLeft.z).distance);//2
                                            vxTopForwardRight -= nrmTopForwardRight * nettingDistance * (sceneFunction(vxTopForwardRight.x, vxTopForwardRight.y, vxTopForwardRight.z).distance);//3
                                            vxBottomBackLeft -= nrmBottomBackLeft * nettingDistance * (sceneFunction(vxBottomBackLeft.x, vxBottomBackLeft.y, vxBottomBackLeft.z).distance);//4
                                            vxBottomBackRight -= nrmBottomBackRight * nettingDistance * (sceneFunction(vxBottomBackRight.x, vxBottomBackRight.y, vxBottomBackRight.z).distance);//5
                                            vxBottomForwardLeft -= nrmBottomForwardLeft * nettingDistance * (sceneFunction(vxBottomForwardLeft.x, vxBottomForwardLeft.y, vxBottomForwardLeft.z).distance);//6
                                            vxBottomForwardRight -= nrmBottomForwardRight * nettingDistance * (sceneFunction(vxBottomForwardRight.x, vxBottomForwardRight.y, vxBottomForwardRight.z).distance);
                                        }
                                        
                                        List<int> ta = triangles[samp.materialId];

                                        //top
                                        if (sceneFunction(voxelWorldPos.x, voxelWorldPos.y - voxelSize, voxelWorldPos.z).distance >= epsilon) //only add if not covered
                                        {
                                            verts.Add(vxTopBackRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardRight);
                                            uvs.Add(Vector2.right);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopBackRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopBackLeft);
                                            uvs.Add(Vector2.up);
                                            ta.Add(vertCount++);

                                            if (!recalculateNormals)
                                            {
                                                norms.Add(nrmTopBackRight);
                                                norms.Add(nrmTopForwardRight);
                                                norms.Add(nrmTopForwardLeft);
                                                norms.Add(nrmTopBackRight);
                                                norms.Add(nrmTopForwardLeft);
                                                norms.Add(nrmTopBackLeft);
                                            }

                      
                                        }

                                        //front
                                        if (sceneFunction(voxelWorldPos.x, voxelWorldPos.y, voxelWorldPos.z + voxelSize).distance >= epsilon) //only add if not covered
                                        {
                                            verts.Add(vxTopForwardRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardRight);
                                            uvs.Add(Vector2.right);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardLeft);
                                            uvs.Add(Vector2.up);
                                            ta.Add(vertCount++);

                                            if (!recalculateNormals)
                                            {
                                                norms.Add(nrmTopForwardRight);
                                                norms.Add(nrmBottomForwardRight);
                                                norms.Add(nrmBottomForwardLeft);
                                                norms.Add(nrmTopForwardRight);
                                                norms.Add(nrmBottomForwardLeft);
                                                norms.Add(nrmTopForwardLeft);
                                            }

                                        }

                                        //right
                                        if (sceneFunction(voxelWorldPos.x + voxelSize, voxelWorldPos.y, voxelWorldPos.z).distance >= epsilon) //only add if not covered
                                        {

    
                                            verts.Add(vxTopBackRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackRight);
                                            uvs.Add(Vector2.right);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardRight);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopBackRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardRight);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardRight);
                                            uvs.Add(Vector2.up);
                                            ta.Add(vertCount++);

                                            if (!recalculateNormals)
                                            {
                                                norms.Add(nrmTopBackRight);
                                                norms.Add(nrmBottomBackRight);
                                                norms.Add(nrmBottomForwardRight);
                                                norms.Add(nrmTopBackRight);
                                                norms.Add(nrmBottomForwardRight);
                                                norms.Add(nrmTopForwardRight);
                                            }
                                        }

                                        //back
                                        if (sceneFunction(voxelWorldPos.x, voxelWorldPos.y, voxelWorldPos.z - voxelSize).distance >= epsilon) //only add if not covered
                                        {

                                            verts.Add(vxTopBackLeft);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackLeft);
                                            uvs.Add(Vector2.right);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackRight);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopBackLeft);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackRight);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopBackRight);
                                            uvs.Add(Vector2.up);
                                            ta.Add(vertCount++);

                                            if (!recalculateNormals)
                                            {
                                                norms.Add(nrmTopBackLeft);
                                                norms.Add(nrmBottomBackLeft);
                                                norms.Add(nrmBottomBackRight);
                                                norms.Add(nrmTopBackLeft);
                                                norms.Add(nrmBottomBackRight);
                                                norms.Add(nrmTopBackRight);
                                            }
                                        }

                                        //left
                                        if (sceneFunction(voxelWorldPos.x - voxelSize, voxelWorldPos.y, voxelWorldPos.z).distance >= epsilon) //only add if not covered
                                        {
                                            verts.Add(vxTopForwardLeft);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardLeft);
                                            uvs.Add(Vector2.right);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopForwardLeft);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxTopBackLeft);
                                            uvs.Add(Vector2.up);
                                            ta.Add(vertCount++);

                                            if (!recalculateNormals)
                                            {
                                                norms.Add(nrmTopForwardLeft);
                                                norms.Add(nrmBottomForwardLeft);
                                                norms.Add(nrmBottomBackLeft);
                                                norms.Add(nrmTopForwardLeft);
                                                norms.Add(nrmBottomBackLeft);
                                                norms.Add(nrmTopBackLeft);
                                            }
                                        }

                                        //bottom
                                        if (sceneFunction(voxelWorldPos.x, voxelWorldPos.y + voxelSize, voxelWorldPos.z).distance >= epsilon) //only add if not covered
                                        {
                                            verts.Add(vxBottomForwardRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackRight);
                                            uvs.Add(Vector2.right);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardRight);
                                            uvs.Add(Vector2.one);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomBackLeft);
                                            uvs.Add(Vector2.zero);
                                            ta.Add(vertCount++);

                                            verts.Add(vxBottomForwardLeft);
                                            uvs.Add(Vector2.up);
                                            ta.Add(vertCount++);

                                            if (!recalculateNormals)
                                            {
                                                norms.Add(nrmBottomForwardRight);
                                                norms.Add(nrmBottomBackRight);
                                                norms.Add(nrmBottomBackLeft);
                                                norms.Add(nrmBottomForwardRight);
                                                norms.Add(nrmBottomBackLeft);
                                                norms.Add(nrmBottomForwardLeft);
                                            }
                                        }

                                    }
                                }
                            }
                        }

                        int id = x + y * widthInChunks + z * lengthStride;
                        bvertices[id] = verts;
                        if (!recalculateNormals) bnormals[id]= norms;
                        buvs[id] = uvs;
                        btriangles[id] = triangles;

                        if (generationDelay > 0) Thread.Sleep(generationDelay);
                    }
                }
            }

            finaleUpdating = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>True if currently generating mesh.</returns>
        public bool IsGenerating()
        {
            return updatingMesh;
        }

        /// <summary>
        /// Sample distance derivative from scene function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public Vector3 SampleDistanceDerivative(float x, float y, float z)
        {
            return (new Vector3(sceneFunction(x + nepsilon, y, z).distance - sceneFunction(x - nepsilon, y, z).distance,
                               sceneFunction(x, y + nepsilon, z).distance - sceneFunction(x, y - nepsilon, z).distance,
                               sceneFunction(x, y, z + nepsilon).distance - sceneFunction(x, y, z - nepsilon).distance));
        }

        /// <summary>
        /// Resize.
        /// </summary>
        public void ResizeView()
        {
            Vector3 voxSize = transform.localScale;
            widthInChunks = Mathf.Max(1,(int)Mathf.Ceil(voxSize.x));
            heightInChunks = Mathf.Max(1,(int)Mathf.Ceil(voxSize.y));
            lengthInChunks = Mathf.Max(1,(int)Mathf.Ceil(voxSize.z));
            lengthStride = heightInChunks*lengthInChunks;
        }


        public void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            ResizeView();
            Gizmos.DrawWireCube(transform.position, new Vector3(widthInChunks,heightInChunks,lengthInChunks)*CHUNK_SIZE*voxelSize);
        }


        //smooth max
        /// <summary>
        /// Exponential smooth max.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static float smax(float a, float b, float k)
        {
            return Mathf.Log(Mathf.Exp(a) + Mathf.Exp(b)) / k;
        }

        //polynomial smooth min
        /// <summary>
        /// Polynomial smooth min.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static  float smin(float a, float b, float k) {
            float h = Mathf.Clamp01(0.5f + 0.5f * (b - a) / k);
            return Mathf.Lerp(b, a, h) - k * h * (1.0f - h);
        }

        //fast cellular voronoi by Shane, on Shadertoy https://www.shadertoy.com/view/4lVGDy
        public static float drawVoronoiCell(float x, float y, float z)
        {
            x = (x - Mathf.Floor(x))-0.5f;
            y = (y - Mathf.Floor(y))-0.5f;
            z = (z - Mathf.Floor(z))-0.5f;
            return x * x + y * y + z * z;
        }
        /// <summary>
        /// Fast cellular voronoi.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static float voronoi(float x, float y, float z) {
            float d1,d2,d3,d4,b;

            d1 = drawVoronoiCell(x-.81f,y-.62f,z-.53f);
            
            b = x;
            x = y-x;
            y += b;
            d2 = drawVoronoiCell(x-.39f,y-.2f,z-.11f);

            b = y;
            y = z-y;
            z += b;
            d3 = drawVoronoiCell(x-.62f, y-.24f, z-.06f);

            b = x;
            x = z-x;
            z += b;
            d4 = drawVoronoiCell(x-.2f, y-.82f, z-.64f);

            if (d1 < d2) {
                if (d1 < d3) {
                    if (d1 < d4) {
                        return d1*2.66f;
                    } else {
                        return d4*2.66f;
                    }
                } else {
                    return d3*2.66f;
                }
            } else {
                if (d2 < d3) {
                    if (d2 < d4) {
                        return d2*2.66f;
                    } else {
                        return d4*2.66f;
                    }
                } else {
                    return d3*2.66f;
                }
            }
        }

        /// <summary>
        /// Pseudo-random hash from seed 'i'.
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public static float hash(float i)
        {
            return Mathf.Abs(i*i * 0.09274f + i * 9258.938477f) % 1.0f;
        }

        /// <summary>
        /// Rotate x,y by radians 'a'
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="a"></param>
        public static void rotate2D(ref float x, ref float y, float a)
        {
            float sa = Mathf.Sin(a),
                  ca = Mathf.Cos(a),
                  px = x;

            x = x * ca - y * sa;
            y = y * ca + px * sa;
        }

        /// <summary>
        /// Box signed distance function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <param name="l"></param>
        /// <returns></returns>
        public static float sdBox(float x, float y, float z, float w, float h, float l) {
            x = Mathf.Abs(x)-w;
            y = Mathf.Abs(y)-h;
            z = Mathf.Abs(z)-l;
            w = x>y?(x>z?x:z):(y>z?y:z);
            if (w > 0.0f) w = 0.0f;
            if (x < 0.0f) x = 0.0f;
            if (y < 0.0f) y = 0.0f;
            if (z < 0.0f) z = 0.0f;
            return w+Mathf.Sqrt(x*x+y*y+z*z);
        }

        /// <summary>
        /// Torus signed distance function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static float sdTorus(float x, float y, float z, float r1, float r2)
        {
            float qx = Mathf.Sqrt(x * x+z*z) - r1;
            return Mathf.Sqrt(qx * qx + y * y) - r2;
        }

        /// <summary>
        /// Sphere signed distance function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="r"></param>
        /// <returns></returns>
        public static float sdSphere(float x, float y, float z, float r)
        {
            return Mathf.Sqrt(x*x+y*y+z*z)-r;
        }

        /// <summary>
        /// Cone signed distance function, cx,cy must be normalized.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <returns></returns>
        public static float sdCone(float x, float y, float z, float cx, float cy)//cx,cy must be normalized
        {
            float q = Mathf.Sqrt(x * x + y * y);// length(p.xy);
            return cx * q + cy * z;
        }

        /// <summary>
        /// Hex prism signed distance function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static float sdHexPrism(float x, float y, float z, float r1, float r2)
        {
            x = Mathf.Abs(x);
            y = Mathf.Abs(y);
            z = Mathf.Abs(z);

            return Mathf.Max(z-r2,Mathf.Max((x*0.866025f+y*0.5f),y)-r1);
        }

        /// <summary>
        /// Triangular prism signed distance function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static float sdTriPrism(float x, float y, float z, float r1, float r2)
        {
            x = Mathf.Abs(x);
            z = Mathf.Abs(z);

            return Mathf.Max(z - r2, Mathf.Max(x * 0.866025f + y * 0.5f, -y) - r1 * 0.5f);
        }

        /// <summary>
        /// Capped cylinder signed distance function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        public static float sdCappedCylinder(float x, float y, float z, float r1, float r2)
        {
            float dx = Mathf.Sqrt(x * x + z * z) - r1,
                  dy = Mathf.Abs(y)-r2;

            if (dx < 0.0f) x = 0.0f;
            else x = dx;
            if (dy < 0.0f) y = 0.0f;
            else y = dy;

            return Mathf.Min(Mathf.Max(dx, dy), 0.0f) + Mathf.Sqrt(x * x + y * y);
        }


        /// <summary>
        /// Empty scene function.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static SceneFunctionSample EmptyScene(float x, float y, float z)
        {
            return new SceneFunctionSample(1e4f, 0);
        }
    }
}