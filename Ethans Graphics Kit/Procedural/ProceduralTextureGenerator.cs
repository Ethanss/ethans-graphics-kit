﻿//Ethan Alexander Shulman 2017

using UnityEngine;


namespace EthansGraphicsKit.ProceduralTexture
{
     public enum Pattern {
            Checker = 0,
            Dot = 1,
            Diamond = 2,
            Grid = 3,
            Flower = 4,
            Wave = 5,
            Noise = 6,
            Voronoi = 7,
            Wood = 8
    }

    public class ProceduralTextureGenerator
    {
        /// <summary>
        /// Generate texture from shader.
        /// </summary>
        /// <param name="shaderName"></param>
        /// <param name="passId"></param>
        /// <param name="textureWidth"></param>
        /// <param name="textureHeight"></param>
        /// <param name="colorA"></param>
        /// <param name="colorB"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static Texture Generate(string shaderName, int passId, int textureWidth, int textureHeight, Color colorA, Color colorB, Vector4 param)
        {
            Shader shader = Shader.Find(shaderName);
            if (shader == null) return null;

            Material mat = new Material(shader);
            mat.SetColor("_ColorA", colorA);
            mat.SetColor("_ColorB", colorB);
            mat.SetVector("_Params", param);

            Texture2D tx = new Texture2D(textureWidth, textureHeight, TextureFormat.ARGB32, true);
            RenderTexture dst = new RenderTexture(textureWidth, textureHeight, 0, RenderTextureFormat.ARGB32);
            Graphics.Blit(null, dst, mat, passId);

            RenderTexture.active = dst;
            tx.ReadPixels(new Rect(0, 0, textureWidth, textureHeight), 0, 0);
            tx.Apply();
            RenderTexture.active = null;

            GameObject.DestroyImmediate(mat);
            GameObject.DestroyImmediate(dst);

            return tx;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <param name="textureWidth"></param>
        /// <param name="textureHeight"></param>
        /// <param name="colorA"></param>
        /// <param name="colorB"></param>
        /// <param name="size"></param>
        /// <param name="smoothness"></param>
        /// <returns></returns>
        public static Texture GeneratePattern(Pattern p, int textureWidth, int textureHeight, Color colorA, Color colorB, Vector2 size, float smoothness, float scale)
        {
            return Generate("Hidden/EGKProceduralTexture", (int)p, textureWidth, textureHeight, colorA, colorB, new Vector4(1.0f/smoothness, size.x, size.y, 10.0f*scale));
        }
    }
}